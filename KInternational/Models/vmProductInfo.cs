﻿using System.Collections.Generic;

namespace KInternational.Models
{
	public class vmProductInfo
	{
		public List<vmColorProductPhoto> ColorPhotos { get; set; }
		public List<vmFeaturedSpecifications> FeaturedSpecifications { get; set; }

		private vmProductInfo()
		{
			ColorPhotos = new List<vmColorProductPhoto>();
			FeaturedSpecifications = new List<vmFeaturedSpecifications>();
		}

		public static vmProductInfo CreateModel(List<vmColorProductPhoto> ColorPhotos, List<vmFeaturedSpecifications> FeatureSpecs)
		{
			return new vmProductInfo()
			{
				ColorPhotos = ColorPhotos,
				FeaturedSpecifications = FeatureSpecs
			};
		}
	}
}
