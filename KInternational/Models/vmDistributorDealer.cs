﻿using KInternational.Models.Models;
using System.Collections.Generic;

namespace KInternational.Models
{
	public class vmDistributorDealer
	{
		public List<DistributorModel> AllDistributors { get; set; }
		public List<DealerModel> AllDealers { get; set; }

		private vmDistributorDealer()
		{
			AllDistributors = new List<DistributorModel>();
			AllDealers = new List<DealerModel>();
		}

		public static vmDistributorDealer CreateModel(List<DistributorModel> Distributrs, List<DealerModel> Dealers)
		{
			return new vmDistributorDealer()
			{
				AllDistributors = Distributrs,
				AllDealers = Dealers
			};
		}
	}
}
