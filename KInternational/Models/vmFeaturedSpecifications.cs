﻿
namespace KInternational.Models
{
	public class vmFeaturedSpecifications
	{
		public string SpecificationTitle { get; private set; }
		public string SpecificationValue { get; private set; }

		private vmFeaturedSpecifications()
		{
		}

		public static vmFeaturedSpecifications CreateModel(string specTitle, string specValue)
		{
			return new vmFeaturedSpecifications()
			{
				SpecificationTitle = specTitle,
				SpecificationValue = specValue
			};
		}
	}
}
