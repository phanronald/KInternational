﻿using KInternational.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KInternational.Models
{
	public class vmProductSpecification
	{
		public List<ProductSpecGroupWithSpecs> ProductSpecGroupWithSpecs { get; private set; }
		public List<SpecificationGroup> SpecificationGroups { get; private set; }

		private vmProductSpecification()
		{
			ProductSpecGroupWithSpecs = new List<ProductSpecGroupWithSpecs>();
			SpecificationGroups = new List<SpecificationGroup>();
		}

		public static vmProductSpecification CreateModel(
			List<ProductSpecGroupWithSpecs> productSpecGroupWithSpecs, 
			List<SpecificationGroup> specGroups)
		{
			return new vmProductSpecification()
			{
				ProductSpecGroupWithSpecs = productSpecGroupWithSpecs,
				SpecificationGroups = specGroups
			};
		}
	}

	public struct ProductSpecGroupWithSpecs
	{
		public int SpecGroupId { get; set; }
		public string SpecName { get; set; }
		public string SpecValue { get; set; }
	}
}
