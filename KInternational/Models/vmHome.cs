﻿using KInternational.Models.Models;
using System.Collections.Generic;

namespace KInternational.Models
{
	public class vmHome
	{
		public string BingMapApiKey { get; set; }

		private vmHome()
		{
		}

		public static vmHome CreateModel(string bingMapApiKey)
		{
			return new vmHome()
			{
				BingMapApiKey = bingMapApiKey
			};
		}
	}
}
