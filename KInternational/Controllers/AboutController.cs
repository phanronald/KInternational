﻿using KInternational.Services.Core;
using Microsoft.AspNetCore.Mvc;

namespace KInternational.Controllers
{
	[Produces("application/json")]
    [Route("api/About")]
    public class AboutController : Controller
    {
		private IAboutService _aboutSvc;

		public AboutController(IAboutService aboutService)
		{
			_aboutSvc = aboutService;
		}

		[HttpGet]
		[Route("GetAbout")]
		public IActionResult GetAboutData()
		{
			var aboutData = _aboutSvc.GetAllAbout();
			return new ObjectResult(aboutData);
		}
	}
}