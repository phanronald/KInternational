﻿using KInternational.Services.Core;
using Microsoft.AspNetCore.Mvc;

namespace KInternational.Controllers
{
	[Produces("application/json")]
    [Route("api/SubCategories")]
    public class SubCategoriesController : Controller
    {
		private IProductSubCategoryService _subCategorySvc;

		public SubCategoriesController(IProductSubCategoryService subCategoryService)
		{
			_subCategorySvc = subCategoryService;
		}

		[HttpGet]
		[Route("GetAllSubCategories")]
		public IActionResult GetAllSubCategories()
		{
			var categories = this._subCategorySvc.GetSubCategories();
			return new ObjectResult(categories);
		}
	}
}