﻿using KInternational.Services.Core;
using Microsoft.AspNetCore.Mvc;

namespace KInternational.Controllers
{
	[Produces("application/json")]
	[Route("api/Technology")]
	public class TechnologyController : Controller
	{
		private ITechnologyService _technologySvc;

		public TechnologyController(ITechnologyService technologyService)
		{
			_technologySvc = technologyService;
		}

		[HttpGet]
		[Route("GetAllTechnologies")]
		public IActionResult GetAllTechnologies()
		{
			var technologies = this._technologySvc.GetAllTechnologies();
			return new ObjectResult(technologies);
		}
	}
}