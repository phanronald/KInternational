using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using KInternational.Models;
using KInternational.Services.Core;
using Microsoft.AspNetCore.Mvc;

namespace KInternational.Controllers
{
	public class HomeController : Controller
	{
		private IHomeService _homeSvc;

		public HomeController(IHomeService homeService)
		{
			_homeSvc = homeService;
		}

		public IActionResult Index()
		{
			var model = vmHome.CreateModel(_homeSvc.GetBingMapApiKey());
			return View(model);
		}

		public IActionResult Error()
		{
			ViewData["RequestId"] = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
			return View();
		}
	}
}
