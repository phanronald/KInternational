﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KInternational.Services.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KInternational.Controllers
{
	[Produces("application/json")]
	[Route("api/Events")]
	public class EventsController : Controller
	{
		private IEventService _eventSvc;

		public EventsController(IEventService eventService)
		{
			_eventSvc = eventService;
		}

		[HttpGet]
		[Route("GetAllEvents")]
		public IActionResult GetAllCategories()
		{
			var events = _eventSvc.GetAllEvents();
			return new ObjectResult(events);
		}
	}
}