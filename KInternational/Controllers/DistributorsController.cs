﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using KInternational.Models;
using KInternational.Models.Models;
using KInternational.Services.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KInternational.Controllers
{
    [Produces("application/json")]
    [Route("api/Distributors")]
    public class DistributorsController : Controller
    {
		private IDistributorService _distributorSvc;
		private IDealerService _dealerSvc;

		public DistributorsController(IDistributorService distributorService,
										IDealerService dealerService)
		{
			_distributorSvc = distributorService;
			_dealerSvc = dealerService;
		}

		[HttpGet]
		[Route("GetDistributorsDealers")]
		public IActionResult GetDistributorsDealers()
		{
			var distributorsData = _distributorSvc.GetAllDistributors();
			var dealersData = _dealerSvc.GetAllDealers();
			var model = vmDistributorDealer.CreateModel(distributorsData.ToList(), dealersData.ToList());
			return new ObjectResult(model);
		}

		[HttpGet]
		[Route("GetDistributorByCountryId")]
		public IActionResult GetDistributorByCountryId(int countryId)
		{
			var data = _distributorSvc.GetAllDistributors().Where(x => x.CountryId == countryId).FirstOrDefault();
			return new ObjectResult(data);
		}

		[HttpPost]
		[Route("SaveUserContactToDistributor")]
		public IActionResult SaveUserContactToDistributor([FromBody]DistroContact data)
		{
			var a = "";
			return Ok(HttpStatusCode.OK);
		}
	}
}
 