﻿using System.Collections.Generic;
using System.Linq;
using KInternational.Models;
using KInternational.Models.Models;
using KInternational.Services.Core;
using Microsoft.AspNetCore.Mvc;

namespace KInternational.Controllers
{
	[Produces("application/json")]
    [Route("api/Products")]
    public class ProductsController : Controller
    {
		private ISpecificationService _specificationSvc;
		private IProductColorPhotosService _productColorPhotosSvc;
		private IProductService _productSvc;

		public ProductsController(IProductService productService, 
			ISpecificationService specificationService, 
			IProductColorPhotosService productColorPhotosService)
		{
			_productSvc = productService;
			_specificationSvc = specificationService;
			_productColorPhotosSvc = productColorPhotosService;
		}

		[HttpGet]
		[Route("GetAllProducts")]
		public IActionResult GetAllProducts()
		{
			var products = this._productSvc.GetProducts();
			return new ObjectResult(products);
		}

		[HttpGet]
		[Route("GetProductInfo")]
		public IActionResult GetProductInfo(int productId)
		{
			var featuredProductSpecification = _specificationSvc.GetAllProductSpecifications_ByProductId(productId)
										.Where(x => x.IsFeatured).ToList();

			var allSpecificiationIds = featuredProductSpecification.Select(x => x.SpecId);

			var featuredSpecification = _specificationSvc.GetAllSpecifications()
										.Where(x => allSpecificiationIds.Contains(x.ProductSpecId));

			var featureSpecJoinProdSpec = from featureProdSpec in featuredProductSpecification
										  join featureSpec in featuredSpecification
										  on featureProdSpec.SpecId equals featureSpec.ProductSpecId
										  select vmFeaturedSpecifications.CreateModel(featureSpec.Name, featureProdSpec.SpecValue);

			var colorPhotoData = _productColorPhotosSvc.GetProductColorPhotos_ById(productId);
			var allProdColorIds = colorPhotoData.Select(x => x.ColorId);
			var allColorIds = _productSvc.GetAllColors().Where(x => allProdColorIds.Contains(x.ColorId));

			var colorJoinColorPhoto = from colorPhoto in colorPhotoData
									  join color in allColorIds
									  on colorPhoto.ColorId equals color.ColorId
									  select vmColorProductPhoto.CreateModel(color.ColorId, colorPhoto.ImageUrl, color.ImageUrl, color.ColorName);

			var model = vmProductInfo.CreateModel(colorJoinColorPhoto.ToList(), featureSpecJoinProdSpec.ToList());
			return new ObjectResult(model);
		}

		[HttpGet]
		[Route("GetProductSpecifications")]
		public IActionResult GetProductSpecifications(int productId)
		{
			var allProdSpecs = _specificationSvc.GetAllProductSpecifications_ByProductId(productId);

			var allSpecIdsInProduct = allProdSpecs.Select(x => x.SpecId);

			var allSpecs = _specificationSvc.GetAllSpecifications()
							.Where(x => allSpecIdsInProduct.Contains(x.ProductSpecId));

			var allSpecGroupIds = allSpecs.Select(x => x.SpecGroupId);

			var allSpecGroups = _specificationSvc.GetAllSpecGroups()
									.Where(x => allSpecGroupIds.Contains(x.SpecGroupId));


			var listProductSpecGroupWithSpecs = new List<ProductSpecGroupWithSpecs>();
			allProdSpecs.ForEach((ProductSpecification productSpec) =>
			{
				var foundSpec = allSpecs.Where(x => x.ProductSpecId == productSpec.SpecId).FirstOrDefault();
				var foundSpecGroup = allSpecGroups.Where(x => x.SpecGroupId == foundSpec.SpecGroupId).FirstOrDefault();
				var tProdSpecGroupWithSpec = new ProductSpecGroupWithSpecs()
				{
					SpecGroupId = foundSpecGroup.SpecGroupId,
					SpecName = foundSpec.Name,
					SpecValue = productSpec.SpecValue
				};

				listProductSpecGroupWithSpecs.Add(tProdSpecGroupWithSpec);
			});

			listProductSpecGroupWithSpecs = listProductSpecGroupWithSpecs.GroupBy(x => x.SpecGroupId).SelectMany(y => y).ToList();
			var model = vmProductSpecification.CreateModel(listProductSpecGroupWithSpecs, allSpecGroups.ToList());
			return new ObjectResult(model);
		}

		[HttpGet]
		[Route("GetProductPhotos")]
		public IActionResult GetProductPhotos(int productId)
		{
			var productPhotos = this._productSvc.GetProductPhotos_ByProductId(productId);
			return new ObjectResult(productPhotos);
		}

		[HttpGet]
		[Route("GetProductVideo")]
		public IActionResult GetProductVideo(int productId)
		{
			var productVideos = this._productSvc.GetProductVideos_ByProductId(productId);
			return new ObjectResult(productVideos);
		}
	}
}
 