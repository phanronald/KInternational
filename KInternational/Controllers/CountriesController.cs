﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KInternational.Services.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KInternational.Controllers
{
    [Produces("application/json")]
    [Route("api/Countries")]
    public class CountriesController : Controller
    {
		private ICountryService _countrySvc;

		public CountriesController(ICountryService countryService)
		{
			_countrySvc = countryService;
		}

		[HttpGet]
		[Route("GetAllCountries")]
		public IActionResult GetAllCountries()
		{
			var countries = this._countrySvc.GetAllCountries();
			return new ObjectResult(countries);
		}
	}
}