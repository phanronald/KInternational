﻿using KInternational.Services.Core;
using Microsoft.AspNetCore.Mvc;

namespace KInternational.Controllers
{
	[Produces("application/json")]
    [Route("api/Categories")]
    public class CategoriesController : Controller
    {
		private IProductCategoryService _categorySvc;

		public CategoriesController(IProductCategoryService categoryService)
		{
			_categorySvc = categoryService;
		}

		[HttpGet]
		[Route("GetAllCategories")]
		public IActionResult GetAllCategories()
		{
			var categories = this._categorySvc.GetCategories();
			return new ObjectResult(categories);
		}
	}
}