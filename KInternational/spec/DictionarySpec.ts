﻿/// <reference path="typings/index.d.ts" />

import { IKeyValuePair } from './../ClientApp/shared/interfaces/IKeyValuePair';
import { Dictionary } from './../ClientApp/shared/core/Dictionary';

describe("Dictionaries", function () {

	var genericDictionary = new Dictionary<string, number>();

	describe("Dictionary Functionality", function () {

		describe("when there is no item in dictionary, the Count function", function () {
			it("should return 0", function () {
				expect(genericDictionary.Count()).toBe(0);
			});
		});

		describe("when there is no item in dictionary, the Add function", function () {
			it("should return a count of 1 when adding a new item", function () {
				genericDictionary.AddKeyValue("1", 99);
				expect(genericDictionary.Count()).toBe(1);
			});
		});

		describe("when there is one item in dictionary, the ContaineKey function", function () {
			it("should return true when key exists", function () {
				expect(genericDictionary.ContainsKey("1")).toBe(true);
			});

			it("should return true when key does not exists", function () {
				expect(genericDictionary.ContainsKey("99")).toBe(false);
			});
		});

		describe("when there is one item in dictionary, the ContainValue function", function () {
			it("should return true when value exists", function () {
				expect(genericDictionary.ContainsValue(99)).toBe(true);
			});

			it("should return false when value does not exists", function () {
				expect(genericDictionary.ContainsValue(1)).toBe(false);
			});
		});
		
		describe("when there is one item in dictionary, then another is added, the GetKeys function", function () {

			it("should return a count of 2", function () {
				genericDictionary.AddKeyValue("3", 1);
				expect(genericDictionary.GetKeys().length).toBe(2);
			});
		});

		describe("when there is two item in dictionary, then another is added, the GetValues function", function () {

			it("should return 3 items with the second one being value of 1", function () {
				genericDictionary.AddKeyValue("4", 100);
				const valueOfSecondItem = genericDictionary.GetValues()[1];
				expect(valueOfSecondItem).toBe(1);
			});
		});
	});

});
