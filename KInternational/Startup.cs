using KInternational.Data.About;
using KInternational.Data.Core;
using KInternational.Data.Country;
using KInternational.Data.Dealer;
using KInternational.Data.Distributor;
using KInternational.Data.Event;
using KInternational.Data.Product;
using KInternational.Data.Technology;
using KInternational.Services.About;
using KInternational.Services.Core;
using KInternational.Services.Country;
using KInternational.Services.Dealer;
using KInternational.Services.Distributor;
using KInternational.Services.Event;
using KInternational.Services.Home;
using KInternational.Services.Product;
using KInternational.Services.Technology;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;

namespace KInternational
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddSingleton<IHomeService, HomeService>();
			services.AddSingleton<ICountryRepo, CountryRepo>();
			services.AddSingleton<ICountryService, CountryService>();

			services.AddSingleton<IAboutRepo, AboutRepo>();
			services.AddSingleton<IAboutService, AboutService>();
			services.AddSingleton<ITechnologyRepo, TechnologyRepo>();
			services.AddSingleton<ITechnologyService, TechnologyService>();
			services.AddSingleton<IDistributorRepo, DistributorRepo>();
			services.AddSingleton<IDistributorService, DistributorService>();
			services.AddSingleton<IDealerRepo, DealerRepo>();
			services.AddSingleton<IDealerService, DealerService>();
			services.AddSingleton<IEventRepo, EventRepo>();
			services.AddSingleton<IEventService, EventService>();

			services.AddSingleton<ISpecificationRepo, SpecificationRepo>();
			services.AddSingleton<ISpecificationService, SpecificationService>();
			services.AddSingleton<IProductColorPhotosRepo, ProductColorPhotosRepo>();
			services.AddSingleton<IProductColorPhotosService, ProductColorPhotosService>();

			services.AddSingleton<IProductCategoryRepo, ProductCategoryRepo>();
			services.AddSingleton<IProductCategoryService, ProductCategoryService>();
			services.AddSingleton<IProductSubCategoryRepo, ProductSubCategoryRepo>();
			services.AddSingleton<IProductSubCategoryService, ProductSubCategoryService>();
			services.AddSingleton<IProductRepo, ProductRepo>();
			services.AddSingleton<IProductService, ProductService>();

			services.AddMvc()
				.AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());

			services.AddAntiforgery(options => options.HeaderName = "X-CSRF-TOKEN");
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
				//app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
				//{
				//    HotModuleReplacement = true,
				//    ReactHotModuleReplacement = true
				//});
			}
			else
			{
				app.UseExceptionHandler("/Home/Error");
			}

			app.UseStaticFiles();

			app.UseMvc(routes =>
			{
				routes.MapRoute(
					name: "default",
					template: "{controller=Home}/{action=Index}/{id?}");

				routes.MapSpaFallbackRoute(
					name: "spa-fallback",
					defaults: new { controller = "Home", action = "Index" });
			});
		}
	}
}
