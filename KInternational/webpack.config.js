const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CheckerPlugin = require('awesome-typescript-loader').CheckerPlugin;
const bundleOutputDir = './wwwroot/dist';
const TerserPlugin = require('terser-webpack-plugin');

module.exports = (env) => {
	const isDevBuild = !(env && env.prod);
	return [{
		stats: { modules: false },
		entry: { 'main': './ClientApp/boot.tsx' },
		resolve: {
			extensions: ['.js', '.jsx', '.ts', '.tsx'],
			alias: {
				globalshare: path.resolve(__dirname, 'ClientApp/shared/'),
				globalmodels: path.resolve(__dirname, 'ClientApp/models/'),
				globalcomponents: path.resolve(__dirname, 'ClientApp/global-components/'),
				globalservices: path.resolve(__dirname, 'ClientApp/services/'),
				appcomponents: path.resolve(__dirname, 'ClientApp/components/')
			}
		},
		output: {
			path: path.join(__dirname, bundleOutputDir),
			filename: '[name].js',
			chunkFilename: '[name]-[chunkhash].js',
			publicPath: 'dist/'
		},
		module: {
			rules: [
				{ test: /\.tsx?$/, include: /ClientApp/, use: 'awesome-typescript-loader?silent=true' },
				{
					test: /\.scss$/,
					use: [
						{
							loader: "style-loader"
						},
						{
							loader: "css-loader",
							options: {
								sourceMap: true
							}
						},
						{
							loader: "sass-loader",
							options: {
								sourceMap: true,
							}
						}
					]
				},
				{
					test: /\.gif/,
					loader: 'url-loader?limit=10000&mimetype=image/gif'
				},
				{
					test: /\.jpg/,
					loader: 'url-loader?limit=10000&mimetype=image/jpg'
				},
				{
					test: /\.png/,
					loader: 'url-loader?limit=10000&mimetype=image/png'
				},
				{
					test: /\.svg/,
					loader: 'url-loader?limit=10000&mimetype=image/svg+xml'
				}
			]
		},
		plugins: [
			new CheckerPlugin(),
			new webpack.DllReferencePlugin({
				context: __dirname,
				manifest: require('./wwwroot/dist/vendor-manifest.json')
			})
		].concat(
			isDevBuild ? [
				// Plugins that apply in development builds only
				new webpack.SourceMapDevToolPlugin({
					filename: '[file].map', // Remove this line if you prefer inline source maps
					moduleFilenameTemplate: path.relative(bundleOutputDir, '[resourcePath]') // Point sourcemap entries to the original file locations on disk
				})
			] : []),
		optimization: {
			minimizer: [].concat(
				isDevBuild ? [] : [
					new TerserPlugin({
						cache: true,
						parallel: true,
						terserOptions: {
							ecma: 6,
							mangle: true
						}
					})
				]
			)
		}
	}];
};