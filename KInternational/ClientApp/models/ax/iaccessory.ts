﻿export interface IAccessory {
	AccessoryID: number;
	AccessoryName: string;
	AccessoryDesc: string;
	AccesoryPartNumber:string;
	IsColorSpecific: boolean;
	IsActive: boolean;
	IsHidden: boolean;
	IsRequiresOptional: boolean;
	ImageUrl: string;
	Price: number;
	PreviousPrice:number;
	DisplayPrice: string;
	CatID: number;
	IncludedPackageID: number;
	IsSupportingAccessory: boolean;
	IsHideSummary: boolean;
	ColorGroupId: number;
	IsShowInList: boolean;
	ItemGroupID: number;
	OptionalType: number;
	DependantIDs: Array<number>;
	ConflictIDs: Array<number>;
	PackageExclusionIDs: Array<number>;
	InValidColorIDs: Array<number>;
	IncludedPackageIDs: Array<number>;
	RequiredOptionalAccessories: Array<number>;
	QTY: number;
	IsInWishList: boolean;
	IsInCart: boolean;
	IsAvailableForPurchase: boolean;
	indexFront: string;
	indexRear: string;
	FrontImageOverlay: string;
	RearImageOverlay: string;
}

export interface IAccessoryJsonResult {
	readonly Accessories:Array<IAccessory>;
}