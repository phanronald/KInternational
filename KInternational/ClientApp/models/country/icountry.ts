﻿export interface ICountry {
	Id: string;
	CountryId: number;
	Name: string;
	UrlKey: string;
}