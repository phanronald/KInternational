﻿
export interface IBingMaps {
	Latitude: string;
	Longitude: string;
	Title: string;
	SubTitle: string;
}

export interface IBingMapsState {
	
}