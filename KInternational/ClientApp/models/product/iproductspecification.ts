﻿export interface IProductSpecification {

	Id: string;
	ProductSpecId: number;
	ProductId: number;
	SpecId: number;
	SpecValue: string;
	IsFeatured: boolean;
}