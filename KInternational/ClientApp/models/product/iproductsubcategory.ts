﻿export interface IProductSubCategory {

	Id: string;
	ProductSubCategoryId: number;
	ProductCategoryId: number;
	Name: string;
	IsActive: boolean;
	SortOrder: number;
}