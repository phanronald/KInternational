﻿export interface IProductColorPhotos {

	Id: string;
	ProductColorPhotoId: number;
	ProductId: number;
	ColorId: number;
	ImageUrl: string;
}