﻿export interface ISpecGroup {

    Id: string;
    SpecGroupId: number;
    Name: string;
    SortOrder: number;
}