﻿export interface IProduct {

	Id: string;
	ProductId: number;
	CategoryId: number;
	SubCategoryId: number;
	Name: string;
	Year: number;
	UrlKey: string;
	IsActive: boolean;
	IsNavigation: boolean;
	NavImageUrl: string;
	SortOrder: number;
}