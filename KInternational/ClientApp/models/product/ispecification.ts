﻿export interface ISpecification {

    Id: string;
    ProductSpecId: number;
    SpecGroupId: number;
    Name: string;
    SortOrder: number;
    IsDisplayed: boolean;
}