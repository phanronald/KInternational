﻿export interface IProductCategory {

	Id: string;
	ProductCategoryId: number;
	Name: string;
	SortOrder: number;
}