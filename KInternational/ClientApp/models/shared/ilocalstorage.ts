﻿
import { IProductCategory } from 'globalmodels/product/iproductcategory';
import { IProductSubCategory } from 'globalmodels/product/iproductsubcategory';
import { IProduct } from 'globalmodels/product/iproduct';

export interface ILocalStoraged {
	categories: IProductCategory[] | null;
	subCategories: IProductSubCategory[] | null;
	products: IProduct[] | null;
}