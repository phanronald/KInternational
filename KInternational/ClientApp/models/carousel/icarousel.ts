﻿
export interface ICarousel {
	SelectedIndex: number;
}

export interface ICarouselState {
	CarouselIndex: number;
}