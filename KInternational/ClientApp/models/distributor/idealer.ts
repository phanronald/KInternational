﻿import { IContactInfo } from 'globalmodels/distributor/icontactinfo';

export interface IDealer extends IContactInfo {
	Id: string;
	DealerId: number;
	ContactName: string;
}