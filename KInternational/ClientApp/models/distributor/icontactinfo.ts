﻿
import { IAddressInfo } from './iaddressinfo';
import { ISocialMedia } from './isocialmedia';

export interface IContactInfo {
	CountryId: number;
	Name: string;
	Email: string;
	Phone: string;
	Fax: string;
	Address: IAddressInfo;
	Website: string;
	Latitude: string;
	Longitude: string;
	SocialMedia: ISocialMedia;
}