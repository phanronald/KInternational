﻿
export interface ISocialMedia {
	Facebook: string;
	FacebokId: string;
	Twitter: string;
	TwitterId: string;
	Youtube: string;
	YoutubeAuthor: string;
	Instagram: string;
	InstagramId: string;
}