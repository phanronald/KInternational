﻿
export interface IContactFormValidation {

	IsValid: boolean;
	IsEmailValid?: boolean;
	IsPhoneValid?: boolean;
	HTMLElementId: string;
	HTMLElementName: string;
}