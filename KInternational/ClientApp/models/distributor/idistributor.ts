﻿import { IContactInfo } from 'globalmodels/distributor/icontactinfo';

export interface IDistributor extends IContactInfo {
	Id: string;
	DistributorId: number;
	DistributorSite: string;
	MapImage: string;
}