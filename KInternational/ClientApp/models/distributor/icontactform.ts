﻿
export interface IContactForm {
	DistributorId: number;
	RequestType: string;
	FirstName: string;
	LastName: string;
	Email: string;
	Phone: string;
	MobilePhone: string;
	City: string;
	VehicleModel: string;
	VehicleYear: string;
	Ownership: string;
	InterestInKawasakiProductsMoto: boolean;
	InterestInKawasakiProductsATV: boolean;
	InterestInKawasakiProductsJetSki: boolean;
	InterestInKawasakiProductsMule: boolean;
	InterestInKawasakiProductsTeryx: boolean;
	Comments: string;
}
