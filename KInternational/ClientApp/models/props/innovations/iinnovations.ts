﻿export interface IInnovationsPage {

}

export interface IInnovationState {
	IsVideoModalOpen: boolean;
	InnovationDescription: string;
	SlideNumber: number;
	InnovationTitle: string;
}

export interface IInnovationDataObj {
	Innovation: IInnovationData[];
}

export interface IInnovationData {
	SlideNumber: number;
	Description: string;
	Title: string;
}