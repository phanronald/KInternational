﻿import { IBasicPage } from 'globalmodels/shared/ibasicpage';
import { ILink } from "globalmodels/shared/ilink";
import { IBasicContact } from 'globalmodels/shared/ibasiccontact';

export interface IAbout {

	Id: string;
	HeaderText: string;
	HeaderImage: string;
	Overview: string;
	Detailed: string;
	Contact: IBasicContact;
	MainAddress: string;
	MailAddress: string;
	Phone: string;
	AboutLinks: ILink[];
}

export interface IAboutState extends IBasicPage {
	AboutData: IAbout | null;
}