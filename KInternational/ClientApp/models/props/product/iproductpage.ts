﻿import { RouteComponentProps } from 'react-router-dom';
import { IBasicPage } from 'globalmodels/shared/ibasicpage';
import { IFourZeroFour } from 'globalmodels/shared/ifourzerofour';

import { IProduct } from 'globalmodels/product/iproduct';
import { IProductColorPhotos } from 'globalmodels/product/iproductcolorphotos';
import { IProductSpecification } from 'globalmodels/product/iproductspecification';

export interface IFeaturedSpecifications {
	SpecificationTitle: string;
	SpecificationValue: string;
}

export interface IColorPhotoColor {
	PhotoImageUrl: string;
	ColorImageUrl: string;
	ColorName: string;
	ColorId: number;
}

export interface IProductPage extends RouteComponentProps<any> {


}

export interface IProductState extends IFourZeroFour, IBasicPage {

	Product: IProduct;
	ProductInfo: IProductInfoViewModel;
	SelectedColorPhoto: IColorPhotoColor; 
	OpenSnackbar: boolean;
}

export interface IProductInfoViewModel {
	ColorPhotos: IColorPhotoColor[];
	FeaturedSpecifications: IFeaturedSpecifications[];
}