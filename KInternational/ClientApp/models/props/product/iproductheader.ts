﻿
export interface IProductHeader {
	ProductName: string;
	ProductYear: number;
	ProductUrlKey: string;
	IsShowroom: boolean;
	IsFeature: boolean;
	IsSpec: boolean;
	IsPhotoGallery: boolean;
	IsVideoGallery: boolean;
}

export interface IProductHeaderState {
	
}