﻿import { RouteComponentProps } from 'react-router-dom';
import { IBasicPage } from 'globalmodels/shared/ibasicpage';
import { IFourZeroFour } from 'globalmodels/shared/ifourzerofour';

import { IProduct } from 'globalmodels/product/iproduct';


export interface IProductFeaturesPage extends RouteComponentProps<any> {


}

export interface IProductFeaturesState extends IFourZeroFour, IBasicPage {
	Product: IProduct;
}