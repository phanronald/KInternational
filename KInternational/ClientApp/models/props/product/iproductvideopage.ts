﻿import { RouteComponentProps } from 'react-router-dom';
import { IBasicPage } from 'globalmodels/shared/ibasicpage';
import { IFourZeroFour } from 'globalmodels/shared/ifourzerofour';

import { IProduct } from 'globalmodels/product/iproduct';

export interface IProductVideoPage extends RouteComponentProps<any> {


}

export interface IProductVideoState extends IFourZeroFour, IBasicPage {
	Product: IProduct;
	SelectedVideo: IProductVideo;
	AllVideosForProduct: Array<IProductVideo>;
}

export interface IProductVideo {

	Id: string;
	ProductId: number;
	FileName: string;
	ThumbnailName: string;
	Title: string;
}