﻿export enum ProductSection {
    None = 0,
    Showroom = 1,
	Features = 2,
	Specifications = 3,
	Photos = 4,
	Videos = 5
}