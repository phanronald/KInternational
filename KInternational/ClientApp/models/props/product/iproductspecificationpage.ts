﻿import { RouteComponentProps } from 'react-router-dom';
import { IBasicPage } from 'globalmodels/shared/ibasicpage';
import { IFourZeroFour } from 'globalmodels/shared/ifourzerofour';

import { IProduct } from 'globalmodels/product/iproduct';
import { ISpecGroup } from 'globalmodels/product/ispecgroup';

export interface IProductSpecPage extends RouteComponentProps<any> {

}

export interface IProductSpecState extends IFourZeroFour, IBasicPage, IProductSpecificationViewModel {
	Product: IProduct;
}

export interface IProductSpecificationViewModel {
	ProductSpecGroupWithSpecs: IProductSpecGroupWithSpecs[]
	SpecificationGroups: ISpecGroup[];
}

export interface IProductSpecGroupWithSpecs {
	SpecGroupId: number;
	SpecName: string;
	SpecValue: string;
}