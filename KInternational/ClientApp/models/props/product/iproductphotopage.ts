﻿import { RouteComponentProps } from 'react-router-dom';
import { IBasicPage } from 'globalmodels/shared/ibasicpage';
import { IFourZeroFour } from 'globalmodels/shared/ifourzerofour';

import { IProduct } from 'globalmodels/product/iproduct';

export interface IProductPhotoPage extends RouteComponentProps<any> {


}

export interface IProductPhotoState extends IFourZeroFour, IBasicPage {
	Product: IProduct;
	SelectedImage: IProductPhoto;
	AllPhotosForProduct: Array<IProductPhoto>;
}

export interface IProductPhoto {

	Id: string;
	ProductId: number;
	FileName: string;
	Title: string;
}