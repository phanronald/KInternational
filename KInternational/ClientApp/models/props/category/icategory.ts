﻿import { RouteComponentProps } from 'react-router-dom';
import { IBasicPage } from 'globalmodels/shared/ibasicpage';
import { IFourZeroFour } from 'globalmodels/shared/ifourzerofour';
import { IProduct } from 'globalmodels/product/iproduct';
import { IProductSubCategory } from 'globalmodels/product/iproductsubcategory';

export interface ICategory extends RouteComponentProps<any> {

}

export interface ICategoryState extends IFourZeroFour, IBasicPage {
	ProductsToShow: IProduct[];
	SubCategoriesToShow: IProductSubCategory[];
	CategorySelected: string;
	CurrentCategoryName: string;
	AllProductsPerCategory: IProduct[];
}