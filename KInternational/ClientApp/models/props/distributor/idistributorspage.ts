﻿import { RouteComponentProps } from 'react-router-dom';
import { IBasicPage } from 'globalmodels/shared/ibasicpage';
import { IFourZeroFour } from 'globalmodels/shared/ifourzerofour';
import { ICountry } from 'globalmodels/country/icountry';
import { IDistributor } from 'globalmodels/distributor/idistributor';
import { IDealer } from 'globalmodels/distributor/idealer';

export interface IDistributorsPage extends RouteComponentProps<any> {

}

export interface IDistributorsState extends IFourZeroFour, IBasicPage {

	CountryDistributorViewModel: IDistributorDealerViewModel;
	SelectedCountryString: string;
	SelectedCountry: ICountry;
	SelectedDistributor: IDistributor;
	SelectedDealers: IDealer[];
	SelectedDealer: IDealer;
	IsMapOpen: boolean;
}

export interface IDistributorDealerViewModel {
	AllDistributors: IDistributor[];
	AllDealers: IDealer[];
}

export interface ISelectedCountryDistributorDealer {
	SelectedCountry: ICountry;
	SelectedDistributor: IDistributor;
	SelectedDealers: IDealer[];
}