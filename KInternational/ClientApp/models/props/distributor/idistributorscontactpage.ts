﻿
import { RouteComponentProps } from 'react-router-dom';
import { IBasicPage } from 'globalmodels/shared/ibasicpage';
import { IFourZeroFour } from 'globalmodels/shared/ifourzerofour';
import { IDistributor } from 'globalmodels/distributor/idistributor';

export interface IDistributorContactPage extends RouteComponentProps<any> {

}

export interface IDistributorContactState extends IFourZeroFour, IBasicPage {
	ContactDistributor: IDistributor;
	ContactRequestType: string;
	ContactFirstName: string;
	ContactLastName: string;
	ContactEmail: string;
	ContactPhone: string;
	ContactMobilePhone: string;
	ContactCity: string;
	ContactVehicleModel: string;
	ContactVehicleYear: string;
	ContactOwnership: string;
	ContactComments: string;
	InterestMoto: boolean;
	InterestATV: boolean;
	InterestJetSki: boolean;
	InterestMule: boolean;
	InterestTeryx: boolean;
}