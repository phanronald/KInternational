﻿import { TechnologyService } from "./../../../components/technology/services/technology.service";

export interface ITechnologyHeader {
	service: TechnologyService;
}

export interface ITechnologyHeaderState {
	IsEngineActive: boolean;
	IsChassisActive: boolean;
}