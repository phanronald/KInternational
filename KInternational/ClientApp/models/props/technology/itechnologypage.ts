﻿import { RouteComponentProps } from 'react-router-dom';
import { IBasicPage } from 'globalmodels/shared/ibasicpage';
import { IFourZeroFour } from 'globalmodels/shared/ifourzerofour';

export interface ITechnologyPage extends RouteComponentProps<any> {

}

export interface ITechnologyState extends IFourZeroFour, IBasicPage {

	IsLandingPage: boolean;
	TechnologyDetail: ITechnology[];
	IsModalOpen: boolean;
	SelectedTechnologyIndex: number;
}

export enum MediaType {
	None = 0,
	Image = 1,
	Video = 2
}

export interface IMedia {
	Image: string;
	Video: string;
	Type: number;
}

export interface IProductAvailable {
	Name: string;
	Link: string;
}

export interface ITechnology {

	Id: string;
	Name: string;
	TechType: number;
	Image: string;
	Description: string;
	Media: IMedia[];
	ProductAvailable: IProductAvailable[];
	TruncatedDescription: string;
}