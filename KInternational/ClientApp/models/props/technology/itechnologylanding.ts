﻿import { TechnologyService } from "./../../../components/technology/services/technology.service";

export interface ITechnologyLanding {
    service: TechnologyService;
}