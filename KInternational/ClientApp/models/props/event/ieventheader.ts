﻿
import { ICountry } from "globalmodels/country/icountry";
import { EventService } from "./../../../components/event/services/event.service";

export interface IEventHeader {
	service: EventService;
}

export interface IEventHeaderState {
	IsGlobal: boolean;
	IsGlobalRacing: boolean;
	IsGlobalConsumer: boolean;
	IsRegional: boolean;
	IsRegionalRacing: boolean;
	IsRegionalConsumer: boolean;
	CountriesToDisplay: ICountry[];
	CountrySelected: number;
}