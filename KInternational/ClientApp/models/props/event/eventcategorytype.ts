﻿export enum EventCategoryType {
	None = 0,
	Racing = 1,
	General = 2,
	Consumer = 3
}
