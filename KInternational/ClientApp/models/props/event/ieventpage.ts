﻿import { RouteComponentProps } from 'react-router-dom';
import { IBasicPage } from 'globalmodels/shared/ibasicpage';
import { IFourZeroFour } from 'globalmodels/shared/ifourzerofour';

export interface IEventPage extends RouteComponentProps<any> {

}

export interface IEventPageState extends IFourZeroFour, IBasicPage {

	IsLandingPage: boolean;
	IsEventTypeLanding: boolean;
	IsCatgoryTypeLanding: boolean;
	EventsToDisplay: IEvent[];
}

export interface IEvent {
	Id: string;
	Title: string;
	Image: string;
	Description: string;
	Link: string;
	EventType: number;
	CategoryType: number;
	CountryId: number[];
}