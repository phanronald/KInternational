﻿export enum EventType {
	None = 0,
	Regional = 1,
	Global = 2
}