﻿
import { ModalService } from 'globalservices/modal/modal.service';

export interface IModal {
	open: boolean;
	service: ModalService;
	className?: string;
}

export interface IModalState {
	IsModalOpen: boolean;
}