﻿
import { Subject, Observable } from 'rxjs';

export class ModalService {

	protected emitOpenCloseModal = new Subject<boolean>();

	public openCloseModalEmitted = this.emitOpenCloseModal.asObservable();

	constructor() {

	}

	public applyOpenCloseModalEmitted = (isOpenModal:boolean): void => {
		this.emitOpenCloseModal.next(isOpenModal);
	}
}