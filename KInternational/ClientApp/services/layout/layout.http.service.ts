﻿
import { Observable, forkJoin } from 'rxjs';
import { CommonService } from 'globalservices/shared/common.service';

import { ICountry } from 'globalmodels/country/icountry';
import { IProductCategory } from 'globalmodels/product/iproductcategory';
import { IProductSubCategory } from 'globalmodels/product/iproductsubcategory';
import { IProduct } from 'globalmodels/product/iproduct';

export class LayoutHttpService {


	public static InitData = (): Observable<[ICountry[], IProductCategory[], IProductSubCategory[], IProduct[]]> => {

		var countryObservable: Observable<ICountry[]> = CommonService.AjaxGet('/api/Countries/GetAllCountries');
		var categoriesObservable: Observable<IProductCategory[]> = CommonService.AjaxGet('/api/Categories/GetAllCategories');
		var subCategoriesObservable: Observable<IProductSubCategory[]> = CommonService.AjaxGet('/api/SubCategories/GetAllSubCategories');
		var productsObservable: Observable<IProduct[]> = CommonService.AjaxGet('/api/Products/GetAllProducts');

		return forkJoin([countryObservable, categoriesObservable, subCategoriesObservable, productsObservable]);
	}

	public static GetAntiXsrfToken = (): Observable<string> => {

		return CommonService.AjaxGet('/api/Security/GetAntiXsrfRequestToken');

	}
}