
import { Collection } from 'globalshare/core/Collection';
import { Dictionary } from 'globalshare/core/Dictionary';
import { ICountry } from 'globalmodels/country/icountry';
import { IDistributor } from 'globalmodels/distributor/idistributor';
import { IDealer } from 'globalmodels/distributor/idealer';
import { IAddressInfo } from 'globalmodels/distributor/iaddressinfo';
import { ISocialMedia } from 'globalmodels/distributor/isocialmedia';
import { IContactForm } from 'globalmodels/distributor/icontactform';
import { IContactFormValidation } from 'globalmodels/distributor/icontactformvalidation';
import { ISelectedCountryDistributorDealer } from 'globalmodels/props/distributor/idistributorspage';

export class DistributorService {

	public static CreateErrorElement = (isValid: boolean, isEmailValid: boolean,
										isPhoneValid: boolean, htmlElementId: string,
										htmlElementName: string): void => {

		let tempSpan = document.createElement('span');
		if (!isValid && isEmailValid === null && isPhoneValid == null) {
			tempSpan.textContent = htmlElementName + ' is required.';
		}
		else if (isEmailValid !== null && !isEmailValid) {
			tempSpan.textContent = htmlElementName + ' is not valid.';
		}
		else if (isPhoneValid !== null && !isPhoneValid) {
			tempSpan.textContent = htmlElementName + ' is not valid phone.';
		}

		tempSpan.className = 'validation-error';

		const inputElement = (document.getElementById(htmlElementId) as HTMLElement);
		const inputParentNode = inputElement.closest('.form-input');
		inputParentNode.appendChild(tempSpan);
	}

	public static DetermineSelectedCountryString = (countryName: string): string => {

		let dynamicParamterCountry = countryName;
		if (dynamicParamterCountry === undefined || dynamicParamterCountry === null) {
			dynamicParamterCountry = "all";
		}

		return dynamicParamterCountry;
	}

	public static DetermineSelectedDistributor = (selectedCountry: string,
		countriesCollection: ICountry[], distributorsAvailable: IDistributor[],
		dealersAvailable: IDealer[]): ISelectedCountryDistributorDealer => {

		let foundDistributor: IDistributor = null;
		let foundCountry: ICountry = null;
		let foundDealersArr: IDealer[] = [];
		if (selectedCountry !== "all") {
			const foundCountries: Collection<ICountry> = new Collection(countriesCollection).Where(x => x.UrlKey.toLowerCase() === selectedCountry);
			if (foundCountries.Any()) {
				foundCountry = foundCountries.FirstOrDefault();
			}
		}

		const distributorCollection: Collection<IDistributor> = new Collection(distributorsAvailable);
		if (foundCountry !== undefined && foundCountry !== null) {
			const foundDistributors: Collection<IDistributor> = distributorCollection.Where(x => x.CountryId === foundCountry.CountryId);
			if (foundDistributors.Any()) {
				foundDistributor = foundDistributors.FirstOrDefault();
			}
		}

		const dealerCollection: Collection<IDealer> = new Collection(dealersAvailable);
		if (foundCountry !== undefined && foundCountry !== null) {
			const foundDealers: Collection<IDealer> = dealerCollection.Where(x => x.CountryId === foundCountry.CountryId);
			foundDealersArr = foundDealers.ToArray();
		}

		return (({
			SelectedCountry: foundCountry,
			SelectedDistributor: foundDistributor,
			SelectedDealers: foundDealersArr
		}) as ISelectedCountryDistributorDealer);
	}

	public static GetCorrectDisplayOfAddress = (address: IAddressInfo): string => {
		let addressFinal: string = "";
		if (address.Address1 !== "") {
			addressFinal = address.Address1;
		}

		addressFinal += '<br />';

		addressFinal += address.City + ", ";
		if (address.StateProvince !== "") {
			addressFinal += address.StateProvince + ", ";
		}

		return addressFinal;
	}

	public static GetCorrectDisplayOfSocialMedia = (socialMedia: ISocialMedia): string => {

		let finalSocialMedia: string = "";
		if (socialMedia.Facebook !== "") {
			finalSocialMedia += "<a href=\"" + socialMedia.Facebook + "\">Facebook</a>";
		}

		if (socialMedia.Twitter !== "") {
			finalSocialMedia += "<a href=\"" + socialMedia.Twitter + "\">Twitter</a>";
		}

		if (socialMedia.Youtube !== "") {
			finalSocialMedia += "<a href=\"" + socialMedia.Youtube + "\">Youtube</a>";
		}

		if (socialMedia.Instagram !== "") {
			finalSocialMedia += "<a href=\"" + socialMedia.Instagram + "\">Instagram</a>";
		}

		return finalSocialMedia;
	}

	public static RemoveErrorMessageElement = (elementId: string): void => {

		const inputElement = (document.getElementById(elementId) as HTMLElement);
		const inputParentNode = inputElement.closest('.form-input');

		const allSpansInParent = (inputParentNode as HTMLElement).querySelectorAll('.validation-error');

		for (let i = 0; i < allSpansInParent.length; i++) {
			inputParentNode.removeChild(allSpansInParent[i]);
		}
	}

	public static SetUpBingMaps = (bingMapUrl: string): HTMLScriptElement => {

		let bingMapUrlScript: HTMLScriptElement = document.querySelector('script[src="' + bingMapUrl + '"]');
		if (bingMapUrlScript === null) {

			bingMapUrlScript = document.createElement('script');
			bingMapUrlScript.type = 'text/javascript';
			bingMapUrlScript.async = true;
			bingMapUrlScript.defer = true;
			bingMapUrlScript.src = bingMapUrl;
			document.getElementsByTagName('head')[0].appendChild(bingMapUrlScript);
		}

		return bingMapUrlScript;
	}

	public static ValidateFormItem = (htmlType: string, elementId: string, elementName: string): IContactFormValidation => {

		let validationDistro: IContactFormValidation = { IsValid: false, IsEmailValid: null, HTMLElementId: elementId, HTMLElementName: elementName };

		switch (htmlType.toLowerCase()) {
			case "text":
			case "textarea": {

				validationDistro.IsValid = !(document.getElementById(elementId) as HTMLInputElement).validity.valueMissing;
				break;
			}
			case "tel": {
				const telephoneInput = (document.getElementById(elementId) as HTMLInputElement);
				let phoneRegex = /^([+]?\d{1,2}[.-\s]?)?(\d{3}[.-]?){2}\d{4}$/;
				validationDistro.IsPhoneValid = phoneRegex.test(telephoneInput.value);
				break;
			}
			case "email": {

				const emailInputElement = (document.getElementById(elementId) as HTMLInputElement).validity;
				validationDistro.IsValid = !emailInputElement.valueMissing;
				if (validationDistro.IsValid) {
					validationDistro.IsEmailValid = emailInputElement.valid;
				}
				break;
			}
			case "select": {

				validationDistro.IsValid = !(document.getElementById(elementId) as HTMLSelectElement).validity.valueMissing;
				break;
			}
		}

		return validationDistro;

	}

	public static ValidateContactForm = (): Dictionary<string, IContactFormValidation> => {

		let validationDictionary = new Dictionary<string, IContactFormValidation>();

		const requestTypeElement = (document.getElementById('request-type-native-helper') as HTMLSelectElement);
		const requestTypeValidation = requestTypeElement.validity.valueMissing;
		validationDictionary.AddKeyValue('request-type-native-helper', { IsValid: !requestTypeValidation, IsEmailValid: null, HTMLElementId: requestTypeElement.id, HTMLElementName: requestTypeElement.name });

		const firstNameElement = (document.getElementById('firstNameId') as HTMLInputElement);
		const firstNameValidation = firstNameElement.validity.valueMissing;
		validationDictionary.AddKeyValue('firstNameId', { IsValid: !firstNameValidation, IsEmailValid: null, HTMLElementId: firstNameElement.id, HTMLElementName: firstNameElement.name });

		const lastNameElement = (document.getElementById('lastNameId') as HTMLInputElement);
		const lastNameValidation = lastNameElement.validity.valueMissing;
		validationDictionary.AddKeyValue('lastNameId', { IsValid: !lastNameValidation, IsEmailValid: null, HTMLElementId: lastNameElement.id, HTMLElementName: lastNameElement.name });

		const cityElement = (document.getElementById('cityId') as HTMLInputElement);
		const cityValidation = cityElement.validity.valueMissing;
		validationDictionary.AddKeyValue('cityId', { IsValid: !cityValidation, IsEmailValid: null, HTMLElementId: cityElement.id, HTMLElementName: cityElement.name });

		const vehicleModelElement = (document.getElementById('vehicleModelId') as HTMLInputElement);
		const vehicleModel = vehicleModelElement.validity.valueMissing;
		validationDictionary.AddKeyValue('vehicleModelId', { IsValid: !vehicleModel, IsEmailValid: null, HTMLElementId: vehicleModelElement.id, HTMLElementName: vehicleModelElement.name });

		const commentsElement = (document.getElementById('commentsId') as HTMLInputElement);
		const commentsValidation = commentsElement.validity.valueMissing;
		validationDictionary.AddKeyValue('commentsId', { IsValid: !commentsValidation, IsEmailValid: null, HTMLElementId: commentsElement.id, HTMLElementName: commentsElement.name });

		const emailElement = (document.getElementById('emailId') as HTMLInputElement);
		let emailValidation = emailElement.validity.valueMissing;
		if (emailValidation) {
			validationDictionary.AddKeyValue('emailId', { IsValid: !emailValidation, IsEmailValid: null, HTMLElementId: emailElement.id, HTMLElementName: emailElement.name });
		}
		else {
			emailValidation = emailElement.validity.valid;
			validationDictionary.AddKeyValue('emailId', { IsValid: emailValidation, IsEmailValid: emailValidation, HTMLElementId: emailElement.id, HTMLElementName: emailElement.name });
		}

		return validationDictionary;
	}
}
