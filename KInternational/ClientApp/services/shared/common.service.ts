import { Observable } from 'rxjs';
import { Collection } from 'globalshare/core/Collection';

export class CommonService {

	public static AjaxGet = (url: string): Observable<any> => {

		return Observable.create((observer: any) => {

			let xhr: XMLHttpRequest = new XMLHttpRequest();
			xhr.open('get', url, true);
			xhr.responseType = 'json';

			xhr.onreadystatechange = () => {

				if (xhr.readyState === XMLHttpRequest.DONE) {
					observer.next(xhr.response);
					observer.complete();
				}
			};

			xhr.send(null);
		});
	}

	public static AjaxPost = (url: string, data: string, xsrfToken: string = ''): Observable<any> => {

		return Observable.create((observer: any) => {

			let xhr: XMLHttpRequest = new XMLHttpRequest();
			xhr.open('post', url, true);
			xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
			if (xsrfToken.trim() !== '') {
				xhr.setRequestHeader('__RequestVerificationToken', xsrfToken);
			}

			xhr.onreadystatechange = () => {

				if (xhr.readyState === XMLHttpRequest.DONE) {
					observer.next(xhr.response);
					observer.complete();
				}
			};

			xhr.send(data);
		});
	}
}
