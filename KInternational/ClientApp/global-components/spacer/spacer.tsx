﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX

import * as React from "react";

export class SpacerComponent extends React.Component<{}, {}> {

	public componentDidMount() {

	}

	public componentWillUnmount() {

	}

	render() {

		return (
			<>
				<div className="toolbar-placeholder"></div>
			</>
		);

	}
}