﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX

import * as React from "react";
import * as ReactDOM from "react-dom";
import { Link } from 'react-router-dom';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Hidden from '@material-ui/core/Hidden';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';

import { Consts } from 'globalshare/consts';
import { StorageUtility } from 'globalshare/core/Storage';

import { IProductCategory } from 'globalmodels/product/iproductcategory';
import { IHeaderSection, IHeaderState } from "globalmodels/shared/navigation";

import './header.component.scss';

export class HeaderComponent extends React.Component<IHeaderSection, IHeaderState> {

	private legalLink = (props: any) => <Link to="/legal" {...props} />;
	private aboutLink = (props: any) => <Link to="/about" {...props} />;
	private innovationsLink = (props: any) => <Link to="/innovations" {...props} />;
	private technologyLink = (props: any) => <Link to="/technology" {...props} />;
	private distributorsAllLink = (props: any) => <Link to="/distributors" {...props} />;
	private eventsLink = (props: any) => <Link to="/events" {...props} />;

	private categories: IProductCategory[] | null = [];

	constructor(props: IHeaderSection) {
		super(props);

		this.categories = StorageUtility.GetLocal(Consts.CATEGORY_KEY);

		this.state = {
			openNavigation: false
		};
	}

	public componentDidMount() {

	}

	public componentWillUnmount() {

	}

	private renderMenuLink = (category: IProductCategory): ((props: any) => JSX.Element) => {
		const url: string = '/category/' + category.Name.toLowerCase();
		const categoryLink = (props: any) => <Link to={url} {...props} />;
		return categoryLink;
	}

	private renderMenuIcon = (category: IProductCategory): any => {

		switch (category.Name.toLowerCase()) {
			case "motorcycle": {
				return <Button>
					<Avatar alt={category.Name.toLowerCase()} src="/icons/moto.svg" className="drawer-icon" />
				</Button>;
			}
			case "mule": {
				return <Button>
					<Avatar alt={category.Name.toLowerCase()} src="/icons/mule.svg" className="drawer-icon" />
				</Button>;
			}
			case "teryx": {
				return <Button>
					<Avatar alt={category.Name.toLowerCase()} src="/icons/teryx.svg" className="drawer-icon" />
				</Button>;
			}
			case "atv": {
				return <Button>
					<Avatar alt={category.Name.toLowerCase()} src="/icons/atv.svg" className="drawer-icon" />
				</Button>;
			}
			case "jet ski": {
				return <Button>
					<Avatar alt={category.Name.toLowerCase()} src="/icons/jetski.svg" className="" />
				</Button>;
			}
		}

	}

	private handleDrawerToggle = (event: (React.MouseEvent<HTMLElement> | React.SyntheticEvent<any>), shouldOpen: boolean): void => {

		this.setState({
			openNavigation: shouldOpen
		});

	}

	render() {

		const { openNavigation } = this.state;

		return (
			<>
				<AppBar position="absolute" className={"app-bar " + (openNavigation ? "app-bar-shift" : "")}>
					<Toolbar disableGutters={!openNavigation}>
						<IconButton color="inherit" aria-label="open drawer"
							onClick={event => this.handleDrawerToggle(event, true)}
							className={"app-menu-icon " + (openNavigation ? "hide" : "")}>

							<MenuIcon />
						</IconButton>
						<Link to="/" className="header-home-link">
							<Typography variant="title" color="inherit" noWrap>
								Kawasaki
							</Typography>
						</Link>
					</Toolbar>
				</AppBar>
				<Hidden smDown implementation="css">
					<Drawer classes={{ paper: "drawer-paper " + (!openNavigation ? "drawer-paper-close" : "") }}
						variant="permanent" open={openNavigation}>

						<div className="app-menu-toolbar">
							<IconButton onClick={event => this.handleDrawerToggle(event, false)}>
								<ChevronLeftIcon />
							</IconButton>
						</div>
						<Divider />
						<List>
							<div>
								{
									this.categories !== null && (this.categories as IProductCategory[]).map((category: IProductCategory, index: number) => {

										return (
											<ListItem button key={index} className="drawer-menu-list-item" component={this.renderMenuLink(category)}>
												{
													this.renderMenuIcon(category)
												}
												<ListItemText>{category.Name}</ListItemText>
											</ListItem>
										)
									})
								}
							</div>
						</List>
						<Divider />
						<List>
							<ListItem button className="drawer-menu-list-item" component={this.eventsLink}>
								<Button>
									<Avatar alt="Events" src="/icons/events.svg" className="drawer-icon" />
								</Button>
								<ListItemText>Events</ListItemText>
							</ListItem>
							<ListItem button className="drawer-menu-list-item" component={this.distributorsAllLink}>
								<Button>
									<Avatar alt="Distributors" src="/icons/distributor.svg" className="drawer-icon" />
								</Button>
								<ListItemText>Distributors</ListItemText>
							</ListItem>
							<ListItem button className="drawer-menu-list-item" component={this.innovationsLink}>
								<Button>
									<Avatar alt="Innovations" src="/icons/innovation.svg" className="drawer-icon" />
								</Button>
								<ListItemText>Innovations</ListItemText>
							</ListItem>
							<ListItem button className="drawer-menu-list-item" component={this.technologyLink}>
								<Button>
									<Avatar alt="Technology" src="/icons/technology.svg" className="drawer-icon" />
								</Button>
								<ListItemText>Technology</ListItemText>
							</ListItem>
							<ListItem button className="drawer-menu-list-item" component={this.legalLink}>
								<Button>
									<Avatar alt="Legal/Privacy" src="/icons/legal.svg" className="drawer-icon" />
								</Button>
								<ListItemText>Legal/Privacy</ListItemText>
							</ListItem>
							<ListItem button className="drawer-menu-list-item" component={this.aboutLink}>
								<Button>
									<Avatar alt="About" src="/icons/about.svg" className="drawer-icon" />
								</Button>
								<ListItemText>About</ListItemText>
							</ListItem>
						</List>
					</Drawer>
				</Hidden>
				<Hidden mdUp>
					<Drawer classes={{ paper: "drawer-paper " + (!openNavigation ? "drawer-paper-close" : "") }}
						variant="temporary" open={openNavigation} anchor="left"
						ModalProps={{ keepMounted: true }} onClose={event => this.handleDrawerToggle(event, false)}>

						<List>
							<div>
								{
									this.categories !== null && (this.categories as IProductCategory[]).map((category: IProductCategory, index: number) => {

										return (
											<ListItem button key={index} className="drawer-menu-list-item" component={this.renderMenuLink(category)}>
												{
													this.renderMenuIcon(category)
												}
												<ListItemText>{category.Name}</ListItemText>
											</ListItem>
										)
									})
								}
							</div>
						</List>
						<Divider />
						<List>
							<ListItem button className="drawer-menu-list-item" component={this.eventsLink}>
								<Button>
									<Avatar alt="Events" src="/icons/events.svg" className="drawer-icon" />
								</Button>
								<ListItemText>Events</ListItemText>
							</ListItem>
							<ListItem button className="drawer-menu-list-item" component={this.distributorsAllLink}>
								<Button>
									<Avatar alt="Distributors" src="/icons/distributor.svg" className="drawer-icon" />
								</Button>
								<ListItemText>Distributors</ListItemText>
							</ListItem>
							<ListItem button className="drawer-menu-list-item" component={this.innovationsLink}>
								<Button>
									<Avatar alt="Innovations" src="/icons/innovation.svg" className="drawer-icon" />
								</Button>
								<ListItemText>Innovations</ListItemText>
							</ListItem>
							<ListItem button className="drawer-menu-list-item" component={this.technologyLink}>
								<Button>
									<Avatar alt="Technology" src="/icons/technology.svg" className="drawer-icon" />
								</Button>
								<ListItemText>Technology</ListItemText>
							</ListItem>
							<ListItem button className="drawer-menu-list-item" component={this.legalLink}>
								<Button>
									<Avatar alt="Legal/Privacy" src="/icons/legal.svg" className="drawer-icon" />
								</Button>
								<ListItemText>Legal/Privacy</ListItemText>
							</ListItem>
							<ListItem button className="drawer-menu-list-item" component={this.aboutLink}>
								<Button>
									<Avatar alt="About" src="/icons/about.svg" className="drawer-icon" />
								</Button>
								<ListItemText>About</ListItemText>
							</ListItem>
						</List>
					</Drawer>
				</Hidden>
			</>
		);

	}
}