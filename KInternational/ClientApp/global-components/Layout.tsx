
import * as React from 'react';
import { Subject } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';
import LinearProgress from '@material-ui/core/LinearProgress';

import { Consts } from 'globalshare/consts';
import { StorageUtility } from 'globalshare/core/Storage';

import { IBasicPage } from 'globalmodels/shared/ibasicpage';
import { IProductCategory } from 'globalmodels/product/iproductcategory';
import { LayoutHttpService } from 'globalservices/layout/layout.http.service';

import { HeaderComponent } from 'globalcomponents/header/header.component';

import './../styles/common.scss';
import './layout.scss';

export interface LayoutProps {
	children?: React.ReactNode;
}

export interface LayoutState extends IBasicPage {
	AntiXsrfToken: string;
}

export class Layout extends React.Component<LayoutProps, LayoutState> {

	private destroyStream: Subject<boolean> = new Subject<boolean>();

	constructor(props: any) {
		super(props);

		this.state = {
			HasDataLoaded: false,
			AntiXsrfToken: '',
			Url: ''
		};
	}

	public componentDidMount() {

		let categoriesAvailable: IProductCategory[] | null = StorageUtility.GetLocal<IProductCategory[]>(Consts.CATEGORY_KEY);
		if (categoriesAvailable === null) {
			LayoutHttpService.InitData().pipe(takeUntil(this.destroyStream))
				.subscribe((results) => {

					const allCountries = results[0];
					const allCategories = results[1];
					const allSubCategories = results[2];
					const allProducts = results[3];

					StorageUtility.StoreLocal(Consts.COUNTRY_KEY, allCountries, Consts.EXPIRATION_OF_ONE_WEEK);
					StorageUtility.StoreLocal(Consts.CATEGORY_KEY, allCategories, Consts.EXPIRATION_OF_ONE_WEEK);
					StorageUtility.StoreLocal(Consts.SUBCATEGORY_KEY, allSubCategories, Consts.EXPIRATION_OF_ONE_WEEK);
					StorageUtility.StoreLocal(Consts.PRODUCT_KEY, allProducts, Consts.EXPIRATION_OF_ONE_WEEK);

					this.setState({
						HasDataLoaded: true
					});

				});
		}
		else {
			this.setState({
				HasDataLoaded: true
			});
		}

		LayoutHttpService.GetAntiXsrfToken().pipe(take(1)).subscribe((xsrfToken: string) => {
			this.setState({
				AntiXsrfToken: xsrfToken
			})
		});

	}

	public componentWillUnmount() {
		this.destroyStream.next(true);
		this.destroyStream.unsubscribe();
	}

	public render() {

		const { HasDataLoaded, AntiXsrfToken } = this.state;

		if (!HasDataLoaded) {
			return <LinearProgress />;
		}

		return (
			<div className='width-100'>
				<div className='main-container clearfix'>
					<HeaderComponent />
					<div className='content-container'>
						{this.props.children}
					</div>
				</div>
			</div>
		);
	}
}
