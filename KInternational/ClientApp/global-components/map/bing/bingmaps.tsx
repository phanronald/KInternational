﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX

import * as React from "react";
import * as ReactDOM from "react-dom";
import "bingmaps";
import { Subject } from 'rxjs';

import { IBingMaps, IBingMapsState } from 'globalmodels/maps/ibingmaps';

import './bingmaps.scss';

declare var Microsoft: any;

export class BingMaps extends React.Component<IBingMaps, IBingMapsState> {

	private bingMap: Microsoft.Maps.Map;
	private destroyStream: Subject<boolean> = new Subject<boolean>();

	constructor(props: any) {
		super(props);

		this.state = {
		};
	}

	public componentDidMount() {

		this.handleChangeMap();
	}

	public componentDidUpdate(prevProps: IBingMaps, prevState: IBingMapsState) {
		if (this.props.Latitude !== prevProps.Latitude && this.props.Longitude !== prevProps.Longitude) {
			this.handleChangeMap();
		}
	}

	public componentWillUnmount() {
		this.destroyStream.next(true);
		this.destroyStream.unsubscribe();
	}

	private handleChangeMap = (): void => {

		this.bingMap = new Microsoft.Maps.Map('#bingMapId',
		{
			center: new Microsoft.Maps.Location(this.props.Latitude, this.props.Longitude)
		});

		const center: Microsoft.Maps.Location = this.bingMap.getCenter();
		const centerPushPin = new Microsoft.Maps.Pushpin(center, {
			title: this.props.Title,
			subTitle: this.props.SubTitle,
			text: '1'
		});

		this.bingMap.entities.clear();
		this.bingMap.entities.push(centerPushPin);
	}

	render() {

		return (
			<>
				<div id="bingMapId" className="bing-map"></div>
			</>
		);

	}
}