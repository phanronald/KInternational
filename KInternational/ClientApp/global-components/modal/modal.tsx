﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX

import * as React from "react";
import * as ReactDOM from "react-dom";

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import Close from '@material-ui/icons/Close';

import { IModal, IModalState } from "globalmodels/modal/IModal";

export class Modal extends React.Component<IModal, IModalState> {

	constructor(props: IModal) {
		super(props);

		this.state = {
			IsModalOpen: false
		};
	}

	public componentDidMount() {
		this.setState({ IsModalOpen: this.props.open });
	}

	public static getDerivedStateFromProps(props: IModal, state: IModalState) {

		return {
			IsModalOpen: props.open,
		};
	}

	public componentWillUnmount() {

	}

	closeVideoDialog = (event: any) => {
		this.props.service.applyOpenCloseModalEmitted(false);
		this.setState({ IsModalOpen: false });
	}

	render() {

		return (
			<Dialog fullScreen={false} open={this.state.IsModalOpen} maxWidth="md" className={'modal-container ' + this.props.className}>
				<DialogTitle className="modal-title">
					<Close className="modal-close-button" onClick={event => this.closeVideoDialog(event)} />
				</DialogTitle>
				<DialogContent className="modal-dialog-conent">
					{this.props.children}
				</DialogContent>
				<DialogActions>

				</DialogActions>
			</Dialog>
		);

	}
}