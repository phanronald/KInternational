﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX

import * as React from "react";
import * as ReactDOM from "react-dom";
import { Link } from 'react-router-dom';

import { IPageNotFound } from "globalmodels/shared/ipagenotfound";

export class PageNotFound extends React.Component<IPageNotFound, any> {

	public componentDidMount() {

	}

	public componentWillUnmount() {

	}

	render() {

		return (
			<div>
				<p>NOT FOUND</p>
			</div>
		);

	}
}