﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX

import * as React from "react";
import * as ReactDOM from "react-dom";
import { Subject } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';

import { ICarousel, ICarouselState } from 'globalmodels/carousel/icarousel';

import './carousel.scss';
import { ReactChild } from "react";

export class Carousel extends React.Component<ICarousel, ICarouselState> {

	private positionPerSlide: number = 58;
	private minSlidesToAddPosition: number = 18;
	private extraPositionToAdd: number = 0.5;
	private destroyStream: Subject<boolean> = new Subject<boolean>();

	constructor(props: any) {
		super(props);

		this.state = {
			CarouselIndex: 0
		};
	}

	public componentDidMount() {
		this.setState({ CarouselIndex: this.props.SelectedIndex });
		this.slideCarousel(this.props.SelectedIndex);
	}

	public componentWillUnmount() {
		this.destroyStream.next(true);
		this.destroyStream.unsubscribe();
	}

	private slideCarousel = (carouselIndex: number): void => {
		const carouselElement: HTMLCollection = document.getElementsByClassName('slide-carousel')[0].children;
		React.Children.map(this.props.children, (child: ReactChild, index: number) => {

			let leftPerSlide: number = this.positionPerSlide * (carouselIndex - 1);
			if (carouselIndex > this.minSlidesToAddPosition) {
				leftPerSlide += (this.extraPositionToAdd * (carouselIndex / this.minSlidesToAddPosition));
			}
			(carouselElement.item(index) as HTMLElement).style.left =
				'-' + leftPerSlide.toString() + 'rem';

		});
	}

	private previousSlideClickHandler = (event: any): void => {

		let newCarouselIndex = this.state.CarouselIndex - 1;
		if (newCarouselIndex > 0) {
			this.slideCarousel(newCarouselIndex);
		}
		else {
			newCarouselIndex = React.Children.count(this.props.children);
			this.slideCarousel(newCarouselIndex);
		}

		this.setState({
			CarouselIndex: newCarouselIndex
		});
	}

	private nextSliderClickHandler = (event: any): void => {

		let newCarouselIndex = this.state.CarouselIndex + 1;
		if (this.state.CarouselIndex < React.Children.count(this.props.children)) {
			this.slideCarousel(newCarouselIndex);
		}
		else {
			newCarouselIndex = 1;
			this.slideCarousel(newCarouselIndex);
		}

		this.setState({
			CarouselIndex: newCarouselIndex
		});
	}

	render() {

		return (
			<>
				<div className="carousel-container">

					<div className="slide-carousel">
						{this.props.children}
					</div>

				</div>
				<div className="carousel-footer">

					<div className="footer-item slide-arrow slide-arrow-left"
						onClick={event => this.previousSlideClickHandler(event)}>&lt;
					</div>

					<div className="footer-item display-carousel-indexing">
						<span>{this.state.CarouselIndex} of {React.Children.count(this.props.children)}</span>
					</div>

					<div className="footer-item slide-arrow slide-arrow-right"
						onClick={event => this.nextSliderClickHandler(event)}>&gt;
					</div>
				</div>
			</>
		);

	}
}