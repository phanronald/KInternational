﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX

import * as React from "react";
import * as ReactDOM from "react-dom";
import { Link } from 'react-router-dom';
import { Subject } from 'rxjs';

import { IHomePage, IHomeState } from "globalmodels/props/home/ihome";

import { Carousel } from 'react-responsive-carousel';

import './home.component.scss';
import "./../../../wwwroot/scss/carousel.scss";

export class HomeComponent extends React.Component<IHomePage, IHomeState> {

	private destroyStream: Subject<boolean> = new Subject<boolean>();

	constructor(props: IHomePage) {
		super(props);
		this.state = {
			AxCheapCounter: 0
		};
	}

	public componentDidMount() {

	}

	public componentWillUnmount() {
		this.destroyStream.next(true);
		this.destroyStream.unsubscribe();
	}

	render() {

		return (
			<div>
				<div>
					<Carousel showStatus={false} showThumbs={false} infiniteLoop={true}
						autoPlay={true} useKeyboardArrows={true} dynamicHeight={true}>
						<div>
							<img src="/images/home/kla-my19-zx10r.jpg" />
							<div className="legend">
								<div>
									<h2>CHAMPIONSHIP-PROVEN POWER: </h2>
									<h3>
										INTRODUCING THE NEW 2019 NINJA® ZX™-10R ABS KRT EDITION
									</h3>
								</div>
								
								<Link to='/products/2019-ninja-zx-10r-abs-krt-edition'>
									<span>SEE THE NEW 2019 NINJA® ZX™-10R ABS KRT</span>
								</Link>
							</div>
						</div>
						<div>
							<img src="/images/home/kla_my19_mule_mx.jpg" />
							<div className="legend">
								<div>
									<h2>CMID-SIZED AND FULLY CAPABLE: </h2>
									<h3>
										INTRODUCING THE NEW MULE PRO-MX™ EPS LE
									</h3>
								</div>

								<Link to='/products/2019-mule-pro-mx-eps-le'>
									<span>SEE THE 2019 MULE PRO-MX™ EPS LE</span>
								</Link>
							</div>
						</div>
						<div>
							<img src="/images/home/kla_my19_teryx.jpg" />
							<div className="legend">
								<div>
									<h2>PERFECT COMBINATION OF REAL STRENGTH AND UNMATCHED PERFORMANCE: </h2>
									<h3>
										DOMINATE THE MOST DEMANDING TRAILS
									</h3>
								</div>

								<Link to='/products/2019-teryx4-800-fi-4x4-eps-le'>
									<span>SEE THE 2019 TERYX4™ 800 FI 4x4 EPS LE</span>
								</Link>
							</div>
						</div>
						<div>
							<img src="/images/home/kla_my19_brute-force.jpg" />
							<div className="legend">
								<div>
									<h2>A CONQUEROR ON FOUR WHEELS: </h2>
									<h3>
										SUPERIOR PERFORMANCE IN A VARIETY OF CONDITIONS
									</h3>
								</div>

								<Link to='/products/2019-brute-force-750-4x4i-eps'>
									<span>SEE THE 2019 BRUTE FORCE® 750 4x4i EPS</span>
								</Link>
							</div>
						</div>
						<div>
							<img src="/images/home/kla_my19_jetski-sxr.jpg" />
							<div className="legend">
								<div>
									<h2>Continuing the Legacy: </h2>
									<h3>
										The New 2019 Stand-Up Jet Ski® SX-R™ Watercraft
									</h3>
								</div>

								<Link to='/products/2019-jet-ski-sx-r'>
									<span>SEE THE 2019 JET SKI® SX-R™</span>
								</Link>
							</div>
						</div>
					</Carousel>
				</div>
			</div>
		);

	}
}