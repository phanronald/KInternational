﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX

import * as React from "react";
import * as ReactDOM from "react-dom";
import { Subject } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import LinearProgress from '@material-ui/core/LinearProgress';

import { ILink } from "globalmodels/shared/ilink";
import { IAbout, IAboutState } from 'globalmodels/props/about/IAbout';

import { CommonService } from 'globalservices/shared/common.service';

import './about.component.scss';

export class AboutComponent extends React.Component<any, IAboutState> {

	private destroySubscription: Subject<boolean> = new Subject<boolean>();

	constructor(props: any) {
		super(props);
		this.state = {
			HasDataLoaded: false,
			AboutData: null,
			Url: ''
		};
	}

	public componentDidMount() {

		CommonService.AjaxGet('/api/About/GetAbout').pipe(take(1))
			.subscribe((response: any) => {

				this.setState({
					HasDataLoaded: true,
					AboutData: (response as IAbout)
				});

			});
	}

	public componentWillUnmount() {
		this.destroySubscription.next(true);
		this.destroySubscription.unsubscribe();
	}

	render() {

		const { HasDataLoaded } = this.state;
		const aboutData: IAbout = (this.state.AboutData as IAbout);

		if (!HasDataLoaded) {
			return <LinearProgress color="secondary" />;
		}

		return (
			<div className="about">
				<img src="/images/about_legal_header.jpg" alt="Kawasaki Latin America Legal Header" />
				<div className="about-container">

					<div className="header">
						<h1 className="content-header-text">{aboutData.HeaderText}</h1>
					</div>
					<div className="about-body">
						<Paper>
							<Typography component="p">
								{aboutData.Overview}
							</Typography>
						</Paper>

						<p>{aboutData.Detailed}</p>
						<p>
							{aboutData.Contact.Text}
							<a className="emailContactLink" href={"mailto:" + aboutData.Contact.Email}>{aboutData.Contact.Email}</a>
						</p>
						<p>
							<span dangerouslySetInnerHTML={{ __html: aboutData.MainAddress }} />
							<br /><br />
							<span dangerouslySetInnerHTML={{ __html: aboutData.MailAddress }} />
						</p>
						<p>

							{
								aboutData.AboutLinks.map((aboutLink: ILink, index: number) => {

									return (
										<span key={index}>
											<a href={aboutLink.Url} className="external">{aboutLink.Text}</a>
											{
												index <= (aboutData.AboutLinks.length - 1) && <br />
											}
										</span>
									)
								})
							}

						</p>
					</div>
				</div>
			</div>
		);
	}
}