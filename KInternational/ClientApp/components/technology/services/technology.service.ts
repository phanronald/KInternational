﻿
import { BehaviorSubject, Observable } from 'rxjs';

import { TechnologyType } from 'globalmodels/props/technology/technologytype';

export class TechnologyService {

	public technologyTypeStream = new BehaviorSubject<TechnologyType>(TechnologyType.None);

	constructor() {

	}

	public clickToTechnologyType = (techType: TechnologyType): void => {
		this.technologyTypeStream.next(techType);
	}

}