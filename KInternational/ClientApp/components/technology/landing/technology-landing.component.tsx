﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX

import * as React from "react";
import * as ReactDOM from "react-dom";
import { Link } from 'react-router-dom';

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import { TechnologyType } from 'globalmodels/props/technology/technologytype';
import { ITechnologyLanding } from "globalmodels/props/technology/itechnologylanding";

import './technology-landing.component.scss';

export class TechnologyLanding extends React.Component<ITechnologyLanding, any> {

	private engineTechLink = (props: any) => <Link to="/technology/engine" onClick={ event => this.props.service.clickToTechnologyType(TechnologyType.Engine)} {...props} />;
	private chassisTechLink = (props: any) => <Link to="/technology/chassis" onClick={event => this.props.service.clickToTechnologyType(TechnologyType.Chassis)} {...props} />;

	constructor(props: any) {
		super(props);
	}

	public componentDidMount() {

	}

	public componentWillUnmount() {

	}

	render() {

		return (
			<div className="technology-landing">
				<img src="/images/technology_landing_header.jpg" alt="Technology Page Header" />
				<div className="technology-container">

					<div className="header row">
						<h2 className="tech-header-txt col-lg-8 col-md-8 col-sm-10 col-xs-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-1 col-xs-offset-1">
							&quot;Our mission is to enable riders to have optimum control of their
							high-performance Kawasaki motorcycle, so they may enjoy the pleasure of
							riding. Helping riders master the performance advantages offered by Kawasaki
							technology is our simple and innate desire.&quot;
						</h2>
					</div>
					<div className="technology-body row">
						<div className="technology-listing col-lg-4 col-md-4 col-sm-5 col-xs-5 col-lg-offset-2 col-md-offset-2 col-sm-offset-1 col-xs-offset-1">

							<Card className="technology-card">
								<CardContent>
									<Typography component="h3" variant="headline" className="technology-card-header">
										ENGINE MANAGEMENT TECHNOLOGY
									</Typography>
									<Typography component="p" className="technology-card-description">
										Ever since manufacturing the first motorcycle engine in 1950,
										Kawasaki has been continually pursuing new technologies in the creation of our
										high-performance engines.
									</Typography>
								</CardContent>
								<CardActions>
									<Button component={this.engineTechLink} size="small" color="primary">Learn More</Button>
								</CardActions>
							</Card>
						</div>

						<div className="technology-listing col-lg-4 col-md-4 col-sm-5 col-xs-5">
							<Card className="technology-card">
								<CardContent>
									<Typography component="h3" variant="headline" className="technology-card-header">
										CHASSIS MANAGEMENT TECHNOLOGY
								</Typography>
									<Typography component="p" className="technology-card-description">
										The result of our advanced engineering, exhaustive testing and continuous
									pursuit of superior reliability, safety and performance.
								</Typography>
								</CardContent>
								<CardActions>
									<Button component={this.chassisTechLink} size="small" color="primary">Learn More</Button>
								</CardActions>
							</Card>
						</div>
					</div>
				</div>
			</div>
		);
	}
}