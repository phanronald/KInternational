﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX

import * as React from "react";
import * as ReactDOM from "react-dom";
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import LinearProgress from '@material-ui/core/LinearProgress';

import { Subject } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';

import { Consts } from 'globalshare/consts';
import { Collection } from "globalshare/core/Collection";
import { StorageUtility } from "globalshare/core/Storage";
import { TechnologyType } from "globalmodels/props/technology/technologytype";
import {
	ITechnologyPage,
	ITechnologyState,
	ITechnology,
	IMedia,
	MediaType
} from "globalmodels/props/technology/itechnologypage";

import { CommonService } from 'globalservices/shared/common.service';
import { ModalService } from 'globalservices/modal/modal.service';
import { TechnologyService } from './services/technology.service';
import { TechnologyHeader } from './header/technology-header.component';
import { TechnologyLanding } from './landing/technology-landing.component';

import { Modal } from 'globalcomponents/modal/modal';
import { Carousel } from 'globalcomponents/carousel/carousel';

import { PageNotFound } from './../../global-components/notfound/pagenotfound';

import './technology.component.scss';

export class TechnologyComponent extends React.Component<ITechnologyPage, ITechnologyState> {

	private isEngine: boolean = false;
	private isChassis: boolean = false;
	private techService: TechnologyService;
	private modalService: ModalService;
	private destroyStream: Subject<boolean> = new Subject<boolean>();
	private a: any = {
		backgroundImage: 'url(' + 'https://www.kawasaki-la.com/cms/images/producttechnology/su4f35kf.tqw.png' +  ')'
	}

	constructor(props: ITechnologyPage) {
		super(props);
		this.state = {
			IsLandingPage: false,
			HasDataLoaded: false,
			TechnologyDetail: [],
			Is404Redirect: false,
			IsModalOpen: false,
			SelectedTechnologyIndex: 0,
			Url: ''
		};

		this.techService = new TechnologyService();
		this.modalService = new ModalService();
	}

	public static getDerivedStateFromProps(props: ITechnologyPage, state: ITechnologyState) {

		if (props.match.url !== state.Url) {
			return {
				HasDataLoaded: false
			};
		}

		return null;
	}

	public componentDidMount() {

		this.modalService.openCloseModalEmitted.pipe(takeUntil(this.destroyStream)).subscribe((isOpen: boolean) => {
			this.setState({ IsModalOpen: isOpen });
		});

		this.handleTechnologyChange(this.props);
	}

	public componentDidUpdate(prevProps: ITechnologyPage, prevState: ITechnologyState) {
		if (this.props.match.url !== prevProps.match.url) {
			this.handleTechnologyChange(this.props);
		}
	}

	public componentWillUnmount() {
		this.destroyStream.next(true);
		this.destroyStream.unsubscribe();
	}

	private handleTechnologyChange = (props: ITechnologyPage): void => {

		this.setState({ Url: props.match.url });
		const currentTechnology = props.match.params[Consts.TECHNOLOGY_TYPE];
		if (currentTechnology === undefined) {
			this.setState({ IsLandingPage: true, HasDataLoaded: true });
		}
		else {

			let currentTechnologyType = TechnologyType.None;
			switch (currentTechnology.toLowerCase()) {
				case TechnologyType[TechnologyType.Engine].toLowerCase(): {
					currentTechnologyType = TechnologyType.Engine;
					break;
				}
				case TechnologyType[TechnologyType.Chassis].toLowerCase(): {
					currentTechnologyType = TechnologyType.Chassis;
					break;
				}
				default: {
					break;
				}

			}

			if (currentTechnologyType === TechnologyType.None) {

				this.setState({
					Is404Redirect: true
				});

				return;

			}

			this.techService.clickToTechnologyType(currentTechnologyType);
			this.getTechnologyData(currentTechnologyType);
			this.displayCorrectTechHeader(currentTechnologyType);

			this.setState({
				IsLandingPage: false
			});
		}
	}

	private getTechnologyData = (techType: TechnologyType): void => {

		let technologiesAvailable: Array<ITechnology> | null = StorageUtility.GetLocal(Consts.TECHNOLOGY_TYPE);
		if (technologiesAvailable === undefined || technologiesAvailable === null || technologiesAvailable.length === 0) {

			CommonService.AjaxGet('/api/Technology/GetAllTechnologies').pipe(take(1))
				.subscribe((response: any) => {

					if (response !== "undefined") {

						const technologyData = response as ITechnology[];

						const updatedTechnologies: Collection<ITechnology> = new Collection(technologyData);
						updatedTechnologies.ForEach((tech: ITechnology | undefined, idx: number | undefined) => {
							if (tech !== undefined && tech !== null) {
								tech.TruncatedDescription = tech.Description.replace(/<[^>]+>/g, '').substring(0, 219) + '...';
							}
						});

						StorageUtility.StoreLocal(Consts.TECHNOLOGY_TYPE, updatedTechnologies.ToArray(), Consts.EXPIRATION_OF_ONE_WEEK);
						technologiesAvailable = this.getFilteredTechnologies(technologyData, techType);

						this.setState({
							TechnologyDetail: technologiesAvailable,
							HasDataLoaded: true
						});

						this.displayCorrectTechHeader(techType);

					}

				});
		}
		else {
			technologiesAvailable = this.getFilteredTechnologies(technologiesAvailable, techType);
			this.displayCorrectTechHeader(techType);

			this.setState({
				TechnologyDetail: technologiesAvailable,
				HasDataLoaded: true
			});
		}

	}

	private getFilteredTechnologies = (technology: ITechnology[], techType: TechnologyType): Array<ITechnology> => {

		let finalFilteredArray: Array<ITechnology> = [];
		let filteredTechnologies: Collection<ITechnology> = new Collection(technology);
		switch (techType) {
			case TechnologyType.Engine: {
				finalFilteredArray = filteredTechnologies.Where(x => x !== undefined && x.TechType === TechnologyType.Engine).ToArray();
				break;
			}
			case TechnologyType.Chassis: {
				finalFilteredArray = filteredTechnologies.Where(x => x !== undefined && x.TechType === TechnologyType.Chassis).ToArray();
				break;
			}
			default: {
				break;
			}

		}

		return finalFilteredArray;

	}

	private displayCorrectTechHeader = (engineType: TechnologyType): void => {

		switch (engineType) {
			case TechnologyType.Engine: {
				this.isEngine = true;
				this.isChassis = false;
				break;
			}
			case TechnologyType.Chassis: {
				this.isEngine = false;
				this.isChassis = true;
				break;
			}
			default: {
				this.isEngine = false;
				this.isChassis = false;
				break;
			}
		}

	}

	private seeMoreClickHandler = (event: any, techPosition: number): void => {
		this.setState({ IsModalOpen: true, SelectedTechnologyIndex: techPosition });
	}

	private mediaClickHandler = (event: any, techMedia: IMedia): void => {

		const selectedThumbnailContainer: HTMLElement = ((event.target as HTMLElement).parentNode.parentNode) as HTMLElement;
		for (let i = 0, len = selectedThumbnailContainer.children.length; i < len; i++) {
			let indivThumbnail = selectedThumbnailContainer.children[i] as HTMLElement;
			indivThumbnail.classList.remove('active');
		}

		(((event.target as HTMLElement).parentNode) as HTMLElement).classList.add('active');

		let contentMediaContainer: HTMLElement = ((selectedThumbnailContainer.parentNode) as HTMLElement).querySelector('#contentMediaId');
		while (contentMediaContainer.firstChild) {
			contentMediaContainer.removeChild(contentMediaContainer.firstChild);
		}

		if (techMedia.Type === MediaType.Image) {
			let imageContainer: HTMLImageElement = document.createElement('img');
			imageContainer.src = techMedia.Image;
			contentMediaContainer.appendChild(imageContainer);
		}
		else {
			let videoContainer: HTMLVideoElement = document.createElement('video');
			videoContainer.src = techMedia.Video;
			videoContainer.controls = true;
			videoContainer.setAttribute('controlsList', 'nodownload');
			contentMediaContainer.appendChild(videoContainer);
		}
	}

	render() {

		const { IsLandingPage, Is404Redirect, HasDataLoaded,
			IsModalOpen, SelectedTechnologyIndex
		} = this.state;

		if (Is404Redirect) {
			return <PageNotFound />
		}

		if (!HasDataLoaded) {
			return <LinearProgress color="secondary" />;
		}

		return (
			<div className="technology">
				<TechnologyHeader service={this.techService} />
				<>
					{
						IsLandingPage && <TechnologyLanding service={this.techService} />
					}
				</>
				<>
					{
						!IsLandingPage &&
						<div className="technology-container">
							<div className="tech-listing-header row">
								{
									this.isEngine &&
									<div id="engineListingHeaderId" className="tech-mgmt-container col-lg-11 col-md-11 col-sm-11 col-xs-11">
										<div className="tech-mgmt-tech">
											<span>ENGINE MANAGEMENT TECHNOLOGY</span>
										</div>
										<div className="tech-mgmt-txt">
											<span>Ever since manufacturing the first motorcycle engine in 1950,
												Kawasaki has been continually pursuing new technologies in the
												creation of our high-performance engines.</span>
										</div>
									</div>
								}

								{
									this.isChassis &&
									<div id="chassisListingHeaderId" className="tech-mgmt-container col-lg-11 col-md-11 col-sm-11 col-xs-11">
										<div className="tech-mgmt-tech">
											<span>CHASSIS MANAGEMENT TECHNOLOGY</span>
										</div>
										<div className="tech-mgmt-txt">
											<span>The result of our advanced engineering, exhaustive testing and
											continuous pursuit of superior reliability, safety and performance.
											</span>
										</div>
									</div>
								}
							</div>
							<div className="technology-listing-container row">

								{
									this.state.TechnologyDetail.map((technology: ITechnology, index: number) => {

										return (
											<div className="technology-listing col-lg-4 col-md-4 col-sm-6 col-xs-12" key={index} data-uid={technology.TechType}>
												<Card className="technology-item">
													<CardHeader className="technology-header" title={technology.Name}
														avatar={<img src={technology.Image} className="technology-header-img" />} />

													<CardContent>
														<div className="technology-description"
															dangerouslySetInnerHTML={{ __html: technology.TruncatedDescription }}>
														</div>
													</CardContent>
													<CardActions>
														<Button size="small" color="primary" className="event-seemore"
															onClick={event => this.seeMoreClickHandler(event, (index+1))}>
															Read More
														</Button>
													</CardActions>
												</Card>
											</div>
										)

									})
								}

							</div>
						</div>
					}
				</>
				<>
					{
						IsModalOpen &&
						<Modal open={IsModalOpen} service={this.modalService} className="modal-tech">

							<Carousel SelectedIndex={SelectedTechnologyIndex}>

								{
									this.state.TechnologyDetail.map((technology: ITechnology, index: number) => {

										return (
											<div className="tech-slide" key={index}>
												<div className="feature-content">
													<div className="feature-content-header">
														<div className="feature-content-icon" style={this.a}></div>
														<div className="feature-content-title">
															{technology.Name}
														</div>
													</div>
													<div className="tech-feature-content-green-line"></div>
													<div className="feature-content-description">

														<div dangerouslySetInnerHTML={{ __html: technology.Description }}></div>

													</div>
												</div>
												<div className="media-content">
													<div className="media-display-container">
														{
															technology.Media.length > 0 &&
															<div id="contentMediaId">
																<img src={technology.Media[0].Image} />
															</div>
														}	
													</div>
													<div className="media-thumb-container">
														{
															technology.Media.map((techMedia: IMedia, index: number) => {
																return (
																	<span className={'media-thumb-item' + (index === 0 ? ' active' : '')} key={index}
																		onClick={event => this.mediaClickHandler(event, techMedia)}>

																		<img src={techMedia.Image} />
																	</span>
																)
															})
														}
													</div>
												</div>
											</div>
										)

									})
								}

							</Carousel>

						</Modal>
					}
				</>
			</div >
		);
	}
}