﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX

import * as React from "react";
import * as ReactDOM from "react-dom";
import { Link } from 'react-router-dom';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import AppBar from '@material-ui/core/AppBar';

import { TechnologyType } from 'globalmodels/props/technology/technologytype';
import { ITechnologyHeader, ITechnologyHeaderState } from "globalmodels/props/technology/itechnologyheader";

import './technology-header.component.scss';

export class TechnologyHeader extends React.Component<ITechnologyHeader, ITechnologyHeaderState> {

	private destroyStream: Subject<boolean> = new Subject<boolean>();

	constructor(props: ITechnologyHeader) {
		super(props);
		this.state = {
			IsEngineActive: false,
			IsChassisActive: false
		};
	}

	public componentDidMount() {

		this.props.service.technologyTypeStream.pipe(takeUntil(this.destroyStream)).subscribe((techType: TechnologyType) => {
			if (techType !== TechnologyType.None) {

				this.setState({
					IsChassisActive: ((techType as TechnologyType) === TechnologyType.Chassis),
					IsEngineActive: ((techType as TechnologyType) === TechnologyType.Engine)
				});
			}
		});

	}

	public componentWillUnmount() {
		this.destroyStream.next(true);
		this.destroyStream.unsubscribe();
	}

	render() {

		const { IsEngineActive, IsChassisActive } = this.state;

		return (
			<AppBar position="static" color="default" className="tech-sub-nav row">

				<div className="tech-type-nav col-lg-12 col-md-12 col-sm-12 col-xs-12">

					<ul className="display-table tech-type-listing">
						<li className="display-table-cell">
							<h1 className="technology-sub-nav-header">
								Kawasaki Technology
						</h1>
						</li>

						<li className="display-table-cell">
							<Link to='/technology/engine' className={'tech-type ' + (IsEngineActive ? 'active' : '')}>
								ENGINE MANAGEMENT TECHNOLOGY
							</Link>
						</li>

						<li className="display-table-cell">
							<Link to='/technology/chassis' className={'tech-type ' + (IsChassisActive ? 'active' : '')}>
								CHASSIS MANAGEMENT TECHNOLOGY
							</Link>
						</li>

					</ul>
				</div>
			</AppBar>
		);
	}
}