﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX

import * as React from "react";
import * as ReactDOM from "react-dom";

import { ILegalPage } from "globalmodels/props/legal/ilegalpage";

import './legal.component.scss';

export class LegalComponent extends React.Component<ILegalPage, any> {

	public componentDidMount() {

	}

	public componentWillUnmount() {

	}

	render() {

		return (
			<div className="legal">
				<img src="/images/about_legal_header.jpg" alt="Kawasaki Latin America Legal Header" />
				<div className="legal-container">
					<div className="header">
						<h1 className="content-header-text">Legal Notices</h1>
					</div>
					<div className="legal-body">

						<p className="contentSubhead">Privacy Policy</p>
						<p>
							<strong>1. Information Collected</strong><br />
							In order to access certain products, services and areas (collectively, the "Services") within the family of   Kawasaki Motors Corp., U.S.A. ("KMC") web sites consisting of kawasaki.com
							and kawpower.com in addition to other promotional or special interest web sites periodically made available by KMC (collectively, the "Web Sites"), you need to provide certain identifying information (the "Registration Data") listed in the online Member Registration page or other pages within the Web Sites. This information may include your legal name, address, shipping address, phone number, e-mail address, subscriber name or "screen name," ownership information of products sold by KMC, areas of interest and password used to access the Services. KMC reserves the right to request any additional information necessary to establish and maintain your account for use of any of the Services available on the Web Sites.
						</p>
						<p>The personal information you provide us is transmitted using Secured Socket Layer encryption technology to a secured database server. Once you become a Registered Member or place an order on the Web Sites, KMC stores such information in order to customize your experience on the Web Sites.</p>
						<p>
							<strong>2. Use of Cookies</strong><br />
							KMC may place Internet "cookies" on your computer's hard drive to assist with your online experience. A   cookie is a text file that is placed on your hard disk by a Web page server. Cookies cannot be used to run programs or deliver viruses to your computer.
						</p>
						<p>One of the primary purposes of cookies for KMC's Web Sites is for session information, including information needed to operate the shopping cart and to view parts diagrams, which may be available on the Web Sites. Cookies are also used to help track site and user activity so that KMC can better determine which areas are most popular and modify the site   accordingly.</p>
						<p>You have the ability to accept or decline cookies. Most Web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. If you choose to decline cookies, you will not be able to fully experience some of the features of the KMC Web Sites including the shopping cart and parts diagrams.</p>
						<p>
							<strong>3. Use of Web Beacons and Spotlight Tags</strong><br />
							KMC may work with third parties to research on our behalf certain usage and activities on parts of our Web Sites. KMC's Web Sites may contain electronic images known as Web beacons or Spotlight Tags - sometimes called single-pixel gifs - that   allow KMC to count users who have visited those pages. Web beacons are not used to access your personal information on the KMC network of sites and services; they are a technique we use to compile aggregated statistics about KMC Web Site usage.
						</p>
						<p dir="ltr">Web beacons collect only a limited set of information including a cookie number, time and date of a page view, and a description of the page on which the Web beacon resides.</p>
						<p>
							<strong>4. Use of Technology Tools</strong><br />
							To help us make   e-mails more useful and interesting, we often receive a confirmation when you   open and click on the hyperlinks contained in the e-mail from us if your   computer supports such capabilities. We may also receive information on the   amount of time the e-mail remained open (dwell time) on your e-mail   client.
						</p>
						<p>
							<strong>5. Confidentiality</strong><br />
							Registration Data is the   property of KMC, subject to and protected by applicable laws. Unless otherwise   authorized as part of your Member Registration, as a part of your shopping   experience or as a part of your interest in any Services, KMC will not sell,   rent, swap or otherwise disclose your Registration Data to any third party. KMC   uses Secured Socket Layer encryption technology to transmit Registration Data,   and stores all Registration Data in a secured mainframe file protected from   Internet access by a fire wall. Credit card information is accessible only by authorized personnel.
						</p>
						<p>
							<strong>6. Uses Of Registration and Other Personal Data</strong><br />
							KMC reserves the right to disclose to third parties information about your usage of the Web Sites and any related services, including   information gathered during your use of the Web Sites. Any third party to which KMC rents, sells, shares or otherwise discloses personal data will be carefully pre-screened by KMC, determined by KMC to be reputable, and will use the   personal data for marketing products and services which KMC determines, in its sole judgment, that you might find of interest. Generally, KMC will disclose such information to third parties only in the form of aggregate data (such as overall patterns or demographic reports) that does not describe or identify any individual user. KMC reserves the right, however, to disclose more specific information to the dealer you designate as a part of your Member Registration, as a part of your shopping experience or as a part of your interest in any   Services and to other third parties, unless you select the "Opt-out" option during the member registration process or thereafter by using the applicable functionality available for updating your Registration Data.
						</p>
						<p>In addition, personal data collected by KMC will be used by KMC for editorial and feedback purposes, for marketing and promotional purposes, for a statistical analysis of your behavior while using the Web Sites, for product development, for content improvement, to inform advertisers as to how many visitors have seen or clicked on their advertisements, and to customize   content and layout of the Web Sites. Names, postal and e-mail addresses collected may be added to KMC’s databases and used for future communications and mailings regarding Web Sites updates, new products and services, upcoming events, and status of orders placed online.</p>
						<p>Unless you select the "Opt-Out" option as described in Section 7 below, KMC reserves the right, directly or through one or more third   parties, to use your Registration Data to send you information about KMC and its dealers, the Web Sites, products sold by KMC and other topics in which you may have an interest.</p>
						<p>KMC does not currently accept any information submitted to or posted at its Web Sites from children under the age of 13. No information should be submitted to or posted at the Web Sites by children under 13 years of age without the consent of their parent or legal guardian. KMC does not allow children under 13 to register as members or to receive direct marketing   communications arising from their use of the Web Sites and, therefore, KMC does not provide any personally identifiable information from children under 13 to any third party. KMC does encourage parents and guardians to spend time online with their children and to participate in the interactive activities offered on any of the Web Sites.</p>
						<p>
							<strong>7. Opt-Out Right</strong><br />
							If you do not wish to receive any special offers or other communications from KMC, its dealers or third   parties, you have the right to select the appropriate option during the membership registration process or when you submit your Registration Data. In addition, at any time after you have registered, you may "Opt-out" using the applicable functionality available for updating your Registration   Data.
						</p>
						<p>
							<strong>8. KMC’s Right to Contact You</strong><br />
							KMC reserves the right to contact visitors to the Web Sites regarding account status and changes to the <a className="external" href="http://www.kawasaki.com/About/Legal#l-use">Terms and Conditions of Use</a>, <a className="external" href="http://www.kawasaki.com/About/Legal#l-sale">Terms and Conditions of Sale</a>, Privacy Policy, or any other policies or agreements relevant to visitors or users of the Web Sites.
						</p>
						<p>
							<strong>9. User Access to Information Collected</strong><br />
							You may   access, modify, correct, change or update your Registration Data at any time through the Member Registration page by using the applicable functionality available for updating your Registration Data. Your Registration Data and any related information is password-restricted for your protection. You may also change your password when you use the functionality available to access your   Registration Data.
						</p>
						<p>
							<strong>10. No Liability for Acts of Third Parties</strong><br />
							Although KMC will use all reasonable efforts to safeguard the confidentiality of the Registration Data, transmissions made by means of the Internet cannot be made absolutely secure. KMC will have no liability for disclosure of Registration Data due to errors in transmission, unauthorized acts of third parties or through the use of third party cookies including those of   advertisers when you view or click on their advertisements. Once you leave any of the Web Sites, you will be subject to third party privacy policies, which may differ significantly from our Privacy Policy.
						</p>
						<p>
							<strong>11. Updates</strong><br />
							KMC reserves the right to change or update this Privacy Policy, or any other KMC policy or practice, at any time. Any changes or updates will be effective immediately upon posting to the Web Sites, and your use of any of the Web Sites following such posting constitutes your acceptance of the Privacy Policy as so changed or updated.
						</p>

					</div>
				</div>
			</div>
		);
	}
}