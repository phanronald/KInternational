﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX

import * as React from "react";
import * as ReactDOM from "react-dom";
import { take } from 'rxjs/operators';
import { Subject } from 'rxjs';

import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import LinearProgress from '@material-ui/core/LinearProgress';

import { Consts } from 'globalshare/consts';
import { Collection } from "globalshare/core/Collection";
import { StorageUtility } from "globalshare/core/Storage";

import { IProduct } from 'globalmodels/product/iproduct';
import {
	IProductVideoPage, IProductVideoState, IProductVideo
} from 'globalmodels/props/product/iproductvideopage';

import { CommonService } from 'globalservices/shared/common.service';
import { ProductHeaderComponent } from './../header/product-header.component';
import { PageNotFound } from './../../../global-components/notfound/pagenotfound';

import './product-video.component.scss';

export class ProductVideoComponent extends React.Component<IProductVideoPage, IProductVideoState> {

	private allProducts: Array<IProduct> = [];
	private destroyStream: Subject<boolean> = new Subject<boolean>();

	constructor(props: IProductVideoPage) {
		super(props);
		this.state = {
			Is404Redirect: false,
			HasDataLoaded: false,
			Url: '',
			Product: null,
			SelectedVideo: null,
			AllVideosForProduct: []
		};

		this.allProducts = StorageUtility.GetLocal(Consts.PRODUCT_KEY);
	}

	public static getDerivedStateFromProps(props: IProductVideoPage, state: IProductVideoState) {

		if (props.match.url !== state.Url) {
			return {
				HasDataLoaded: false
			};
		}

		return null;
	}

	public componentDidMount() {

		this.handleProductVideosChange(this.props);
	}

	public componentDidUpdate(prevProps: IProductVideoPage, prevState: IProductVideoState) {
		if (this.props.match.url !== prevProps.match.url) {
			this.handleProductVideosChange(this.props);
		}
	}

	public componentWillUnmount() {
		this.destroyStream.next(true);
		this.destroyStream.unsubscribe();
	}

	private getFoundProduct = (allProducts: Array<IProduct>, urlKey: string): IProduct => {

		const foundProducts = new Collection(allProducts).Where(x => x.UrlKey.toLowerCase() === urlKey.toLowerCase());

		if (foundProducts.Any()) {
			return foundProducts.FirstOrDefault();
		}

		return null;
	}

	private getProductVideosData = (product: IProduct): void => {

		CommonService.AjaxGet('/api/Products/GetProductVideo?productId=' + product.ProductId).pipe(take(1))
			.subscribe((response: IProductVideo[]) => {

				if (response !== null) {

					this.setState({
						HasDataLoaded: true,
						Is404Redirect: false,
						Product: product,
						AllVideosForProduct: response,
						SelectedVideo: response.length > 0 ? response[0] : null
					});
				}

			});
	}

	private handleProductVideosChange = (props: IProductVideoPage): void => {
		this.setState({ Url: props.match.url });

		const foundProduct: IProduct = this.getFoundProduct(this.allProducts, props.match.params[Consts.PRODUCT_URLKEY]);
		if (foundProduct !== null) {

			this.getProductVideosData(foundProduct);
		}
		else {
			this.setState({ Is404Redirect: true });
		}
	}

	private handleChangePhoto = (event: any, selectVideo: IProductVideo, videoIndex: number): void => {
		this.setState({ SelectedVideo: selectVideo });
	}

	render() {

		const { Is404Redirect, HasDataLoaded, Product,
			AllVideosForProduct, SelectedVideo } = this.state;

		if (Is404Redirect) {
			return <PageNotFound />
		}

		if (!HasDataLoaded) {
			return <LinearProgress color="secondary" />;
		}

		return (
			<div className="product-video-container">

				<ProductHeaderComponent IsFeature={false} IsPhotoGallery={false} IsShowroom={false}
					IsVideoGallery={true} IsSpec={false} ProductName={Product.Name}
					ProductYear={Product.Year} ProductUrlKey={Product.UrlKey} />

				<div className="main-video col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<video src={SelectedVideo.FileName} autoPlay={true} controls={true}
						controlsList="nodownload" poster={SelectedVideo.ThumbnailName}></video>
				</div>
				<div className="video-grid col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<GridList cols={6} className="video-list" id="photoGalleryListId">
						{
							AllVideosForProduct.map((video: IProductVideo, index: number) => (
								<GridListTile key={index} id={index.toString()}
									className={SelectedVideo.Id === video.Id ? "active" : ""}>

									<img src={video.ThumbnailName} alt={video.Title} onClick={event => this.handleChangePhoto(event, video, index)} />
								</GridListTile>
						))}
					</GridList>
				</div>
			</div>
		);

	}
}