﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX

import * as React from "react";
import * as ReactDOM from "react-dom";
import { take } from 'rxjs/operators';
import { Subject } from 'rxjs';

import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import LinearProgress from '@material-ui/core/LinearProgress';

import { Consts } from 'globalshare/consts';
import { Collection } from "globalshare/core/Collection";
import { StorageUtility } from "globalshare/core/Storage";

import { IProduct } from 'globalmodels/product/iproduct';
import { IProductFeaturesPage, IProductFeaturesState } from 'globalmodels/props/product/iproductfeaturespage';

import { CommonService } from 'globalservices/shared/common.service';

import { PageNotFound } from './../../../global-components/notfound/pagenotfound';
import { ProductHeaderComponent } from './../header/product-header.component';
import './product-features.component.scss';

export class ProductFeaturesComponent extends React.Component<IProductFeaturesPage, IProductFeaturesState> {

	private allProducts: Array<IProduct> = [];
	private destroyStream: Subject<boolean> = new Subject<boolean>();

	constructor(props: IProductFeaturesPage) {
		super(props);
		this.state = {
			Is404Redirect: false,
			HasDataLoaded: false,
			Url: '',
			Product: null
		};

		this.allProducts = StorageUtility.GetLocal(Consts.PRODUCT_KEY);
	}

	public static getDerivedStateFromProps(props: IProductFeaturesPage, state: IProductFeaturesState) {

		if (props.match.url !== state.Url) {
			return {
				HasDataLoaded: false
			};
		}

		return null;
	}

	public componentDidMount() {

		this.handleProductFeaturesChange(this.props);
	}

	public componentDidUpdate(prevProps: IProductFeaturesPage, prevState: IProductFeaturesState) {
		if (this.props.match.url !== prevProps.match.url) {
			this.handleProductFeaturesChange(this.props);
		}
	}

	public componentWillUnmount() {
		this.destroyStream.next(true);
		this.destroyStream.unsubscribe();
	}

	private getFoundProduct = (allProducts: Array<IProduct>, urlKey: string): IProduct => {

		const foundProducts = new Collection(allProducts).Where(x => x.UrlKey.toLowerCase() === urlKey.toLowerCase());

		if (foundProducts.Any()) {
			return foundProducts.FirstOrDefault();
		}

		return null;
	}

	private handleProductFeaturesChange = (props: IProductFeaturesPage): void => {
		this.setState({ Url: props.match.url });

		const foundProduct: IProduct = this.getFoundProduct(this.allProducts, props.match.params[Consts.PRODUCT_URLKEY]);
		if (foundProduct !== null) {

			this.setState({ Is404Redirect: false, HasDataLoaded: true, Product: foundProduct });
			
		}
		else {
			this.setState({ Is404Redirect: true });
		}
	}

	render() {

		const { Is404Redirect, HasDataLoaded, Product } = this.state;

		if (Is404Redirect) {
			return <PageNotFound />
		}

		if (!HasDataLoaded) {
			return <LinearProgress color="secondary" />;
		}

		return (
			<div className="product-feature-container">

				<ProductHeaderComponent IsFeature={true} IsPhotoGallery={false} IsShowroom={false}
					IsVideoGallery={false} IsSpec={false} ProductName={Product.Name}
					ProductYear={Product.Year} ProductUrlKey={Product.UrlKey} />

				<iframe frameBorder="0" scrolling="no" marginWidth={0} marginHeight={0} seamless={true}
					width={1230} height={600} id="featureFrameId" sandbox="allow-scripts"
					src="http://www.kawasaki-la.com/Content/Features/721/la-en/features.html?v6"></iframe>
			</div>
		);

	}
}