﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX

import * as React from "react";
import * as ReactDOM from "react-dom";
import { take } from 'rxjs/operators';
import { Subject } from 'rxjs';

import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import LinearProgress from '@material-ui/core/LinearProgress';

import { Consts } from 'globalshare/consts';
import { Collection } from "globalshare/core/Collection";
import { StorageUtility } from "globalshare/core/Storage";

import { IProduct } from 'globalmodels/product/iproduct';
import {
	IProductPhotoPage, IProductPhotoState, IProductPhoto
} from 'globalmodels/props/product/iproductphotopage';

import { CommonService } from 'globalservices/shared/common.service';
import { ProductHeaderComponent } from './../header/product-header.component';
import { PageNotFound } from './../../../global-components/notfound/pagenotfound';

import './product-photo.component.scss';

export class ProductPhotoComponent extends React.Component<IProductPhotoPage, IProductPhotoState> {

	private allProducts: Array<IProduct> = [];
	private destroyStream: Subject<boolean> = new Subject<boolean>();

	constructor(props: IProductPhotoPage) {
		super(props);
		this.state = {
			Is404Redirect: false,
			HasDataLoaded: false,
			Url: '',
			Product: null,
			SelectedImage: null,
			AllPhotosForProduct: []
		};

		this.allProducts = StorageUtility.GetLocal(Consts.PRODUCT_KEY);
	}

	public static getDerivedStateFromProps(props: IProductPhotoPage, state: IProductPhotoState) {

		if (props.match.url !== state.Url) {
			return {
				HasDataLoaded: false
			};
		}

		return null;
	}

	public componentDidMount() {

		this.handleProductPhotosChange(this.props);
	}

	public componentDidUpdate(prevProps: IProductPhotoPage, prevState: IProductPhotoState) {
		if (this.props.match.url !== prevProps.match.url) {
			this.handleProductPhotosChange(this.props);
		}
	}

	public componentWillUnmount() {
		this.destroyStream.next(true);
		this.destroyStream.unsubscribe();
	}

	private getFoundProduct = (allProducts: Array<IProduct>, urlKey: string): IProduct => {

		const foundProducts = new Collection(allProducts).Where(x => x.UrlKey.toLowerCase() === urlKey.toLowerCase());

		if (foundProducts.Any()) {
			return foundProducts.FirstOrDefault();
		}

		return null;
	}

	private getProductPhotosData = (product: IProduct): void => {

		CommonService.AjaxGet('/api/Products/GetProductPhotos?productId=' + product.ProductId).pipe(take(1))
			.subscribe((response: IProductPhoto[]) => {

				if (response !== null) {

					this.setState({
						HasDataLoaded: true,
						Is404Redirect: false,
						Product: product,
						AllPhotosForProduct: response,
						SelectedImage: response.length > 0 ? response[0] : null
					});
				}

			});
	}

	private handleProductPhotosChange = (props: IProductPhotoPage): void => {
		this.setState({ Url: props.match.url });

		const foundProduct: IProduct = this.getFoundProduct(this.allProducts, props.match.params[Consts.PRODUCT_URLKEY]);
		if (foundProduct !== null) {

			this.getProductPhotosData(foundProduct);
		}
		else {
			this.setState({ Is404Redirect: true });
		}
	}

	private handleChangePhoto = (event: any, selectImage: IProductPhoto): void => {
		this.setState({ SelectedImage: selectImage });
	}

	render() {

		const { Is404Redirect, HasDataLoaded, Product, SelectedImage } = this.state;

		if (Is404Redirect) {
			return <PageNotFound />
		}

		if (!HasDataLoaded) {
			return <LinearProgress color="secondary" />;
		}

		return (
			<div className="product-photo-container">

				<ProductHeaderComponent IsFeature={false} IsPhotoGallery={true} IsShowroom={false}
					IsVideoGallery={false} IsSpec={false} ProductName={Product.Name}
					ProductYear={Product.Year} ProductUrlKey={Product.UrlKey} />

				<div className="row photo-gallery-container">
					<div className="main-photo col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<img src={SelectedImage.FileName} alt={SelectedImage.Title} />
					</div>
					<div className="photo-grid col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<GridList cols={6} className="photo-list" id="photoGalleryListId">
							{this.state.AllPhotosForProduct.map((photo: IProductPhoto, index: number) => (
								<GridListTile key={index} id={index.toString()}
									className={SelectedImage.Id === photo.Id ? "active" : ""}>

									<img src={photo.FileName} alt={photo.Title} onClick={event => this.handleChangePhoto(event, photo)} />
								</GridListTile>
							))}
						</GridList>
					</div>
				</div>
			</div>
		);

	}
}