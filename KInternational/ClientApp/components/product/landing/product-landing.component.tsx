﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX

import * as React from "react";
import * as ReactDOM from "react-dom";
import { Link } from 'react-router-dom';
import { take } from 'rxjs/operators';
import { Subject } from 'rxjs';

import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import Close from '@material-ui/icons/Close';
import Snackbar from '@material-ui/core/Snackbar';
import LinearProgress from '@material-ui/core/LinearProgress';

import { Consts } from 'globalshare/consts';
import { Collection } from "globalshare/core/Collection";
import { StorageUtility } from "globalshare/core/Storage";

import { IProduct } from 'globalmodels/product/iproduct';
import {
	IProductPage, IProductState, IProductInfoViewModel,
	IColorPhotoColor, IFeaturedSpecifications
} from 'globalmodels/props/product/iproductpage';

import { CommonService } from 'globalservices/shared/common.service';

import { PageNotFound } from './../../../global-components/notfound/pagenotfound';

import './product-landing.component.scss';

export class ProductLandingComponent extends React.Component<IProductPage, IProductState> {

	private allProducts: Array<IProduct> = [];
	private destroyStream: Subject<boolean> = new Subject<boolean>();

	constructor(props: IProductPage) {
		super(props);
		this.state = {
			Is404Redirect: false,
			HasDataLoaded: false,
			Url: '',
			Product: null,
			ProductInfo: {
				ColorPhotos: [],
				FeaturedSpecifications: []
			},
			SelectedColorPhoto: null,
			OpenSnackbar: false
		};

		this.allProducts = StorageUtility.GetLocal(Consts.PRODUCT_KEY);
	}

	public static getDerivedStateFromProps(props: IProductPage, state: IProductState) {

		if (props.match.url !== state.Url) {
			return {
				HasDataLoaded: false
			};
		}

		return null;
	}

	public componentDidMount() {

		this.handleProductChange(this.props);
	}

	public componentDidUpdate(prevProps: IProductPage, prevState: IProductState) {
		if (this.props.match.url !== prevProps.match.url) {
			this.handleProductChange(this.props);
		}
	}

	public componentWillUnmount() {
		this.destroyStream.next(true);
		this.destroyStream.unsubscribe();
	}

	private handleProductChange = (props: IProductPage): void => {
		this.setState({ Url: props.match.url });

		const foundProduct: IProduct = this.getFoundProduct(this.allProducts, props.match.params[Consts.PRODUCT_URLKEY]);
		if (foundProduct !== null) {

			this.getProductData(foundProduct);
		}
		else {
			this.setState({ Is404Redirect: true });
		}
	}

	private getFoundProduct = (allProducts: Array<IProduct>, urlKey: string): IProduct => {

		const foundProducts = new Collection(allProducts).Where(x => x.UrlKey.toLowerCase() === urlKey.toLowerCase());

		if (foundProducts.Any()) {
			return foundProducts.FirstOrDefault();
		}

		return null;
	}

	private getProductData(product: IProduct): void {

		CommonService.AjaxGet('/api/Products/GetProductInfo?productId=' + product.ProductId).pipe(take(1))
			.subscribe((response: IProductInfoViewModel) => {

				if (response !== null) {

					this.setState({
						HasDataLoaded: true,
						Is404Redirect: false,
						Product: product,
						ProductInfo: {
							ColorPhotos: response.ColorPhotos,
							FeaturedSpecifications: response.FeaturedSpecifications
						},
						SelectedColorPhoto: response.ColorPhotos.length > 0 ? response.ColorPhotos[0] : null,
						OpenSnackbar: false
					});
				}

			});
	}

	private handleChangeColorOfProduct = (event: any, index: number) => {

		const productColorPhotos = this.state.ProductInfo.ColorPhotos;
		this.setState({
			SelectedColorPhoto: productColorPhotos.length > 0 ? productColorPhotos[index] : null
		});
	}

	private handleReleatedProducts = (event: any) => {
		this.setState({
			OpenSnackbar: true
		});
	}

	private handleCloseReleatedProducts = (event: any) => {
		this.setState({
			OpenSnackbar: false
		});
	}

	render() {

		const { Is404Redirect, HasDataLoaded, OpenSnackbar } = this.state;

		if (Is404Redirect) {
			return <PageNotFound />
		}

		if (!HasDataLoaded) {
			return <LinearProgress color="secondary" />;
		}

		return (
			<div className="product-container row">

				<div className="product-options col-lg-3 col-md-12 col-sm-12 col-xs-12">
					<h1 onClick={(event) => this.handleReleatedProducts(event)}><span>{this.state.Product.Year} {this.state.Product.Name}</span></h1>
					<ul className="product-info-tabs row">
						<li className="col-lg-5">
							<Link to={"/product/" + this.state.Product.UrlKey}>Showroom</Link>
						</li>

						<li className="col-lg-5">
							<Link to={"/product/" + this.state.Product.UrlKey + "/photos"}>Gallery</Link>
						</li>

						<li className="col-lg-5">
							<Link to={"/product/" + this.state.Product.UrlKey + "/features"}>Features</Link>
						</li>

						<li className="col-lg-5">
							<Link to={"/product/" + this.state.Product.UrlKey + "/specifications"}>Specifications</Link>
						</li>

					</ul>

					<Card className="color-card">
						<CardHeader className="color-card-title" title="Colors" />
						<CardContent className="color-card-content">
							<div className="row">
								{
									this.state.ProductInfo !== undefined && this.state.ProductInfo !== null &&
									this.state.ProductInfo.ColorPhotos.map((colorPhotoColor: IColorPhotoColor, index: number) => {

										return (

											<div className="color-content" key={colorPhotoColor.ColorId}>

												<div className="swatch-container">
													<img data-colorname={colorPhotoColor.ColorName}
														src={colorPhotoColor.ColorImageUrl}
														onClick={event => this.handleChangeColorOfProduct(event, index)}
													/>
												</div>

											</div>
										)

									})
								}
							</div>
							<div className="row">
								{
									this.state.SelectedColorPhoto != null &&
									<div className="color-name-content">
										<div className="active-color-name">{this.state.SelectedColorPhoto.ColorName}</div>
									</div>
								}

							</div>
						</CardContent>
					</Card>

				</div>

				{
					this.state.SelectedColorPhoto != null &&
					<div className="color-photo-container col-lg-5 col-md-12 col-sm-12 col-xs-12">
						<img src={this.state.SelectedColorPhoto.PhotoImageUrl} />
					</div>
				}

				<div className="spec-container col-lg-3 col-md-12 col-sm-12 col-xs-12">
					<div className="spec-headline">Specifications</div>
					{
						this.state.ProductInfo !== undefined && this.state.ProductInfo !== null &&
						this.state.ProductInfo.FeaturedSpecifications.map((featureSpec: IFeaturedSpecifications, index: number) => {

							return (
								<Card className="spec-item">
									<CardHeader className="spec-title-header" title={featureSpec.SpecificationTitle} />
									<CardContent>
										<div className="spect-description">
											{featureSpec.SpecificationValue}
										</div>
									</CardContent>
								</Card>
							)

						})
					}
				</div>

				<Snackbar
					open={OpenSnackbar}
					message={
						<span id="message-id">I love snacks</span>
					}
					action={[
						<IconButton className="snack-close" onClick={(event) => this.handleCloseReleatedProducts(event)}>
							<Close />
						</IconButton>
					]}
				/>
			</div>
		);

	}
}