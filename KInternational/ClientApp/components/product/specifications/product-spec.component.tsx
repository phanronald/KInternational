﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX

import * as React from "react";
import * as ReactDOM from "react-dom";
import { take } from 'rxjs/operators';
import { Subject } from 'rxjs';

import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import LinearProgress from '@material-ui/core/LinearProgress';

import { Consts } from 'globalshare/consts';
import { Collection } from "globalshare/core/Collection";
import { StorageUtility } from "globalshare/core/Storage";

import { IProduct } from 'globalmodels/product/iproduct';
import { ISpecGroup } from 'globalmodels/product/ispecgroup';

import {
	IProductSpecPage, IProductSpecState, IProductSpecGroupWithSpecs,
	IProductSpecificationViewModel
} from 'globalmodels/props/product/iproductspecificationpage';

import { CommonService } from 'globalservices/shared/common.service';

import { ProductHeaderComponent } from './../header/product-header.component';
import { PageNotFound } from './../../../global-components/notfound/pagenotfound';

import './product-spec.component.scss';

export class ProductSpecificationComponent extends React.Component<IProductSpecPage, IProductSpecState> {

	private allProducts: Array<IProduct> = [];
	private destroyStream: Subject<boolean> = new Subject<boolean>();

	constructor(props: IProductSpecPage) {
		super(props);
		this.state = {
			Is404Redirect: false,
			HasDataLoaded: false,
			Url: '',
			Product: null,
			SpecificationGroups: [],
			ProductSpecGroupWithSpecs: []
		};

		this.allProducts = StorageUtility.GetLocal(Consts.PRODUCT_KEY);
	}

	public static getDerivedStateFromProps(props: IProductSpecPage, state: IProductSpecState) {

		if (props.match.url !== state.Url) {
			return {
				HasDataLoaded: false
			};
		}

		return null;
	}

	public componentDidMount() {

		this.handleProductSpecificationChange(this.props);
	}

	public componentDidUpdate(prevProps: IProductSpecPage, prevState: IProductSpecState) {
		if (this.props.match.url !== prevProps.match.url) {
			this.handleProductSpecificationChange(this.props);
		}
	}

	public componentWillUnmount() {
		this.destroyStream.next(true);
		this.destroyStream.unsubscribe();
	}

	private getFoundProduct = (allProducts: Array<IProduct>, urlKey: string): IProduct => {

		const foundProducts = new Collection(allProducts).Where(x => x.UrlKey.toLowerCase() === urlKey.toLowerCase());

		if (foundProducts.Any()) {
			return foundProducts.FirstOrDefault();
		}

		return null;
	}

	private getProductSpecificationData = (product: IProduct): void => {

		CommonService.AjaxGet('/api/Products/GetProductSpecifications?productId=' + product.ProductId).pipe(take(1))
			.subscribe((response: IProductSpecificationViewModel) => {

				if (response !== null) {

					this.setState({
						HasDataLoaded: true,
						Is404Redirect: false,
						Product: product,
						ProductSpecGroupWithSpecs: response.ProductSpecGroupWithSpecs,
						SpecificationGroups: response.SpecificationGroups
					});
				}

			});
	}

	private handleProductSpecificationChange = (props: IProductSpecPage): void => {
		this.setState({ Url: props.match.url });

		const foundProduct: IProduct = this.getFoundProduct(this.allProducts, props.match.params[Consts.PRODUCT_URLKEY]);
		if (foundProduct !== null) {

			this.getProductSpecificationData(foundProduct);
		}
		else {
			this.setState({ Is404Redirect: true });
		}
	}

	render() {

		const { Is404Redirect, HasDataLoaded, Product,
				ProductSpecGroupWithSpecs, SpecificationGroups } = this.state;

		if (Is404Redirect) {
			return <PageNotFound />
		}

		if (!HasDataLoaded) {
			return <LinearProgress color="secondary" />;
		}

		return (
			<div className="product-spec-container">

				<ProductHeaderComponent IsFeature={false} IsPhotoGallery={false} IsShowroom={false}
					IsVideoGallery={false} IsSpec={true} ProductName={Product.Name}
					ProductYear={Product.Year} ProductUrlKey={Product.UrlKey} />

				<div className="spec-image-container">
					<img src={Product.NavImageUrl.replace('.color', '').replace('.jpg', '.png')} />
				</div>

				<List className="row">
					{
						SpecificationGroups.length > 0 &&
						SpecificationGroups.map((specGroup: ISpecGroup) => {

							var currentSpecGroupSpec = new Collection(ProductSpecGroupWithSpecs)
								.Where(x => x.SpecGroupId === Number(specGroup.SpecGroupId));

							return (
								<div key={specGroup.Id} className="spec-group-container col-lg-6">
									<ListSubheader className="spec-group-header">
										{specGroup.Name}
									</ListSubheader>
									{
										currentSpecGroupSpec.ToArray().map((currentProdSpec: IProductSpecGroupWithSpecs, index: number) => {
											return (

												<ListItem className="spec-item-container">

													<ListItemText primary={currentProdSpec.SpecName} secondary={currentProdSpec.SpecValue} />

												</ListItem>
											)
										})
									}
								</div>
							)
						})
					}
				</List>
			</div>
		);

	}
}