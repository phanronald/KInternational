﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX

import * as React from "react";
import * as ReactDOM from "react-dom";
import { Link } from 'react-router-dom';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import AppBar from '@material-ui/core/AppBar';

import { ProductSection } from "./../../../models/props/product/productsection";
import { IProductHeader, IProductHeaderState } from 'globalmodels/props/product/iproductheader';

import './product-header.component.scss';

export class ProductHeaderComponent extends React.Component<IProductHeader, IProductHeaderState> {

	private destroyStream: Subject<boolean> = new Subject<boolean>();

	constructor(props: IProductHeader) {
		super(props);
		this.state = {
		};

	}

	public componentDidMount() {

	}

	public componentWillUnmount() {
		this.destroyStream.next(true);
		this.destroyStream.unsubscribe();
	}

	render() {

		return (
			<AppBar position="static" color="default" className="product-detail-sub-nav row">

				<div className="tech-type-nav col-lg-12 col-md-12 col-sm-12 col-xs-12">

					<ul className="display-table product-detail-type-listing">

						<li className="display-table-cell">
							<h1 className="product-detail-sub-nav-header">
								<span className="header-year">{this.props.ProductYear}</span>&nbsp;
								<span className="header-productname">{this.props.ProductName}</span>
							</h1>
						</li>

						<li className="display-table-cell">
							<Link id="showRoomHeaderId" to={"/product/" + this.props.ProductUrlKey + "/"} className={'product-detail-type ' + (this.props.IsShowroom ? 'active' : '')}>Showroom</Link>
						</li>

						<li className="display-table-cell">
							<Link id="featureHeaderId" to={"/product/" + this.props.ProductUrlKey + "/features"} className={'product-detail-type ' + (this.props.IsFeature ? 'active' : '')}>Features</Link>
						</li>

						<li className="display-table-cell">
							<Link id="specHeaderId" to={"/product/" + this.props.ProductUrlKey + "/specifications"} className={'product-detail-type ' + (this.props.IsSpec ? 'active' : '')}>Specifications</Link>
						</li>

						<li className="display-table-cell">
							<Link id="photoGalleryHeaderId" to={"/product/" + this.props.ProductUrlKey + "/photos"} className={'product-detail-type ' + (this.props.IsPhotoGallery ? 'active' : '')}>Photos</Link>
						</li>

						<li className="display-table-cell">
							<Link id="videoGalleryHeaderId" to={"/product/" + this.props.ProductUrlKey + "/videos"} className={'product-detail-type ' + (this.props.IsVideoGallery ? 'active' : '')}>Videos</Link>
						</li>

					</ul>

				</div>
			</AppBar>
		);

	}
}