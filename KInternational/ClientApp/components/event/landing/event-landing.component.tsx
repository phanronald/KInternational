﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX

import * as React from "react";
import * as ReactDOM from "react-dom";
import { Link } from 'react-router-dom';

import { IEventLandingPage } from 'globalmodels/props/event/ieventlandingpage';

import './event-landing.component.scss';

export class EventLanding extends React.Component<IEventLandingPage, any> {

	constructor(props: any) {
		super(props);
	}

	public componentDidMount() {

	}

	public componentWillUnmount() {

	}

	render() {

		return (
			<div className="event-landing-container row">
				<div className="header col-lg-9 col-md-11 col-sm-9 col-xs-11">
					<div className="col-lg col-md col-sm col-xs">
						<h1 className="content-header-text">Worldwide Kawasaki Racing and Events</h1>
					</div>
				</div>
				<div className="event-landing-body row">

					<div className="row">

						<div className="global-event-container col-lg col-md col-sm col-xs col-sm-offset-2 col-xs-offset-1">
							<Link to="/events/global">
								<img src="/images/events_landing_global_english.jpg" alt="kawasaki Latin America" />
							</Link>
						</div>
						<div className="regional-event-container col-lg col-md col-sm col-xs col-sm-offset-2 col-xs-offset-1">
							<Link to="/events/regional">
								<img src="/images/events_landing_region_english.jpg" alt="kawasaki Latin America" />
							</Link>
						</div>

					</div>

				</div>
			</div>
		);
	}
}