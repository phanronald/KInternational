﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX

import * as React from "react";
import * as ReactDOM from "react-dom";
import { Link } from 'react-router-dom';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import AppBar from '@material-ui/core/AppBar';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import { Consts } from 'globalshare/consts';
import { Collection } from "globalshare/core/Collection";
import { StorageUtility } from "globalshare/core/Storage";

import { ICountry } from "globalmodels/country/icountry";
import { EventType } from 'globalmodels/props/event/eventtype';
import { EventCategoryType } from 'globalmodels/props/event/eventcategorytype';
import { IEventHeader, IEventHeaderState } from "globalmodels/props/event/ieventheader";
import { IEvent } from 'globalmodels/props/event/ieventpage';

import './event-header.component.scss';

export class EventHeader extends React.Component<IEventHeader, IEventHeaderState> {

	private countriesAvailable: ICountry[] | null = [];
	private destroyStream: Subject<boolean> = new Subject<boolean>();

	constructor(props: IEventHeader) {
		super(props);
		this.state = {
			IsGlobal: false,
			IsGlobalRacing: false,
			IsGlobalConsumer: false,
			IsRegional: false,
			IsRegionalRacing: false,
			IsRegionalConsumer: false,
			CountriesToDisplay: [],
			CountrySelected: 0
		};

		this.countriesAvailable = StorageUtility.GetLocal(Consts.COUNTRY_KEY);
	}

	public componentDidMount() {

		this.props.service.eventCategoryTypeStream.pipe(takeUntil(this.destroyStream))
			.subscribe((eventCategoryType: [EventType, EventCategoryType]) => {

				const currentEventType = eventCategoryType[0];
				const currentEventCategoryType = eventCategoryType[1];
				if (currentEventType !== EventType.None) {

					this.setState({
						IsGlobal: currentEventType === EventType.Global && currentEventCategoryType === EventCategoryType.None,
						IsRegional: currentEventType === EventType.Regional && currentEventCategoryType === EventCategoryType.None
					});

					if (currentEventCategoryType[1] !== EventCategoryType.None) {

						this.setState({
							IsGlobalRacing: currentEventType === EventType.Global && currentEventCategoryType === EventCategoryType.Racing,
							IsGlobalConsumer: currentEventType === EventType.Global && currentEventCategoryType === EventCategoryType.Consumer,
							IsRegionalRacing: currentEventType === EventType.Regional && currentEventCategoryType === EventCategoryType.Racing,
							IsRegionalConsumer: currentEventType === EventType.Regional && currentEventCategoryType === EventCategoryType.Consumer
						});
					}
					else {

						this.setState({
							IsGlobalRacing: false, IsGlobalConsumer: false,
							IsRegionalRacing: false, IsRegionalConsumer: false
						});
					}
				}
				else {
					this.setState({
						IsGlobal: false, IsGlobalRacing: false,
						IsGlobalConsumer: false, IsRegional: false,
						IsRegionalRacing: false, IsRegionalConsumer: false
					});
				}

			});

		this.props.service.eventCountryDropdownStream.pipe(takeUntil(this.destroyStream))
			.subscribe((eventData: [IEvent[], EventType, EventCategoryType]) => {
				const events = eventData[0];
				const currentEventType = eventData[1];
				const currentEventCategoryType = eventData[2];
				this.setupCountryDropdownlist(events, currentEventType, currentEventCategoryType);
			});

	}

	public componentWillUnmount() {
		this.destroyStream.next(true);
		this.destroyStream.unsubscribe();
	}

	private setupCountryDropdownlist = (events: IEvent[], eventType: EventType,
										eventCategoryType: EventCategoryType): void => {

		if (eventType === EventType.Regional) {
			if (this.countriesAvailable != null && this.countriesAvailable.length > 0) {

				const filteredEvents: Collection<IEvent> = new Collection(events);
				let allCountryIds: Collection<number> = filteredEvents.Where(x => x.CountryId.length > 0)
					.Select(x => x.CountryId)
					.SelectMany(x => new Collection(x))
					.Distinct();

				let countriesCollection: Collection<ICountry> = new Collection(this.countriesAvailable);
				countriesCollection = countriesCollection.Where(x => allCountryIds.Contains(x.CountryId));
				this.setState({ CountriesToDisplay: countriesCollection.ToArray() });
			}
		}
	}

	private handleCountryChange = (event): void => {
		this.props.service.eventCountrySelected(event.target.value);
		this.setState({ CountrySelected: event.target.value });
	};

	render() {

		const { IsGlobal, IsRegional, IsGlobalConsumer, IsGlobalRacing,
			IsRegionalConsumer, IsRegionalRacing, CountrySelected } = this.state;

		return (
			<AppBar position="static" color="default" className="event-sub-nav row">

				<div className="event-type-nav col-lg-12 col-md-12 col-sm-12 col-xs-12">

					<ul className="row event-type-listing">

						<li className="">
							<Link to='/events/global' className={'event-type all-events ' + (IsGlobal ? 'active' : '')}>
								All Global
							</Link>
						</li>

						<li className="">
							<Link to='/events/global/racing' className={'event-type ' + (IsGlobalRacing ? 'active' : '')}>
								Global Racing
							</Link>
						</li>

						<li className="">
							<Link to='/events/global/consumer' className={'event-type ' + (IsGlobalConsumer ? 'active' : '')}>
								Global Consumer
							</Link>
						</li>

						<li className="">
							<Link to='/events/regional' className={'event-type all-events ' + (IsRegional ? 'active' : '')}>
								All Regional
							</Link>
						</li>

						<li className="">
							<Link to='/events/regional/racing' className={'event-type ' + (IsRegionalRacing ? 'active' : '')}>
								Regional Racing
							</Link>
						</li>

						<li className="">
							<Link to='/events/regional/consumer' className={'event-type ' + (IsRegionalConsumer ? 'active' : '')}>
								Regional Consumer
							</Link>
						</li>

						{
							(IsRegional || IsRegionalRacing || IsRegionalConsumer) &&
							<li className="country-dropdown-events">
								<Select className="select-menu-item" value={CountrySelected} onChange={this.handleCountryChange}>
									<MenuItem value={0}>Select Country</MenuItem>
									{
										this.state.CountriesToDisplay.map((country: ICountry, index: number) => {

											return (
												<MenuItem value={country.CountryId} key={index}>{country.Name}</MenuItem>
											)

										})
									}
								</Select>
							</li>
						}
					</ul>

				</div>

			</AppBar>
		);
	}
}