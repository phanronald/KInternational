﻿
import { BehaviorSubject, Observable } from 'rxjs';

import { EventType } from 'globalmodels/props/event/eventtype';
import { EventCategoryType } from 'globalmodels/props/event/eventcategorytype';
import { IEvent } from 'globalmodels/props/event/ieventpage';

export class EventService {

	public eventCategoryTypeStream = new BehaviorSubject<[EventType, EventCategoryType]>([EventType.None, EventCategoryType.None]);
	public eventCountryDropdownStream = new BehaviorSubject<[IEvent[], EventType, EventCategoryType]>([[], EventType.None, EventCategoryType.None]);
	public eventCountrySelectedStream = new BehaviorSubject<number>(0);

	constructor() {

	}

	public clickToEventCategoryType = (eventCategoryType: [EventType, EventCategoryType]): void => {
		this.eventCategoryTypeStream.next(eventCategoryType);
	}

	public eventSetupCountryDropdown = (eventsDisplayed: [IEvent[], EventType, EventCategoryType]): void => {
		this.eventCountryDropdownStream.next(eventsDisplayed);
	}

	public eventCountrySelected = (countryValue: number): void => {
		this.eventCountrySelectedStream.next(countryValue);
	}
}