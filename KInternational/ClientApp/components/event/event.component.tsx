﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX

import * as React from "react";
import * as ReactDOM from "react-dom";
import { Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import LinearProgress from '@material-ui/core/LinearProgress';

import { Consts } from 'globalshare/consts';
import { Collection } from "globalshare/core/Collection";
import { StorageUtility } from "globalshare/core/Storage";

import { EventType } from 'globalmodels/props/event/eventtype';
import { EventCategoryType } from 'globalmodels/props/event/eventcategorytype';
import {
	IEvent,
	IEventPage,
	IEventPageState
} from 'globalmodels/props/event/ieventpage';

import { CommonService } from 'globalservices/shared/common.service';
import { EventService } from './services/event.service';

import { EventHeader } from './header/event-header.component';
import { EventLanding } from './landing/event-landing.component';
import { PageNotFound } from './../../global-components/notfound/pagenotfound';

import './event.component.scss';

export class EventComponent extends React.Component<IEventPage, IEventPageState> {

	private currentEventType: EventType = EventType.None;
	private currentEventCategoryType: EventCategoryType = EventCategoryType.None;
	private eventService: EventService;
	private destroyStream: Subject<boolean> = new Subject<boolean>();

	constructor(props: any) {
		super(props);
		this.state = {
			Is404Redirect: false,
			HasDataLoaded: false,
			Url: '',
			IsLandingPage: false,
			IsCatgoryTypeLanding: false,
			IsEventTypeLanding: false,
			EventsToDisplay: []
		};

		this.eventService = new EventService();
	}

	public static getDerivedStateFromProps(props: IEventPage, state: IEventPageState) {

		if (props.match.url !== state.Url) {
			return {
				HasDataLoaded: false
			};
		}

		return null;
	}

	public componentDidMount() {
		this.handleEventChange(this.props);
		this.eventService.eventCountrySelectedStream.pipe(takeUntil(this.destroyStream))
			.subscribe((countryId: number) => {

			let eventsByCountry: IEvent[] = StorageUtility.GetLocal(Consts.EVENT_KEY);
			const currentFilteredEvents: Collection<IEvent> = new Collection(eventsByCountry);
			if (countryId !== 0) {
				eventsByCountry = currentFilteredEvents.Where(x => x.CountryId.some(y => y === countryId)).ToArray();
			}
			else {
				eventsByCountry = currentFilteredEvents.Where(x => x.EventType === EventType.Regional).ToArray();
			}
			
			this.setState({
				EventsToDisplay: eventsByCountry
			});

		});
	}

	public componentDidUpdate(prevProps: IEventPage, prevState: IEventPageState) {
		if (this.props.match.url !== prevProps.match.url) {
			this.handleEventChange(this.props);
		}
	}

	public componentWillUnmount() {
		this.destroyStream.next(true);
		this.destroyStream.unsubscribe();
	}

	private handleEventChange = (props: IEventPage): void => {

		const currentEventType: string = props.match.params[Consts.EVENT_TYPE];
		if (currentEventType === undefined) {
			this.setState({
				IsLandingPage: true, IsCatgoryTypeLanding: false,
				IsEventTypeLanding: false, HasDataLoaded: true
			});

			return;
		}

		const currentEventCategoryType: string = props.match.params[Consts.EVENT_CATEGORY_TYPE];
		this.getEventsData(currentEventType, currentEventCategoryType);
	}

	private getEventsData(eventType: string, eventCategoryType: string): void {

		let eventsAvailable: Array<IEvent> = StorageUtility.GetLocal(Consts.EVENT_KEY);
		if (eventsAvailable === undefined || eventsAvailable === null) {

			CommonService.AjaxGet('/api/Events/GetAllEvents').pipe(take(1))
				.subscribe((response: IEvent[]) => {

					if (response) {
						eventsAvailable = response;
						StorageUtility.StoreLocal(Consts.EVENT_KEY, eventsAvailable, Consts.EXPIRATION_OF_ONE_WEEK);
						const filteredEvents = this.getFilteredEvents(eventsAvailable, eventType, eventCategoryType);

						this.setState({
							EventsToDisplay: filteredEvents
						});

						this.eventService.clickToEventCategoryType([this.currentEventType, this.currentEventCategoryType]);
						this.eventService.eventSetupCountryDropdown([filteredEvents, this.currentEventType, this.currentEventCategoryType]);
					}

				});
		}
		else {

			const filteredEvents = this.getFilteredEvents(eventsAvailable, eventType, eventCategoryType);
			this.setState({
				EventsToDisplay: filteredEvents
			});

			this.eventService.clickToEventCategoryType([this.currentEventType, this.currentEventCategoryType]);
			this.eventService.eventSetupCountryDropdown([filteredEvents, this.currentEventType, this.currentEventCategoryType]);
		}

		this.setState({
			IsLandingPage: false, HasDataLoaded: true
		});
	}

	private getFilteredEvents(events: IEvent[], eventType: string, eventCategoryType: string): Array<IEvent> {

		const filteredEvents: Collection<IEvent> = new Collection(events);

		let finalFilteredArray: Collection<IEvent> = new Collection();
		switch (eventType.toLowerCase()) {
			case EventType[EventType.Global].toLowerCase(): {
				finalFilteredArray = filteredEvents.Where(x => x.EventType === EventType.Global);
				this.currentEventType = EventType.Global;
				break;
			}
			case EventType[EventType.Regional].toLowerCase(): {
				finalFilteredArray = filteredEvents.Where(x => x.EventType === EventType.Regional);
				this.currentEventType = EventType.Regional;
				break;
			}
			default: {
				finalFilteredArray = filteredEvents;
				this.currentEventType = EventType.None;
				break;
			}
		}

		if (eventCategoryType === undefined || eventCategoryType === null) {
			this.setState({ IsEventTypeLanding: true, IsCatgoryTypeLanding: false });
			this.currentEventCategoryType = EventCategoryType.None;
			return finalFilteredArray.ToArray();
		}

		switch (eventCategoryType.toLowerCase()) {
			case EventCategoryType[EventCategoryType.Racing].toLowerCase(): {
				finalFilteredArray = finalFilteredArray.Where(x => x.CategoryType === EventCategoryType.Racing);
				this.currentEventCategoryType = EventCategoryType.Racing;
				break;
			}
			case EventCategoryType[EventCategoryType.Consumer].toLowerCase(): {
				finalFilteredArray = finalFilteredArray.Where(x => x.CategoryType === EventCategoryType.Consumer);
				this.currentEventCategoryType = EventCategoryType.Consumer;
				break;
			}
			default: {
				this.currentEventCategoryType = EventCategoryType.None;
				break;
			}
		}

		this.setState({ IsEventTypeLanding: false, IsCatgoryTypeLanding: true });
		return finalFilteredArray.ToArray();
		
	}

	render() {

		const { Is404Redirect, HasDataLoaded, IsLandingPage,
			IsEventTypeLanding, IsCatgoryTypeLanding, EventsToDisplay } = this.state;

		if (Is404Redirect) {
			return <PageNotFound />
		}

		if (!HasDataLoaded) {
			return <LinearProgress color="secondary" />;
		}

		return (
			<div className="event-page">
				{
					!IsLandingPage &&
					<EventHeader service={this.eventService} />
				}
				<>
					{
						IsLandingPage && <EventLanding />
					}
				</>

				<>
					{
						!IsLandingPage &&
						<div className="event-listing-container row">

							{
								EventsToDisplay !== undefined && EventsToDisplay !== null &&
								EventsToDisplay.map((event: IEvent, index: number) => {

									return (
										<div className="event-listing col-lg-4 col-md-4 col-sm-6 col-xs-12" key={index} data-uid={event.Id}>
											<Card className="event-card">
												<CardMedia image={event.Image} title={event.Title} className="event-image" />
												<CardContent>
													<Typography variant="headline" component="div" className="event-title">
														{event.Title}
													</Typography>
													<Typography component="div" className="event-description">
														{event.Description}
													</Typography>
												</CardContent>
												<CardActions>
													<Button size="small" color="primary" className="event-seemore" href={event.Link}>
														Read More
													</Button>
												</CardActions>
											</Card>
										</div>
									)

								})
							}

						</div>

					}
				</>

			</div>
		);

	}
}