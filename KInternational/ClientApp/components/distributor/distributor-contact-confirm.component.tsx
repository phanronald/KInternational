﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX

import * as React from "react";
import * as ReactDOM from "react-dom";
import { Subject } from 'rxjs';

import './distributor-contact-confirm.component.scss';

export class DistributorContactConfirmComponent extends React.Component<any, any> {

	private destroyStream: Subject<boolean> = new Subject<boolean>();

	constructor(props: any) {
		super(props);
		this.state = {
		};
	}

	public componentDidMount() {
;
	}

	public componentWillUnmount() {
		this.destroyStream.next(true);
		this.destroyStream.unsubscribe();
	}

	render() {

		return (
			<div className="distributor-confirmation">
				CONFIRMATION
			</div>
		);

	}
}