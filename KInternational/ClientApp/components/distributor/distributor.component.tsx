﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX

import * as React from "react";
import * as ReactDOM from "react-dom";
import { Link } from 'react-router-dom';
import { Subject } from 'rxjs';
import { take } from 'rxjs/operators';
import Typography from '@material-ui/core/Typography';
import LinearProgress from '@material-ui/core/LinearProgress';
import Close from '@material-ui/icons/Close';
import LocationOn from '@material-ui/icons/LocationOn';

import { Consts } from 'globalshare/consts';
import { StorageUtility } from 'globalshare/core/Storage';
import { Collection } from 'globalshare/core/Collection';

import { ICountry } from 'globalmodels/country/icountry';
import { IDistributor } from 'globalmodels/distributor/idistributor';
import { IDealer } from 'globalmodels/distributor/idealer';
import {
	IDistributorsPage,
	IDistributorsState,
	IDistributorDealerViewModel
} from 'globalmodels/props/distributor/idistributorspage';

import { CommonService } from 'globalservices/shared/common.service';
import { DistributorService } from 'globalservices/component/distributor.service';

import { BingMaps } from './../../global-components/map/bing/bingmaps';
import { PageNotFound } from './../../global-components/notfound/pagenotfound';

import './distributor.component.scss';

declare var bingMapApiKey: any;

export class DistributorComponent extends React.Component<IDistributorsPage, IDistributorsState> {

	private allCountries: ICountry[] | null;
	private distributorsAvailable: IDistributor[] | null;
	private dealersAvailable: IDealer[] | null;
	private bingMapUrl: string = 'https://www.bing.com/api/maps/mapcontrol?key=' + bingMapApiKey;
	private bingMapUrlScript: HTMLScriptElement;

	private destroySubscription: Subject<boolean> = new Subject<boolean>();

	constructor(props: any) {
		super(props);
		this.state = {
			Is404Redirect: false,
			HasDataLoaded: false,
			CountryDistributorViewModel: {
				AllDistributors: [],
				AllDealers: []
			},
			SelectedCountryString: "all",
			SelectedCountry: null,
			SelectedDistributor: null,
			SelectedDealers: [],
			SelectedDealer: null,
			IsMapOpen: false,
			Url: ''
		};

		this.allCountries = StorageUtility.GetLocal(Consts.COUNTRY_KEY);
		this.bingMapUrlScript = DistributorService.SetUpBingMaps(this.bingMapUrl);
	}

	public static getDerivedStateFromProps(props: IDistributorsPage, state: IDistributorsState) {

		if (props.match.url !== state.Url) {
			return {
				HasDataLoaded: false
			};
		}

		return null;
	}

	public componentDidMount() {

		this.distributorsAvailable = StorageUtility.GetLocal(Consts.DISTRIBUTOR_KEY);
		this.dealersAvailable = StorageUtility.GetLocal(Consts.DEALER_KEY);

		if ((this.distributorsAvailable === undefined || this.distributorsAvailable === null) ||
			(this.dealersAvailable === undefined || this.dealersAvailable === null)) {

			CommonService.AjaxGet('/api/Distributors/GetDistributorsDealers').pipe(take(1))
				.subscribe((response: IDistributorDealerViewModel) => {

					const distroDealerCountriesModel = response as IDistributorDealerViewModel;
					this.distributorsAvailable = distroDealerCountriesModel.AllDistributors;
					this.dealersAvailable = distroDealerCountriesModel.AllDealers;

					StorageUtility.StoreLocal(Consts.DISTRIBUTOR_KEY, this.distributorsAvailable, Consts.EXPIRATION_OF_ONE_WEEK);
					StorageUtility.StoreLocal(Consts.DEALER_KEY, this.dealersAvailable, Consts.EXPIRATION_OF_ONE_WEEK);

					this.handleDistributorChange(this.props);
				});
		}
		else {
			this.handleDistributorChange(this.props);
		}
	}

	public componentDidUpdate(prevProps: IDistributorsPage, prevState: IDistributorsState) {
		if (this.props.match.url !== prevProps.match.url) {
			this.handleDistributorChange(this.props);
		}
	}

	public componentWillUnmount() {
		this.destroySubscription.next(true);
		this.destroySubscription.unsubscribe();

		if (this.bingMapUrlScript !== null) {
			this.bingMapUrlScript.parentNode.removeChild(this.bingMapUrlScript);
		}
	}

	private handleDistributorChange = (props: IDistributorsPage): void => {

		const currentCountryName: string = props.match.params[Consts.COUNTRY_NAME];
		if (this.allCountries !== null) {

			if (currentCountryName !== undefined) {

				const countryFound: ICountry | undefined = new Collection(this.allCountries)
					.Where(x => x !== undefined && x.Name.replace(/ /g, "").toLowerCase() ==
						currentCountryName.replace('-', '').toLowerCase())
					.FirstOrDefault();

				if (countryFound === undefined || countryFound === null) {

					this.setState({
						Is404Redirect: true,
						Url: props.match.url
					});

					return;
				}
			}

			this.setupDistributorState(currentCountryName, props.match.url);
		}
	}

	private countryIconMouseOver = (event: any): void => {
		const target: HTMLElement = (event.target as HTMLElement);
		const urlKey: string = target.dataset.urlkey.toLowerCase();
		if (this.state.SelectedCountry == null || urlKey !== this.state.SelectedCountry.UrlKey.toLowerCase()) {
			let bigMapElement: HTMLElement = document.querySelector('.map-image-container');
			bigMapElement.style.backgroundImage = 'url(/images/maps/' + urlKey.toLowerCase().replace('-', '_') + '.jpg)';
		}
	}

	private countryIconMouseOut = (event: any): void => {
		const target: HTMLElement = (event.target as HTMLElement);
		const urlKey: string = target.dataset.urlkey.toLowerCase();
		if (this.state.SelectedCountry == null || urlKey !== this.state.SelectedCountry.UrlKey.toLowerCase()) {
			let bigMapElement: HTMLElement = document.querySelector('.map-image-container');
			bigMapElement.style.backgroundImage = '';
		}
	}

	private setupDistributorState = (currentCountryName:string, currentUrl:string): void => {

		const selectedCountryString = DistributorService.DetermineSelectedCountryString(currentCountryName).toLocaleLowerCase();
		const selectedDistroDealerCountry = DistributorService.DetermineSelectedDistributor(selectedCountryString,
			this.allCountries, this.distributorsAvailable, this.dealersAvailable);

		this.setState({
			Is404Redirect: false,
			HasDataLoaded: true,
			CountryDistributorViewModel: {
				AllDistributors: this.distributorsAvailable,
				AllDealers: this.dealersAvailable
			},
			SelectedCountryString: selectedCountryString,
			SelectedCountry: selectedDistroDealerCountry.SelectedCountry,
			SelectedDistributor: selectedDistroDealerCountry.SelectedDistributor,
			SelectedDealers: selectedDistroDealerCountry.SelectedDealers,
			Url: currentUrl
		});
	}

	private dealerClickHandler = (dealer: IDealer): void => {
		this.setState({
			SelectedDealer: dealer,
			IsMapOpen: true
		});
	}

	private closeMapClickHandler = (event: any): void => {
		this.setState({
			IsMapOpen: false
		});
	}

	render() {

		const { HasDataLoaded, Is404Redirect, SelectedCountry, IsMapOpen,
			SelectedDistributor, SelectedDealers, SelectedDealer } = this.state;

		if (Is404Redirect) {
			return <PageNotFound />
		}

		if (!HasDataLoaded) {
			return <LinearProgress color="secondary" />;
		}

		return (
			<div className="distributors-container">

				<div className="distributors-listing-container row">
					<div className="distributors-listing col-lg-3 col-md-3 col-sm-4 col-xs-8">

						<div className="distributor-title">

							<Typography component="h2" className="distributor-title-header">
								Select a Country
							</Typography>

							<Typography component="p" className="distributor-title-description">
								Please select a country to find your closest dealer’s contact information & more.
							</Typography>

						</div>

						<ul className="distributor-countries-list">
							{
								this.allCountries.map((country: ICountry, index: number) => {

									const isSelected: boolean = this.state.SelectedCountryString.toLowerCase() === country.UrlKey.toLowerCase() ? true : false;

									return (
										<li key={index} data-uid={country.CountryId}>
											<Link to={"/distributors/" + country.UrlKey}
												data-urlkey={country.UrlKey}
												className={country.UrlKey.toLowerCase() + " " + (isSelected ? "selected" : "")}
												onMouseOver={event => this.countryIconMouseOver(event)}
												onMouseOut={event => this.countryIconMouseOut(event)}>

												{country.Name}
											</Link>
										</li>
									)

								})
							}

						</ul>

					</div>

					<div className="distributors-map col-lg-6 col-md-6 col-sm-5">
						<div className={"map-image-container " + (SelectedCountry != null ? SelectedCountry.UrlKey.toLowerCase() : "")}></div>
					</div>

					<div className="distributors-contact-info col-lg-3 col-md-3 col-sm-3 col-xs-4">

						{
							SelectedCountry !== null && SelectedDistributor !== null &&
							<div className="distributor-info">
								<Typography component="h1" className="distributor-offical-header">
									Kawasaki Official Importer of {SelectedCountry.Name}
								</Typography>

								<div className="distributor-offical-info">

									<div className="distributor-name">{SelectedDistributor.Name}</div>
									<div className="distributor-address">
										<span dangerouslySetInnerHTML={{ __html: DistributorService.GetCorrectDisplayOfAddress(SelectedDistributor.Address) }} />
										<span>{SelectedCountry.Name}</span>
									</div>
									<div className="distributor-phone">
										{SelectedDistributor.Phone}
									</div>

									<div className="distributor-social-sites">

										<Link to={"/distributors/" + SelectedCountry.UrlKey + "/contact"} className="distributor-contact">Contact</Link>
										<a href={SelectedDistributor.Website} className="distributor-website">Website</a>
										<div dangerouslySetInnerHTML={{ __html: DistributorService.GetCorrectDisplayOfSocialMedia(SelectedDistributor.SocialMedia) }}>
										</div>
									</div>
								</div>
							</div>
						}

						{
							SelectedCountry !== null && SelectedDistributor !== null && SelectedDealers.length > 0 &&
							<div className="dealer-info">
								<Typography component="h1" className="dealer-offical-header">
									KAWASAKI OFFICIAL DEALERS OF {SelectedCountry.Name}
								</Typography>

								<div className="dealer-offical-info">

									{
										SelectedDealers.map((dealer: IDealer, index: number) => {

											const hasLatLong: boolean = dealer.Latitude !== "" && dealer.Longitude !== "";

											return (
												<div key={index} className="dealers-container">
													<div className="dealer-name">{dealer.Name}</div>
													<div className="distributor-address">
														<span dangerouslySetInnerHTML={{ __html: DistributorService.GetCorrectDisplayOfAddress(dealer.Address) }} />
													</div>
													<div className="dealer-phone">
														{dealer.Phone}
													</div>

													{
														hasLatLong &&
														<div className="dealer-lat-long">
															<span>
																<a className="dealer-map-link"
																	onClick={(event) => this.dealerClickHandler(dealer)}>

																	<LocationOn />
																	View in Bing Maps
																</a>
															</span>
														</div>
													}
												</div>
											)
										})
									}
								</div>
							</div>
						}
					</div>
				</div>

				{
					SelectedDealer !== null && IsMapOpen &&
					<div className="distributor-map">
						<Close className="modal-close-button" onClick={event => this.closeMapClickHandler(event)} />
						<BingMaps Latitude={SelectedDealer.Latitude}
							Longitude={SelectedDealer.Longitude}
							Title={SelectedDealer.Name}
							SubTitle={SelectedDealer.Address.City} />
					</div>
				}

			</div>
		);
	}
}