﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX

import * as React from "react";
import * as ReactDOM from "react-dom";
import { Link } from 'react-router-dom';
import { Subject } from 'rxjs';
import { take } from 'rxjs/operators';
import Typography from '@material-ui/core/Typography';
import LinearProgress from '@material-ui/core/LinearProgress';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import Switch from '@material-ui/core/Switch';
import Button from '@material-ui/core/Button';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import FormControl from '@material-ui/core/FormControl';

import { Consts } from 'globalshare/consts';
import { Collection } from 'globalshare/core/Collection';
import { StorageUtility } from 'globalshare/core/Storage';

import { ICountry } from 'globalmodels/country/icountry';
import { IDistributor } from 'globalmodels/distributor/idistributor';
import {
	IDistributorContactPage,
	IDistributorContactState
} from 'globalmodels/props/distributor/idistributorscontactpage';
import { IContactForm } from 'globalmodels/distributor/icontactform';
import { IContactFormValidation } from 'globalmodels/distributor/icontactformvalidation';

import { CommonService } from 'globalservices/shared/common.service';
import { DistributorService } from 'globalservices/component/distributor.service';

import { PageNotFound } from './../../global-components/notfound/pagenotfound';

import './distributor-contact.component.scss';

export class DistributorContactComponent extends React.Component<IDistributorContactPage, IDistributorContactState> {

	private countriesAvailable: ICountry[] = [];
	private countryForContact: ICountry | undefined;
	private destroySubscription: Subject<boolean> = new Subject<boolean>();

	constructor(props: any) {
		super(props);
		this.state = {
			Is404Redirect: false,
			HasDataLoaded: false,
			Url: '',
			ContactDistributor: null,
			ContactRequestType: '',
			ContactFirstName: '',
			ContactLastName: '',
			ContactEmail: '',
			ContactPhone: '',
			ContactMobilePhone: '',
			ContactCity: '',
			ContactVehicleModel: '',
			ContactVehicleYear: '',
			ContactOwnership: '',
			ContactComments: '',
			InterestMoto: false,
			InterestATV: false,
			InterestJetSki: false,
			InterestMule: false,
			InterestTeryx: false
		};

		this.countriesAvailable = StorageUtility.GetLocal(Consts.COUNTRY_KEY);
	}

	public componentDidMount() {

		const currentCountryName: string = this.props.match.params[Consts.COUNTRY_NAME];
		if (this.countriesAvailable.length > 0) {
			if (currentCountryName !== undefined) {

				this.countryForContact = new Collection(this.countriesAvailable)
					.Where(x => x !== undefined && x.Name.replace(/ /g, "").toLowerCase() ==
						currentCountryName.replace('-', '').toLowerCase())
					.FirstOrDefault();

				if (this.countryForContact === undefined) {

					this.setState({
						Is404Redirect: true,
						Url: this.props.match.url
					});

					return;
				}

				CommonService.AjaxGet('/api/Distributors/GetDistributorByCountryId?countryId=' + this.countryForContact.CountryId)
					.pipe(take(1))
					.subscribe((currentDistributor: IDistributor) => {

						this.setState({
							ContactDistributor: currentDistributor,
							HasDataLoaded: true,
							Is404Redirect: false,
							Url: this.props.match.url
						});
						
					});
			}
		}
	}

	public componentWillUnmount() {
		this.destroySubscription.next(true);
		this.destroySubscription.unsubscribe();
	}

	private selectChange = (event: React.ChangeEvent<HTMLElement>, name: any): void => {

		const currentSelectInput: HTMLSelectElement = (event.target as HTMLSelectElement);
		this.setState(this.updateContactState(name, currentSelectInput.options[currentSelectInput.selectedIndex].text));

		const distroValidation = DistributorService.ValidateFormItem('select', event.target.id, (event.target as HTMLSelectElement).name);
		DistributorService.RemoveErrorMessageElement(event.target.id);

		DistributorService.CreateErrorElement(distroValidation.IsValid,
			distroValidation.IsEmailValid, distroValidation.IsPhoneValid,
			distroValidation.HTMLElementId, distroValidation.HTMLElementName);
	};

	private switchCheckbox = (event: React.ChangeEvent<HTMLElement>, checked: boolean, name: any): void => {
		this.setState(this.updateContactState(name, (event.target as HTMLInputElement).checked));
	}

	private textboxChange = (event: React.ChangeEvent<HTMLElement>, name: any): void => {

		const currentTextbox: HTMLInputElement = (event.target as HTMLInputElement);
		this.setState(this.updateContactState(name, (event.target as HTMLInputElement).value));

		const distroValidation = DistributorService.ValidateFormItem((event.target as HTMLInputElement).type,
			event.target.id, (event.target as HTMLInputElement).name);

		DistributorService.RemoveErrorMessageElement(event.target.id);

		DistributorService.CreateErrorElement(distroValidation.IsValid,
			distroValidation.IsEmailValid, distroValidation.IsPhoneValid,
			distroValidation.HTMLElementId, distroValidation.HTMLElementName);
	}

	private submitContactForm = (event: React.MouseEvent<HTMLElement>): void => {

		const validationErrors = DistributorService.ValidateContactForm();
		const validationValueErrors = new Collection(validationErrors.GetValues());
		const allInValidElements = validationValueErrors.Where(x => x.IsValid === false);
		if (allInValidElements.Count() === 0) {

			let contactFormData: IContactForm = {
				DistributorId: this.state.ContactDistributor.DistributorId,
				RequestType: this.state.ContactRequestType,
				FirstName: this.state.ContactFirstName,
				LastName: this.state.ContactLastName,
				Email: this.state.ContactEmail,
				Phone: this.state.ContactPhone,
				MobilePhone: this.state.ContactMobilePhone,
				City: this.state.ContactCity,
				VehicleModel: this.state.ContactVehicleModel,
				VehicleYear: this.state.ContactVehicleYear,
				Ownership: this.state.ContactOwnership,
				InterestInKawasakiProductsMoto: this.state.InterestMoto,
				InterestInKawasakiProductsATV: this.state.InterestATV,
				InterestInKawasakiProductsJetSki: this.state.InterestJetSki,
				InterestInKawasakiProductsMule: this.state.InterestMule,
				InterestInKawasakiProductsTeryx: this.state.InterestTeryx,
				Comments: this.state.ContactComments,
			};

			CommonService.AjaxPost('/api/Distributors/SaveUserContactToDistributor', JSON.stringify(contactFormData))
				.pipe(take(1))
				.subscribe((response: any) => {

					if (new Number(response).valueOf() === 200) {
						this.props.history.push('/distributors/' + this.countryForContact.UrlKey + '/confirmation');
					}

				});
		}
		else {

			validationValueErrors.ForEach((distroValidation: IContactFormValidation, index: number) => {
				DistributorService.RemoveErrorMessageElement(distroValidation.HTMLElementId);
			});

			allInValidElements.ForEach((distroValidation: IContactFormValidation, index: number) => {

				if (!distroValidation.IsValid || !distroValidation.IsEmailValid) {

					DistributorService.CreateErrorElement(distroValidation.IsValid,
						distroValidation.IsEmailValid, distroValidation.IsPhoneValid,
						distroValidation.HTMLElementId, distroValidation.HTMLElementName);
				}
			});
		}
	}

	private updateContactState = <T extends string|boolean>(key: keyof IDistributorContactState, value: T) =>
		(prevState: IDistributorContactState): IDistributorContactState => ({
			...prevState,
			[key]: value
		});

	render() {

		const { HasDataLoaded, Is404Redirect } = this.state;

		if (Is404Redirect) {
			return <PageNotFound />
		}

		if (!HasDataLoaded) {
			return <LinearProgress color="secondary" />;
		}

		return (
			<div className="distro-contact-container">

				<div className="contact-form-header col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
					<div className="contact-header-info">CONTACT KAWASAKI DISTRIBUTOR</div>
					<div className="contact-info contact-info-name">{this.state.ContactDistributor.Name}</div>
					<div className="contact-info">{this.state.ContactDistributor.Phone}</div>
					<div className="contact-info contact-info-country">Kawasaki Official Importer of {this.countryForContact.Name}</div>
				</div>

				<div className="contact-form-container col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">

					<form id="contactFormId">
						<FormControl className="form-input select-input" required>
							<InputLabel htmlFor="request-type-native-helper">Request Type</InputLabel>
							<Select native defaultValue={this.state.ContactRequestType} input={<Input name="Request Type" id="request-type-native-helper" />}
								onChange={(event) => this.selectChange(event, 'ContactRequestType')}>
								<option value=""></option>
								<option>Request a Qoute</option>
								<option>Parts & Accessories Info</option>
								<option>General Questions</option>
								<option>Other</option>
							</Select>
						</FormControl>

						<TextField id="firstNameId" label="First Name" className="form-input long-inputtype"
							type="text" defaultValue={this.state.ContactFirstName} margin="normal" required
							name="First Name" onBlur={(event) => this.textboxChange(event, 'ContactFirstName')} />

						<TextField id="lastNameId" label="Last Name" className="form-input long-inputtype" type="text"
							defaultValue={this.state.ContactLastName} margin="normal" required
							name="Last Name" onBlur={(event) => this.textboxChange(event, 'ContactLastName')} />

						<TextField id="emailId" label="Email" className="form-input long-inputtype" type="email"
							defaultValue={this.state.ContactEmail} margin="normal" required
							name="Email" onBlur={(event) => this.textboxChange(event, 'ContactEmail')} />

						<TextField id="phoneId" label="Phone" className="form-input short-inputtype" type="tel"
							defaultValue={this.state.ContactPhone} margin="normal" inputProps={{ maxLength: 16 }}
							name="Phone Number" onBlur={(event) => this.textboxChange(event, 'ContactPhone')} />

						<TextField id="mobilePhoneId" label="Mobile Phone" className="form-input short-inputtype" type="tel"
							defaultValue={this.state.ContactMobilePhone} margin="normal" inputProps={{ maxLength: 16 }}
							name="Mobile Number" onBlur={(event) => this.textboxChange(event, 'ContactMobilePhone')} />

						<TextField id="cityId" label="City" className="form-input long-inputtype" type="text"
							defaultValue={this.state.ContactCity} margin="normal" required
							name="City" onBlur={(event) => this.textboxChange(event, 'ContactCity')} />

						<TextField id="vehicleModelId" label="Vehicle Model" className="form-input short-inputtype" type="text"
							defaultValue={this.state.ContactVehicleModel} margin="normal" required
							name="Vehicle Model" onBlur={(event) => this.textboxChange(event, 'ContactVehicleModel')} />

						<FormControl className="form-input select-input">
							<InputLabel htmlFor="vehicle-year-native-helper">Vehicle Year</InputLabel>
							<Select native defaultValue={this.state.ContactVehicleYear} input={<Input id="vehicle-year-native-helper" />}
								onChange={(event) => this.selectChange(event, 'ContactVehicleYear')} >
								<option value=""></option>
								<option>After 2012</option>
								<option>Between 2008 and 2011</option>
								<option>Between 2000 and 2008</option>
								<option>Between 1990 and 2000</option>
								<option>Between 1975 and 1990</option>
								<option>Before 1975</option>
							</Select>
						</FormControl>

						<FormControl className="form-input select-input">
							<InputLabel htmlFor="year-ownership-native-helper">Years of Ownership</InputLabel>
							<Select native defaultValue={this.state.ContactOwnership} input={<Input id="year-ownership-native-helper" />}
								onChange={(event) => this.selectChange(event, 'ContactOwnership')} >
								<option value=""></option>
								<option>1-2</option>
								<option>3-5</option>
								<option>6-10</option>
								<option>10-20</option>
								<option>More than 20</option>
							</Select>
						</FormControl>

						<FormGroup className="form-input switch-container">

							<div className="interest-kawi-product-container">
								<label className="interest-kawi-product">Interest in Kawasaki products</label>
							</div>

							<FormControlLabel className="switch-input"
								control={
									<Switch
										checked={this.state.InterestMoto}
										onChange={(event, checked) => this.switchCheckbox(event, checked, 'InterestMoto')}
									/>
								}
								label="Motorcycle"
							/>

							<FormControlLabel className="switch-input"
								control={
									<Switch
										checked={this.state.InterestATV}
										onChange={(event, checked) => this.switchCheckbox(event, checked, 'InterestATV')}
									/>
								}
								label="ATV"
							/>

							<FormControlLabel className="switch-input"
								control={
									<Switch
										checked={this.state.InterestJetSki}
										onChange={(event, checked) => this.switchCheckbox(event, checked, 'InterestJetSki')}
									/>
								}
								label="Jet Ski"
							/>

							<FormControlLabel className="switch-input"
								control={
									<Switch
										checked={this.state.InterestMule}
										onChange={(event, checked) => this.switchCheckbox(event, checked, 'InterestMule')}
									/>
								}
								label="Mule"
							/>

							<FormControlLabel className="switch-input"
								control={
									<Switch
										checked={this.state.InterestTeryx}
										onChange={(event, checked) => this.switchCheckbox(event, checked, 'InterestTeryx')}
									/>
								}
								label="Teryx"
							/>
						</FormGroup>

						<TextField id="commentsId" label="Questions or comments" className="form-input long-inputtype" required
							multiline rows="4" defaultValue={this.state.ContactComments} margin="normal"
							name="Comments" onBlur={(event) => this.textboxChange(event, 'ContactComments')} />

						<div className="submit-privacy-container">
							<label className="privacy-legal-lbl">
								By clicking SUBMIT, you verify that you are over 13 years of age and agree to Kawasaki's
							<Link to='/legal'> privacy policy</Link>.
						</label>
						</div>

						<Button variant="raised" className="send-button" onClick={(event) => this.submitContactForm(event)}>Send</Button>
					</form>
				</div>
			</div>
		);
	}
}