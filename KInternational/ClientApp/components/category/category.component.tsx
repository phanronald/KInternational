﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX

import * as React from "react";
import * as ReactDOM from "react-dom";
import { Link } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import LinearProgress from '@material-ui/core/LinearProgress';

import { Consts } from 'globalshare/consts';
import { StorageUtility } from 'globalshare/core/Storage';
import { Collection } from 'globalshare/core/Collection';

import { IProduct } from 'globalmodels/product/iproduct';
import { IProductCategory } from 'globalmodels/product/iproductcategory';
import { IProductSubCategory } from 'globalmodels/product/iproductsubcategory';
import { ICategory, ICategoryState } from "globalmodels/props/category/icategory";

import { PageNotFound } from './../../global-components/notfound/pagenotfound';
import { SpacerComponent } from './../../global-components/spacer/spacer';

import './category.component.scss';

export class CategoryComponent extends React.Component<ICategory, ICategoryState> {

	private categories: IProductCategory[] | null = [];
	private subCategories: IProductSubCategory[] | null = [];
	private products: IProduct[] | null = [];

	constructor(props: any) {
		super(props);

		this.categories = StorageUtility.GetLocal(Consts.CATEGORY_KEY);
		this.subCategories = StorageUtility.GetLocal(Consts.SUBCATEGORY_KEY);
		this.products = StorageUtility.GetLocal(Consts.PRODUCT_KEY);

		this.state = {
			CategorySelected: '0',
			CurrentCategoryName: '',
			ProductsToShow: [],
			SubCategoriesToShow: [],
			AllProductsPerCategory: [],
			Is404Redirect: false,
			HasDataLoaded: false,
			Url: ''
		};
	}

	public static getDerivedStateFromProps(props: ICategory, state: ICategoryState) {

		if (props.match.url !== state.Url) {
			return {
				HasDataLoaded: false
			};
		}

		return null;
	}

	public componentDidMount() {
		this.handleCategoryChange(this.props);
	}

	public componentDidUpdate(prevProps: ICategory, prevState: ICategoryState) {
		if (this.props.match.url !== prevProps.match.url) {
			this.handleCategoryChange(this.props);
		}
	}

	public componentWillUnmount() {

	}

	handleCategoryChange = (props: ICategory): void => {

		const currentCategoryName = props.match.params[Consts.CATEGORY_NAME];
		if (this.categories !== null) {
			const categoryFound: IProductCategory|undefined = new Collection(this.categories)
				.Where(x => x !== undefined && x.Name.toLowerCase() == currentCategoryName.toLowerCase())
				.FirstOrDefault();

			if (categoryFound === undefined || categoryFound === null) {

				this.setState({
					Is404Redirect: true,
					CategorySelected: '0',
					Url: props.match.url
				});

			}
			else {

				const categoryId: number = categoryFound.ProductCategoryId;

				let subCategoriesFromCategory: IProductSubCategory[] = [];
				if (this.subCategories !== null) {
					subCategoriesFromCategory = new Collection(this.subCategories)
						.Where(x => x !== undefined && x.ProductCategoryId == categoryId).ToArray();
				}

				let productsFromSelection: IProduct[] = [];
				if (this.products !== null) {
					productsFromSelection = new Collection(this.products)
						.Where(x => x !== undefined && x.CategoryId == categoryId).ToArray();
				}

				this.setState({
					ProductsToShow: productsFromSelection,
					SubCategoriesToShow: subCategoriesFromCategory,
					CurrentCategoryName: currentCategoryName,
					AllProductsPerCategory: productsFromSelection,
					Is404Redirect: false,
					CategorySelected: '0',
					HasDataLoaded: true,
					Url: props.match.url
				});
			}
		}
	}

	handleChange = (event: any, value: any) => {

		let newProductsToShow: IProduct[] = this.state.AllProductsPerCategory;
		const subCategoryId: number = parseInt(value, 10);
		if (!isNaN(subCategoryId) && subCategoryId !== 0) {

			newProductsToShow = new Collection(this.state.AllProductsPerCategory)
				.Where(x => x !== undefined && x.SubCategoryId == value).ToArray();
		}

		this.setState({
			ProductsToShow: newProductsToShow,
			CategorySelected: value,
			HasDataLoaded: true
		});
	};

	render() {

		const {
			SubCategoriesToShow,
			CategorySelected,
			CurrentCategoryName,
			Is404Redirect,
			HasDataLoaded
		} = this.state;

		if (Is404Redirect) {
			return <PageNotFound />
		}

		if (!HasDataLoaded) {
			return <LinearProgress color="secondary" />;
		}

		return (
			<div>
				<SpacerComponent />
				<div className="category-name-section-header">{CurrentCategoryName}</div>
				<div className="category-page-container">

					<AppBar position="static" color="default" className="category-sub-nav col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<Tabs
							value={CategorySelected} onChange={this.handleChange} indicatorColor="primary"
							textColor="primary" scrollable scrollButtons="auto">

							<Tab label="All" value="0" />
							{
								SubCategoriesToShow.map((subCategory: IProductSubCategory, index: number) => {

									return (
										<Tab key={index} label={subCategory.Name} value={subCategory.ProductSubCategoryId} />
									)
								})
							}

						</Tabs>
					</AppBar>
					<div className="category-product-list-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<ul className="category-product-list">
							{
								this.state.ProductsToShow.map((product: IProduct, index: number) => {

									return (
										<Typography key={index} component="li" style={{ padding: 8 * 1 }}>
											<Link to={'/product/' + product.UrlKey}>
												<img alt={product.Name} src={product.NavImageUrl} />
												<div className="category-product-info">
													<span>{product.Year}</span>&nbsp;
												<span>{product.Name}</span>
												</div>
											</Link>
										</Typography>
									)
								})
							}
						</ul>
					</div>
				</div>
			</div>
		);
	}
}