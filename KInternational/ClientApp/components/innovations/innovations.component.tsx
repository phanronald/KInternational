﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX

import * as React from "react";
import * as ReactDOM from "react-dom";
import { Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';

import { Modal } from "./../../global-components/modal/modal";

import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';

import {
	IInnovationsPage, IInnovationState, IInnovationData, IInnovationDataObj
} from "globalmodels/props/innovations/IInnovations";

import { CommonService } from 'globalservices/shared/common.service';
import { ModalService } from 'globalservices/modal/modal.service';

import './innovations.component.scss';

export class InnovationComponent extends React.Component<IInnovationsPage, IInnovationState> {

	private readonly DEFAULT_ANGLE: number = 60;
	private angleOfImages: number = 0;
	private slideToHaveOpactity: number = 1;
	private listOfInnovationsDescription: IInnovationData[] = [];

	private modalService: ModalService;
	private destroyStream: Subject<boolean> = new Subject<boolean>();	

	constructor(props: IInnovationsPage) {
		super(props);

		this.state = {
			IsVideoModalOpen: false,
			InnovationDescription: '',
			SlideNumber: 1,
			InnovationTitle: ''
		};

		this.modalService = new ModalService();
	}

	public componentDidMount() {

		this.modalService.openCloseModalEmitted.pipe(takeUntil(this.destroyStream)).subscribe((isOpen: boolean) => {
			this.setState({ IsVideoModalOpen: isOpen });
		});

		CommonService.AjaxGet('/data/innovation.json').pipe(take(1))
			.subscribe((response: any) => {

				this.listOfInnovationsDescription.push(...(response as IInnovationDataObj).Innovation);
				if (this.listOfInnovationsDescription.length > 0) {

					this.setState({
						InnovationDescription: this.listOfInnovationsDescription[0].Description,
						SlideNumber: this.slideToHaveOpactity,
						InnovationTitle: this.listOfInnovationsDescription[0].Title
					});
				}

		});

		const rootContainers: HTMLCollectionOf<Element> = document.getElementsByClassName('main-container');
		if (rootContainers.length > 0) {
			const rootContainer: HTMLDivElement = rootContainers[0] as HTMLDivElement;
			rootContainer.style.padding = '0px';
		}
	}

	public componentWillUnmount() {
		this.destroyStream.next(true);
		this.destroyStream.unsubscribe();
	}

	openVideoDialog = (event: any) => {
		this.setState({ IsVideoModalOpen: true });
	}

	moveCarouse = (event: any, direction: string) => {

		let carousel: HTMLElement = (document.querySelector('#carouselImagesId') as HTMLElement);
		if (direction === '+') {
			this.angleOfImages += this.DEFAULT_ANGLE;
		}
		else {
			this.angleOfImages -= this.DEFAULT_ANGLE;
		}

		carousel.setAttribute('style', 'transform: rotateY(' + this.angleOfImages + 'deg)');

		let allCarouselImages = (document.getElementById('carouselImagesId') as HTMLElement).childNodes;
		for (let i = 0; i < allCarouselImages.length; ++i) {
			const carouselImage: HTMLImageElement = allCarouselImages[i] as HTMLImageElement;
			carouselImage.style.opacity = "0.1";
		}

		if (direction === '+') {
			if (this.slideToHaveOpactity === 1) {
				this.slideToHaveOpactity = 6;
			}
			else {
				this.slideToHaveOpactity--;
			}
		}
		else {
			if (this.slideToHaveOpactity === 6) {
				this.slideToHaveOpactity = 1;
			}
			else {
				this.slideToHaveOpactity++;
			}
		}

		(allCarouselImages[this.slideToHaveOpactity - 1] as HTMLImageElement).style.opacity = "1";

		this.setState({
			InnovationDescription: this.listOfInnovationsDescription[this.slideToHaveOpactity - 1].Description,
			InnovationTitle: this.listOfInnovationsDescription[this.slideToHaveOpactity - 1].Title,
			SlideNumber: this.slideToHaveOpactity
		});
	}

	render() {

		return (
			<div className="innovations">
				<div className="row">

					<div className="inno-technology-listing col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-5 col-sm-offset-1 col-xs-6 col-xs-offset-1">
						<Card className="innovations-item">
							<CardHeader className="technology-info-header" title="Kawasaki's World of Innovation" />
							<CardContent>
								<div className="technology-information">
									<div className="inno-heading"><span>{this.state.InnovationTitle}</span></div>
									<div className="inno-copy">

										<span dangerouslySetInnerHTML={{ __html: this.state.InnovationDescription }} />
										{
											this.state.SlideNumber === 1 &&
											<div className="inno-video-link" onClick={event => this.openVideoDialog(event)}>
												<span>Kawasaki's World of Innovation</span>
											</div>
										}
									</div>

								</div>
							</CardContent>
							<CardActions>
								<Button size="small" color="primary" className="innovation-heavy-industries" href="http://www.khi.co.jp/english/index.html ">
									Visit Kawasaki Heavy Industries
								</Button>
							</CardActions>
						</Card>
					</div>

					<div className="inno-carousel inno-carousel col-lg-7 col-md-7 col-sm-6 col-xs-5">
						<span className="left-arrow-container" onClick={event => this.moveCarouse(event, '-')}><i className="inno-arrow left-arrow" /></span>
						<span className="right-arrow-container" onClick={event => this.moveCarouse(event, '+')}><i className="inno-arrow right-arrow" /></span>

						<figure id="carouselImagesId">
							<img src="http://www.kawasaki-la.com/images/inno/khi/poster.jpg" />
							<img src="http://www.kawasaki-la.com/images/inno/khi/aviation.jpg" />
							<img src="http://www.kawasaki-la.com/images/inno/khi/bullet_train.jpg" />
							<img src="http://www.kawasaki-la.com/images/inno/khi/plants.jpg" />
							<img src="http://www.kawasaki-la.com/images/inno/khi/robotics.jpg" />
							<img src="http://www.kawasaki-la.com/images/inno/khi/motorcycle_engines.jpg" />
						</figure>
					</div>

					<Modal open={this.state.IsVideoModalOpen} service={this.modalService}>

						<video controls autoPlay={true} controlsList="nodownload"
							src="http://www.kawasaki-la.com/cms/images/innovations/KHI_VIDEO_H264_640x360.MP4">
						</video>

					</Modal>
				</div>
			</div>
		);
	}
}