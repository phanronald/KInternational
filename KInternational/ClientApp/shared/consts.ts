﻿export class Consts {

	/* LOCAL STORAGE KEY */
	public static CATEGORY_KEY: string = "klaCategory";
	public static SUBCATEGORY_KEY: string = "klaSubCategory";
	public static PRODUCT_KEY: string = "klaProduct";
	public static COUNTRY_KEY: string = "klaCountries";
	public static EVENT_KEY: string = "klaEvents";
	public static EVENT_FILTER_KEY: string = "klaFilteredEvents";
	public static DISTRIBUTOR_KEY: string = "klaDistributors";
	public static DEALER_KEY: string = "klaDealers";

	/* DYNAMIC PARAMETERS */
	public static CATEGORY_NAME: string = 'catgoryname';
	public static EVENT_TYPE: string = 'eventtype';
	public static EVENT_CATEGORY_TYPE: string = 'categorytype';
	public static TECHNOLOGY_TYPE: string = 'technologytype';
	public static COUNTRY_NAME: string = 'countryName';
	public static PRODUCT_URLKEY: string = 'producturlkey';

	public static EXPIRATION_OF_ONE_WEEK: number = 10080;

	constructor() {

	}

}