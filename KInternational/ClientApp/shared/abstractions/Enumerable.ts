﻿import { IEnumerable } from 'globalshare/interfaces/IEnumerable';

export class Enumerable<T> implements IEnumerable<T> {

	// The underlying array data structure of the collection
	protected _items: Array<T> = [];

	constructor(items: T[] = []) {
		this._items = items;
	}

	// Add an object to the collection
	public Add = (item: T): void => {
		this._items.push(item);
	}

	public AddRange = (items: T[]): void => {
		this._items.push(...items);
	}

	public Aggregate = <U>(callbackfun: (previousValue: U, currentValue?: T, currentIndex?: number, list?: T[]) => any, initialValue?: U): any => {
		if (this.Count() === 0) {
			return undefined;
		}

		const newerInitialValue: T | U = initialValue === undefined ? (this.DefaultValuePerType(this.First()) as any) : initialValue;
		return this._items.reduce(callbackfun, newerInitialValue);
	}

	public All = (predicate: (value?: T, index?: number, list?: T[]) => boolean): boolean => {
		return this._items.every(predicate);
	}

	public Any = (expression?: (value?: T, index?: number, list?: T[]) => boolean): boolean => {
		return expression === undefined ? this._items.length > 0 : this._items.some(expression);
	}

	public Average = (expression?: (value?: T, index?: number, list?: T[]) => any): number => {

		if (this.Count() === 0) {
			return 0;
		}

		const collectionItemType = typeof this.ElementAt(0);
		if (collectionItemType !== "number") {
			throw new Error(collectionItemType + " does have the defintion of 'Average'");
		}

		return this.Sum(expression) / this.Count(expression);
	}

	/* cannot use fat arrow due to needng to use super to call it and it will not work
	 * https://stackoverflow.com/questions/31088947/inheritance-method-call-triggers-typescript-compiler-error
	 * */
	public Clear(): void {
		this._items = [];
	}

	public Concat = (second: Enumerable<T>): Enumerable<T> => {
		return new Enumerable<T>(this._items.concat(second.ToArray()));
	}

	public Contains = (item: T): boolean => {
		return this._items.some(x => x === item);
	}

	// Length of the collection
	public Count = (expression?: (value?: T, index?: number, list?: T[]) => boolean): number => {
		return expression === undefined ? this._items.length : this.Where(expression).Count();
	}

	public DeepCopy = (): IEnumerable<T> => {
		return new Enumerable<T>(JSON.parse(JSON.stringify(this._items)) as T[]);
	}

	public DefaultIfEmpty = (defaultValue?: T): Enumerable<T> => {
		if (this.Count() > 0) {
			return this;
		}

		const defaultArray: Array<T> = [];
		defaultArray.push((defaultValue as T));
		return (defaultValue === undefined ? new Enumerable<T>() : new Enumerable<T>(defaultArray));
	}

	public Distinct = (): Enumerable<T> => {
		return new Enumerable(Array.from(new Set(this._items).values()));
	}

	public DistinctBy = <U>(expression: (value?: T, index?: number, list?: T[]) => U): Enumerable<T> => {

		//this.GroupBy(expression, x => x);

		//let mappedPredicate = this.Select(expression);
		//this.Where((item, index, items) => {
		//	return mappedPredicate.IndexOf(null) === index;
		//});
		return this.Where((value, index, iter) => (iter as T[]).indexOf((value as T)) === (index as number));
		/* const distinctActivities = activities.filter((activity, index, activities) => {
		 * return activities.map(mappedActivity => mappedActivity.name).indexOf(activity.name) == index;
		 * });*/
	}

	// Get a specific item from a collection given it's index
	public ElementAt = (index: number): T => {
		if (this.Count() === 0 || index > this.Count()) {
			throw new Error('ArgumentOutOfRangeException: index is less than 0 or greater than or equal to the number of elements in source.');
		}

		return this._items[index];
	}

	public ElementAtOrDefault = (index: number): T|undefined => {
		//return this.ElementAt(index) || this.DefaultValuePerType(this._items);
		return this.ElementAt(index) || undefined;
	}

	public Empty = (): IEnumerable<T> => {
		return new Enumerable();
	}

	public Except = (source: Enumerable<T>): Enumerable<T> => {
		return this.Where(x => !source.Contains((x as T)));
	}

	public Exists = (predicate: (value?: T, index?: number, list?: T[]) => boolean): boolean => {
		return this.Where(predicate).Count() > 0;
	}

	public ForEach = (action: (value?: T, index?: number, list?: T[]) => any): void => {
		return this._items.forEach(action);
	}

	public First = (expression?: (value?: T, index?: number, list?: T[]) => boolean): T => {
		if (this.Count() === 0) {
			throw new Error('InvalidOperationException: The source sequence is empty.');
		}

		return expression === undefined ? this._items[0] : this.Where(expression).First();
	}

	public FirstOrDefault = (expression?: (value?: T, index?: number, list?: T[]) => boolean): T|undefined => {

		try {
			const firstWithExpression = this.First(expression);
			return firstWithExpression;
		}
		catch (e) {
			//return this.DefaultValuePerType(this._items);
			return undefined;
		}
	}

	public GetRange = (index: number, count: number): Enumerable<T> => {

		const absIndex: number = Math.abs(index);
		const absCount: number = Math.abs(count);
		const finalCount: number = absIndex + absCount > this._items.length ? (this._items.length - 1) : absIndex + absCount;
		return new Enumerable<T>(this._items.slice(absIndex, finalCount));
	}

	public GroupBy = (keySelector: (key: T) => any, elementSelector: (key: T) => any): any => {

		//let groupedEnumberable = new Enumberable<IGrouping<any, any>>();

		return this.Aggregate(
			(grouped, item) => ((<any>grouped)[keySelector((item as T))] ?
				(<any>grouped)[keySelector((item as T))].push(elementSelector((item as T))) :
				(<any>grouped)[keySelector((item as T))] = [elementSelector((item as T))], grouped),
			{});
	}

	public IndexOf = (item: T, startIndex?: number): number => {
		return this._items.indexOf(item, startIndex);
	}

	public Insert = (index: number, element: T): void => {
		if (index < 0 || index > this._items.length) {
			throw new Error('Index is out of range.');
		}

		this._items.splice(index, 0, element);
	}

	public InsertRange = (index: number, source: IEnumerable<T>): void => {
		let currentIndex = index;
		source.ToArray().forEach((item) => {
			this.Insert(currentIndex, item);
			currentIndex++;
		});
	}

	public Intersect = (source: Enumerable<T>): Enumerable<T> => {
		return this.Where(x => source.Contains((x as T)));
	}

	public Last = (expression?: (value?: T, index?: number, list?: T[]) => boolean): T => {

		if (this.Count() === 0) {
			throw new Error('InvalidOperationException: The source sequence is empty.');
		}

		return expression === undefined ? this._items[this._items.length - 1] : this.Where(expression).Last();
	}

	public LastOrDefault = (expression?: (value?: T, index?: number, list?: T[]) => boolean): T|undefined => {

		try {
			const lastWithExpression = this.Last(expression);
			return lastWithExpression;
		}
		catch (e) {
			//return this.DefaultValuePerType(this._items);
			return undefined;
		}
	}

	public LastIndexOf = (item: T, startIndex?: number): number => {
		return this._items.lastIndexOf(item, startIndex);
	}

	public Max = <U>(expression?: (value?: T, index?: number, list?: T[]) => U): T | U => {
		if (expression === undefined) {
			return <T>this.Aggregate((currentMax, currentValue) => currentMax > (currentValue as T) ? currentMax : currentValue);
		}

		let selectedProperty: Enumerable<U> = this.Select(expression);
		return selectedProperty.Max();
	}

	public Min = <U>(expression?: (value?: T, index?: number, list?: T[]) => U): T | U => {
		if (expression === undefined) {
			return <T>this.Aggregate((currentMin, currentValue) => currentMin < (currentValue as T) ? currentMin : currentValue);
		}

		let selectedProperty: Enumerable<U> = this.Select(expression);
		return selectedProperty.Min()
	}

	public OrderBy = (keySelector: (key: T) => any): Enumerable<T> => {
		let orderArrayComparer: ((a: T, b: T) => any) = this.ComparerForKey(keySelector, false);
		return new Enumerable(this._items.slice(0).sort(orderArrayComparer));
	}

	public OrderByDescending = (keySelector: (key: T) => any): Enumerable<T> => {
		let orderArrayComparer: ((a: T, b: T) => any) = this.ComparerForKey(keySelector, true);
		return new Enumerable(this._items.slice(0).sort(orderArrayComparer));
	}

	public Range = (start: number, count: number): IEnumerable<number> => {
		if (count < 0 || ((start + count) > Number.MAX_SAFE_INTEGER)) {
			throw new Error('The count cannot be less than 0 or the start with count cannot be bigger than max integer.');
		}

		let tempNumber: number = 0;
		let rangeArray: number[] = [];
		for (let i = 0; i < count; i = tempNumber + 1) {
			rangeArray.push(start + i);
			tempNumber = i;
		}

		return new Enumerable(rangeArray);
	}

	public Remove = (item: T): boolean => {
		return this._items.indexOf(item) !== -1 ? (this.RemoveAt(this._items.indexOf(item)), true) : false;
	}

	public RemoveAll = (predicate: (value?: T, index?: number, list?: T[]) => boolean): number => {
		const itemsToBeRemoved = this.Where(predicate);
		itemsToBeRemoved.ToArray().forEach((item) => {
			this.Remove(item);
		});

		return itemsToBeRemoved.Count();
	}

	// Delete an object from the collection
	public RemoveAt = (index: number): void => {
		this.RemoveRange(index, 1);
	}

	public RemoveRange = (index: number, count: number): void => {
		this._items.splice(index, count);
	}

	public Repeat = (item: T, count: number): IEnumerable<T> => {
		if (count < 0 || count > Number.MAX_SAFE_INTEGER) {
			throw new Error('The count cannot be less than 0 or more than max value of a number.');
		}

		let tempItems: IEnumerable<T> = new Enumerable();
		for (let i = 0; i < count; i++) {
			tempItems.Add(item);
		}

		return tempItems;
	}

	public Reverse = (): IEnumerable<T> => {
		const reversed = this.DeepCopy();
		return new Enumerable<T>(reversed.ToArray().reverse());
	}

	public Select = <U>(expression: (value?: T, index?: number, list?: T[]) => U): Enumerable<U> => {
		var newArrayMapper = this._items.map(expression);
		return new Enumerable<U>(newArrayMapper);
	}

	public SelectMany = <U extends Enumerable<any>>(expression: (value?: T, index?: number, list?: T[]) => U): U => {

		return this.Aggregate((groupedCollection: U, currentValue, currentIndex) =>
			(groupedCollection.AddRange(this.Select(expression).ElementAt((currentIndex as number)).ToArray()), groupedCollection), (new Enumerable<any>() as U));
	}

	public Single = (expression?: (value?: T, index?: number, list?: T[]) => boolean): T => {
		if (this.Count() !== 1) {
			throw new Error('The collection does not contain exactly one element.');
		}

		return this.First(expression);
	}

	public SingleOrDefault = (expression?: (value?: T, index?: number, list?: T[]) => boolean): T => {
		if (this.Count() > 1) {
			throw new Error('The collection contains more than one element.');
		}

		return this.Single(expression);
	}

	public Skip = (amount: number): Enumerable<T> => {
		if (this.Count() === 0) {
			return this;
		}

		const skippedArray = this._items.slice(Math.abs(amount));
		return new Enumerable<T>(skippedArray);
	}

	public SkipWhile = (expression?: (value?: T, index?: number, list?: T[]) => boolean): Enumerable<T> => {
		if (this.Count() === 0) {
			return this;
		}

		const itemsBasedOnExpression = this.Where(expression);
		return this.Except(itemsBasedOnExpression);
	}

	public Sum = (expression?: (value?: T, index?: number, list?: T[]) => number): number => {
		if (this.Count() === 0) {
			return 0;
		}

		const collectionItemType = typeof this.ElementAt(0);
		if (collectionItemType !== "number") {
			throw new Error(collectionItemType + " does have the defintion of 'Sum'");
		}

		return expression === undefined ? <number>this.Aggregate((result: number, currentValue) => result += (<number><any>currentValue)) : this.Select(expression).Sum();
	}

	public Take = (amount: number): Enumerable<T> => {
		if (this.Count() === 0) {
			return this;
		}

		const takenArray = this._items.slice(0, Math.abs(amount));
		return new Enumerable<T>(takenArray);
	}

	public TakeWhile = (expression?: (value?: T, index?: number, list?: T[]) => boolean): Enumerable<T> => {

		if (this.Count() === 0) {
			return this;
		}

		const itemsBasedOnExpression = this.Where(expression);
		return this.Intersect(itemsBasedOnExpression);
	}

	public Where = (expression?: (value?: T, index?: number, list?: T[]) => boolean): Enumerable<T> => {
		if (expression === undefined) {
			throw new Error('ArgumentNullException: The expression cannot be undefined or null.');
		}

		const filteredArray = this._items.filter(expression);
		return new Enumerable<T>(filteredArray);
	}

	public ToArray = (): T[] => {
		return this._items;
	}

	public Union = (second: Enumerable<T>): Enumerable<T> => {
		return this.Concat(second);
	}

	private ComparerForKey = (keySelector: (key: T) => any, descending?: boolean): ((a: T, b: T) => number) => {

		return (a: T, b: T) => {
			return this.Compare(a, b, keySelector, descending);
		};
	}

	private Compare = (a: T, b: T, keySelector: (key: T) => any, descending?: boolean): number => {
		const sortKeyA = keySelector(a);
		const sortKeyB = keySelector(b);
		if (sortKeyA > sortKeyB) {
			return (!descending ? 1 : -1);
		}

		if (sortKeyA < sortKeyB) {
			return (!descending ? -1 : 1);
		}

		return 0;
	}

	private DefaultValuePerType = (collectionType: any): T|undefined => {

		switch (typeof collectionType) {
			case "number":
				{
					return <T><any>0;
				}
			case "string":
				{
					return <T><any>"";
				}
			case "boolean":
				{
					return <T><any>false;
				}
			case "object":
				{
					let isArray: boolean = Array.isArray(collectionType);
					if (isArray) {
						return <T><any>[];
					}

					return undefined;
				}
			default: {
				return undefined;
			}
		};
	}

	private LookThroughGroupArray = (item: T, keyToGroup: (key: T) => T, groupedArray: Array<T[]>): T[] => {

		for (let i = 0; i < groupedArray.length; i++) {
			if (groupedArray[i].length > 0 && keyToGroup(groupedArray[i][0]) == item) {
				return groupedArray[i];
			}
		}

		return [];
	}
}