﻿import { Enumerable } from "globalshare/abstractions/Enumerable";

export class Collection<T> extends Enumerable<T> {

	constructor(items: T[] = []) {
		super(items);
	}
}