﻿import { IKeyValuePair } from 'globalshare/interfaces/IKeyValuePair';
import { IDictionary } from 'globalshare/interfaces/IDictionary';
import { IEnumerable } from 'globalshare/interfaces/IEnumerable';
import { Enumerable } from 'globalshare/abstractions/Enumerable';

export class Dictionary<K, V> extends Enumerable<IKeyValuePair<K, V>> implements IDictionary<K, V>, IEnumerable<IKeyValuePair<K, V>> {

	protected internalArray: any = {};

	constructor(items: IKeyValuePair<K, V>[] = []) {
		super(items);
		this.Clear();
		for (var kvPair of items) {
			this.AddKeyValue(kvPair.key, kvPair.value);
		}
	}

	public AddKeyValue = (key: K, value: V): void => {
		this.internalArray[String(key)] = value;
		this.Add({ key: key, value: value });
	}

	/* cannot use fat arrow due to needng to use super to call it and it will not work
	 * https://stackoverflow.com/questions/31088947/inheritance-method-call-triggers-typescript-compiler-error
	 * */
	public Clear():void {
		this.internalArray = {};
		super.Clear();
	}

	public ContainsKey = (key: K): boolean => {
		return (this.GetItem(key) !== undefined);
	}

	public ContainsValue = (value: V): boolean => {

		const allValues = this.GetValues();
		for (var val of allValues) {
			if (val === value) {
				return true;
			}
		}

		return false;
	}

	public GetKeys = (): K[] => {
		let keyArray: K[] = [];

		Object.keys(this.internalArray).map((key, index) => {

			keyArray.push(JSON.parse(key) as K);

		});

		return keyArray;
	}

	public GetItem = (key: K): V => {
		return this.internalArray[String(key)];
	}

	public GetValue = (key: K): V | null => {
		const kvPair: IKeyValuePair<K, V> = this.internalArray[String(key)];
		if (typeof (kvPair) === 'undefined') {
			return null;
		}

		return kvPair.value;
	}

	public GetValues = (): V[] => {

		let valueArray: V[] = [];

		Object.keys(this.internalArray).map((key, index) => {

			valueArray.push(this.internalArray[String(key)]);

		});

		return valueArray;
	}

	public ToArray = (): IKeyValuePair<K, V>[] => {

		let dictionaryInArray: IKeyValuePair<K, V>[] = [];

		Object.keys(this.internalArray).map((key, index) => {

			const kvPair = { key: JSON.parse(key) as K, value: this.internalArray[String(key)] };
			dictionaryInArray.push(kvPair);
		});

		return dictionaryInArray;
	}

	private EmptyDictionary = (): Dictionary<K, V> => {
		return new Dictionary<K, V>();
	}
}