﻿import { IKeyValuePair } from './IKeyValuePair';

export interface IDictionary<K, V> {
	AddKeyValue(key: K, value: V): void;
	Clear(): void;
	ContainsKey(key: K): boolean;
	ContainsValue(value: V): boolean;
	GetKeys(): K[];
	GetItem(key: K): V;
	GetValue(key: K): V|null;
	GetValues(): V[];
	ToArray(): IKeyValuePair<K, V>[];
}