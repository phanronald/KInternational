﻿export interface StorageModel {
	value: string;
	timestamp: number;
}