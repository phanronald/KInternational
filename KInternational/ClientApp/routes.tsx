import * as React from 'react';
import { Route, Switch } from 'react-router-dom';
import { Layout } from 'globalcomponents/layout';

import { AboutComponent } from './components/about/about.component';
import { HomeComponent } from './components/home/home.component';
import { CategoryComponent } from './components/category/category.component';
import { DistributorComponent } from './components/distributor/distributor.component';
import { DistributorContactComponent } from './components/distributor/distributor-contact.component';
import { DistributorContactConfirmComponent } from './components/distributor/distributor-contact-confirm.component';
import { EventComponent } from './components/event/event.component';
import { InnovationComponent } from './components/innovations/innovations.component';
import { LegalComponent } from './components/legal/legal.component';
import { TechnologyComponent } from './components/technology/technology.component';

import { ProductLandingComponent } from './components/product/landing/product-landing.component';
import { ProductSpecificationComponent } from './components/product/specifications/product-spec.component';
import { ProductPhotoComponent } from './components/product/photos/product-photo.component';
import { ProductVideoComponent } from './components/product/videos/product-video.component';
import { ProductFeaturesComponent } from './components/product/features/product-features.component';

import { PageNotFound } from './global-components/notfound/pagenotfound';

export const routes = <Layout> <Switch>
	<Route exact path='/' component={HomeComponent} />

	<Route path="/about" component={AboutComponent} />
	<Route path="/innovations" component={InnovationComponent} />
	<Route path="/legal" component={LegalComponent} />

	<Route path="/category/:catgoryname?" component={CategoryComponent} />
	<Route path="/events/:eventtype?/:categorytype?" component={EventComponent} />
	<Route path="/technology/:technologytype?" component={TechnologyComponent} />

	<Route path="/product/:producturlkey/specifications" component={ProductSpecificationComponent} />
	<Route path="/product/:producturlkey/photos" component={ProductPhotoComponent} />
	<Route path="/product/:producturlkey/videos" component={ProductVideoComponent} />
	<Route path="/product/:producturlkey/features" component={ProductFeaturesComponent} />
	<Route path="/product/:producturlkey" component={ProductLandingComponent} />

	<Route path="/distributors/:countryName/contact" component={DistributorContactComponent} />
	<Route path="/distributors/:countryName/confirmation" component={DistributorContactConfirmComponent} />
	<Route path="/distributors/:countryName?" component={DistributorComponent} />

	<Route component={PageNotFound} />
</Switch></Layout>;
