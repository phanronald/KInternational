﻿using KInternational.Data.DBConfig;
using KInternational.Data.Technology;
using KInternational.Models.Models;
using KInternational.Services.Core;
using System.Collections.Generic;
using System.Linq;

namespace KInternational.Services.Technology
{
	public class TechnologyService : ITechnologyService
	{
		public IList<TechnologyModel> GetAllTechnologies()
		{
			var client = new TechnologyRepo(new MongoConfig());
			return client.GetTechnologies().ToList();
		}

		public IList<TechnologyModel> GetTechnologiesByType(int techType)
		{
			var client = new TechnologyRepo(new MongoConfig());
			return client.GetTechnologies().Where(x => x.TechType == techType).ToList();
		}
	}
}
