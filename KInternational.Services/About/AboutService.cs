﻿using KInternational.Services.Core;
using KInternational.Data.About;
using KInternational.Models.Models;
using KInternational.Data.DBConfig;

namespace KInternational.Services.About
{
	public class AboutService : IAboutService
	{
		public AboutModel GetAllAbout()
		{
			var client = new AboutRepo(new MongoConfig());
			return client.GetAboutData();
		}
	}
}
