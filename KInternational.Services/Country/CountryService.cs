﻿using System.Collections.Generic;
using System.Linq;
using KInternational.Services.Core;
using KInternational.Models.Models;
using KInternational.Data.DBConfig;
using KInternational.Data.Country;

namespace KInternational.Services.Country
{
	public class CountryService : ICountryService
	{
		public List<CountryModel> GetAllCountries()
		{
			var client = new CountryRepo(new MongoConfig());
			return client.GetAllCountries().ToList();
		}
	}
}
