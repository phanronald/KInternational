﻿using KInternational.Models.Models;
using System.Collections.Generic;

namespace KInternational.Services.Core
{
	public interface IProductColorPhotosService
	{
		List<ProductColorPhotos> GetAllProductColorPhotos();
		List<ProductColorPhotos> GetProductColorPhotos_ById(int productId);
	}
}
