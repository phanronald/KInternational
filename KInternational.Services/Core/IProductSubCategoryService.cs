﻿using KInternational.Models.Models;
using System.Collections.Generic;

namespace KInternational.Services.Core
{
	public interface IProductSubCategoryService
	{
		List<ProductSubCategoryModel> GetSubCategories();
		ProductSubCategoryModel Get_SubCategory_ById(int subCategoryId);
	}
}
