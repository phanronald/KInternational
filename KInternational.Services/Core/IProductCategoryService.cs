﻿using KInternational.Models.Models;
using System.Collections.Generic;

namespace KInternational.Services.Core
{
	public interface IProductCategoryService
	{
		List<ProductCategoryModel> GetCategories();
		ProductCategoryModel Get_Category_ById(int categoryId);
	}
}
