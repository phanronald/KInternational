﻿using KInternational.Models.Models;
using System.Collections.Generic;

namespace KInternational.Services.Core
{
	public interface ITechnologyService
	{
		IList<TechnologyModel> GetAllTechnologies();
		IList<TechnologyModel> GetTechnologiesByType(int techType);
	}
}
