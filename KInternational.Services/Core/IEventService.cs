﻿using KInternational.Models.Models;
using System.Collections.Generic;

namespace KInternational.Services.Core
{
	public interface IEventService
	{
		IList<EventModel> GetAllEvents();
	}
}
