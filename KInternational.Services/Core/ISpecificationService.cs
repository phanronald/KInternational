﻿using KInternational.Models.Models;
using System.Collections.Generic;

namespace KInternational.Services.Core
{
	public interface ISpecificationService
	{
		List<ProductSpecification> GetAllProductSpecifications();
		List<ProductSpecification> GetAllProductSpecifications_ByProductId(int productId);

		List<SpecificationGroup> GetAllSpecGroups();
		List<SpecificationGroup> GetAllSpecGroups_ById(int specGroupId);

		List<Specification> GetAllSpecifications();
		List<Specification> GetAllSpecifications_ById(int specId);
	}
}
