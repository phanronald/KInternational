﻿using KInternational.Models.Models;
using System.Collections.Generic;

namespace KInternational.Services.Core
{
	public interface IProductService
	{
		List<ProductModel> GetProducts();
		ProductModel Get_Product_ById(int productId);

		List<ColorModel> GetAllColors();
		ColorModel GetColor_ByColorId(int colorId);

		List<ProductPhoto> GetProductPhotos_ByProductId(int productId);
		List<ProductVideo> GetProductVideos_ByProductId(int productId);
	}
}
