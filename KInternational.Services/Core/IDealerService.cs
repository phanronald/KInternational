﻿using KInternational.Models.Models;
using System.Collections.Generic;

namespace KInternational.Services.Core
{
	public interface IDealerService
	{
		IList<DealerModel> GetAllDealers();
	}
}
