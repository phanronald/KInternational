﻿using KInternational.Models.Models;

namespace KInternational.Services.Core
{
	public interface IAboutService
	{
		AboutModel GetAllAbout();
	}
}
