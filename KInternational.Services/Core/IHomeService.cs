﻿using KInternational.Models.Models;

namespace KInternational.Services.Core
{
	public interface IHomeService
	{
		string GetBingMapApiKey();
	}
}
