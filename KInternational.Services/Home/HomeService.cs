﻿using KInternational.Services.Core;
using System;
using System.Configuration;

namespace KInternational.Services.Home
{
	public class HomeService : IHomeService
	{
		private readonly string BING_MAP_API_KEY = "BING_MAP_API_KEY";

		public string GetBingMapApiKey()
		{
			var fromConfig = ConfigurationManager.AppSettings[BING_MAP_API_KEY];
			if (string.Equals(fromConfig, "{BING_MAP_KEY}", StringComparison.InvariantCultureIgnoreCase))
			{
				return Environment.GetEnvironmentVariable(BING_MAP_API_KEY);
			}

			return fromConfig;
		}
	}
}
