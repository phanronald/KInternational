﻿using System.Collections.Generic;
using System.Linq;
using KInternational.Data.DBConfig;
using KInternational.Data.Event;
using KInternational.Models.Models;
using KInternational.Services.Core;

namespace KInternational.Services.Event
{
	public class EventService : IEventService
	{
		public IList<EventModel> GetAllEvents()
		{
			var client = new EventRepo(new MongoConfig());
			return client.GetAllEvents().ToList();
		}
	}
}
