﻿using KInternational.Data.DBConfig;
using KInternational.Data.Dealer;
using KInternational.Models.Models;
using KInternational.Services.Core;
using System.Collections.Generic;
using System.Linq;

namespace KInternational.Services.Dealer
{
	public class DealerService : IDealerService
	{
		public IList<DealerModel> GetAllDealers()
		{
			var client = new DealerRepo(new MongoConfig());
			return client.GetAllDealers().ToList();
		}
	}
}
