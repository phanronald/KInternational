﻿using KInternational.Data.DBConfig;
using KInternational.Data.Distributor;
using KInternational.Models.Models;
using KInternational.Services.Core;
using System.Collections.Generic;
using System.Linq;

namespace KInternational.Services.Distributor
{
	public class DistributorService : IDistributorService
	{
		public IList<DistributorModel> GetAllDistributors()
		{
			var client = new DistributorRepo(new MongoConfig());
			return client.GetAllDistributors().ToList();
		}
	}
}
