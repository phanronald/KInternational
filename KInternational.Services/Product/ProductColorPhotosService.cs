﻿using KInternational.Data.DBConfig;
using KInternational.Data.Product;
using KInternational.Models.Models;
using KInternational.Services.Core;
using System.Collections.Generic;
using System.Linq;

namespace KInternational.Services.Product
{
	public class ProductColorPhotosService : IProductColorPhotosService
	{
		public List<ProductColorPhotos> GetAllProductColorPhotos()
		{
			var client = new ProductColorPhotosRepo(new MongoConfig());
			return client.GetAllProductColorPhotos().ToList();
		}

		public List<ProductColorPhotos> GetProductColorPhotos_ById(int productId)
		{
			var listOfProductColorPhotos = GetAllProductColorPhotos().Where(x => x.ProductId == productId);
			return listOfProductColorPhotos.ToList();
		}
	}
}
