﻿using System.Collections.Generic;
using System.Linq;
using KInternational.Data.DBConfig;
using KInternational.Data.Product;
using KInternational.Models.Models;
using KInternational.Services.Core;

namespace KInternational.Services.Product
{
	public class ProductCategoryService : IProductCategoryService
	{
		public List<ProductCategoryModel> GetCategories()
		{
			var client = new ProductCategoryRepo(new MongoConfig());
			return client.GetCategories().ToList();
		}

		public ProductCategoryModel Get_Category_ById(int categoryId)
		{
			var listOfCategories = GetCategories().Where(x => x.ProductCategoryId == categoryId);
			if(listOfCategories.Any())
			{
				return listOfCategories.FirstOrDefault();
			}

			return null;
		}
	}
}
