﻿using KInternational.Data.DBConfig;
using KInternational.Data.Product;
using KInternational.Models.Models;
using KInternational.Services.Core;
using System.Collections.Generic;
using System.Linq;

namespace KInternational.Services.Product
{
	public class SpecificationService : ISpecificationService
	{
		public List<ProductSpecification> GetAllProductSpecifications()
		{
			var client = new SpecificationRepo(new MongoConfig());
			return client.GetAllProductSpecification().ToList();
		}

		public List<ProductSpecification> GetAllProductSpecifications_ByProductId(int productId)
		{
			var allProductSpecs = GetAllProductSpecifications();
			if (allProductSpecs != null)
			{
				return allProductSpecs.Where(x => x.ProductId == productId).ToList();
			}

			return new List<ProductSpecification>();
		}

		public List<SpecificationGroup> GetAllSpecGroups()
		{
			var client = new SpecificationRepo(new MongoConfig());
			return client.GetAllSpecificationGroups().ToList();
		}

		public List<SpecificationGroup> GetAllSpecGroups_ById(int specGroupId)
		{
			var allSpecGroups = GetAllSpecGroups();
			if (allSpecGroups != null)
			{
				return allSpecGroups.Where(x => x.SpecGroupId == specGroupId).ToList();
			}

			return new List<SpecificationGroup>();
		}

		public List<Specification> GetAllSpecifications()
		{
			var client = new SpecificationRepo(new MongoConfig());
			return client.GetAllSpecifications().ToList();
		}

		public List<Specification> GetAllSpecifications_ById(int specId)
		{
			var allSpecifications = GetAllSpecifications();
			if (allSpecifications != null)
			{
				return allSpecifications.Where(x => x.ProductSpecId == specId).ToList();
			}

			return new List<Specification>();
		}
	}
}
