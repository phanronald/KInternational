﻿using System.Collections.Generic;
using System.Linq;
using KInternational.Data.DBConfig;
using KInternational.Data.Product;
using KInternational.Models.Models;
using KInternational.Services.Core;

namespace KInternational.Services.Product
{
	public class ProductService : IProductService
	{
		public List<ProductModel> GetProducts()
		{
			var client = new ProductRepo(new MongoConfig());
			return client.GetProducts().ToList();
		}

		public ProductModel Get_Product_ById(int productId)
		{
			var listOfProducts = GetProducts().Where(x => x.ProductId == productId);
			if (listOfProducts.Any())
			{
				return listOfProducts.FirstOrDefault();
			}

			return null;
		}

		public List<ColorModel> GetAllColors()
		{
			var client = new ProductRepo(new MongoConfig());
			return client.GetAllColors().ToList();
		}

		public ColorModel GetColor_ByColorId(int colorId)
		{
			var listOfColors = GetAllColors().Where(x => x.ColorId == colorId);
			if (listOfColors.Any())
			{
				return listOfColors.FirstOrDefault();
			}

			return null;
		}

		public List<ProductPhoto> GetProductPhotos_ByProductId(int productId)
		{
			var client = new ProductRepo(new MongoConfig());
			return client.GetAllProductPhotos().Where(x => x.ProductId == productId).ToList();
		}

		public List<ProductVideo> GetProductVideos_ByProductId(int productId)
		{
			var client = new ProductRepo(new MongoConfig());
			return client.GetAllProductVideos().Where(x => x.ProductId == productId).ToList();
		}
	}
}
