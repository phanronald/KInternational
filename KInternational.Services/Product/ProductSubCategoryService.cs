﻿using System.Collections.Generic;
using System.Linq;
using KInternational.Services.Core;
using KInternational.Data.DBConfig;
using KInternational.Data.Product;
using KInternational.Models.Models;

namespace KInternational.Services.Product
{
	public class ProductSubCategoryService : IProductSubCategoryService
	{
		public List<ProductSubCategoryModel> GetSubCategories()
		{
			var client = new ProductSubCategoryRepo(new MongoConfig());
			return client.GetSubCategories().ToList();
		}

		public ProductSubCategoryModel Get_SubCategory_ById(int subCategoryId)
		{
			var listOfSubCategories = GetSubCategories().Where(x => x.ProductSubCategoryId == subCategoryId);
			if (listOfSubCategories.Any())
			{
				return listOfSubCategories.FirstOrDefault();
			}

			return null;
		}
	}
}
