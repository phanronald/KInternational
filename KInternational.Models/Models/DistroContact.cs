﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KInternational.Models.Models
{
	public class DistroContact
	{
		public int DistributorId { get; set; }
		public string RequestType { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Email { get; set; }
		public string Phone { get; set; }
		public string MobilePhone { get; set; }
		public string City { get; set; }
		public string VehicleModel { get; set; }
		public string VehicleYear { get; set; }
		public string Ownership { get; set; }
		public string Comments { get; set; }
		public bool InterestInKawasakiProductsMoto { get; set; }
		public bool InterestInKawasakiProductsATV { get; set; }
		public bool InterestInKawasakiProductsJetSki { get; set; }
		public bool InterestInKawasakiProductsMule { get; set; }
		public bool InterestInKawasakiProductsTeryx { get; set; }
	}
}
