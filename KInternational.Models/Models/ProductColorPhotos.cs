﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace KInternational.Models.Models
{
	public class ProductColorPhotos
	{
		[BsonId]
		public ObjectId Id { get; set; }

		[BsonElement("ProductColorPhotoId")]
		public int ProductColorPhotoId { get; set; }

		[BsonElement("ProductId")]
		public int ProductId { get; set; }

		[BsonElement("ColorId")]
		public int ColorId { get; set; }

		[BsonElement("ImageUrl")]
		public string ImageUrl { get; set; }
	}
}
