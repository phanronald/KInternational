﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace KInternational.Models.Models
{
	public class ProductSpecification
	{
		[BsonId]
		public ObjectId Id { get; set; }

		[BsonElement("ProductSpecId")]
		public int ProductSpecId { get; set; }

		[BsonElement("ProductId")]
		public int ProductId { get; set; }

		[BsonElement("SpecId")]
		public int SpecId { get; set; }

		[BsonElement("SpecValue")]
		public string SpecValue { get; set; }

		[BsonElement("IsFeatured")]
		public bool IsFeatured { get; set; }
	}
}
