﻿using KInternational.Models.Models.Shared;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace KInternational.Models.Models
{
	public class DealerModel : ContactInfo
	{
		[BsonId]
		public ObjectId Id { get; set; }

		[BsonElement("DealerId")]
		public int DealerId { get; set; }

		[BsonElement("ContactName")]
		public string ContactName { get; set; }
	}
}
