﻿namespace KInternational.Models.Models.Shared
{
	public class BasicContact
	{
		public string Text { get; set; }
		public string Email { get; set; }
	}
}
