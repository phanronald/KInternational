﻿namespace KInternational.Models.Models.Shared
{
	public class ProductAvailable
	{
		public string Name { get; set; }
		public string Link { get; set; }
	}
}
