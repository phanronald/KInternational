﻿namespace KInternational.Models.Models.Shared
{
	public class Media
	{
		public string Image { get; set; }
		public string Video { get; set; }
		public int Type { get; set; }
	}
}
