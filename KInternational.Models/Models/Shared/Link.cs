﻿namespace KInternational.Models.Models.Shared
{
	public class Link
	{
		public string Text { get; set; }
		public string Url { get; set; }
	}
}
