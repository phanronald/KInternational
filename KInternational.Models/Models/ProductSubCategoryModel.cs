﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KInternational.Models.Models
{
	public class ProductSubCategoryModel
	{
		[BsonId]
		public ObjectId Id { get; set; }

		[BsonElement("ProductSubCategoryId")]
		public int ProductSubCategoryId { get; set; }

		[BsonElement("ProductCategoryId")]
		public int ProductCategoryId { get; set; }

		[BsonElement("Name")]
		public string Name { get; set; }

		[BsonElement("IsActive")]
		public bool IsActive { get; set; }

		[BsonElement("SortOrder")]
		public int SortOrder { get; set; }
	}
}
