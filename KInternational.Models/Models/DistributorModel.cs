﻿using KInternational.Models.Models.Shared;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace KInternational.Models.Models
{
	public class DistributorModel : ContactInfo
	{
		[BsonId]
		public ObjectId Id { get; set; }

		[BsonElement("DistributorId")]
		public int DistributorId { get; set; }
	
		[BsonElement("DistributorSite")]
		public string DistributorSite { get; set; }

		[BsonElement("MapImage")]
		public string MapImage { get; set; }
	}
}
