﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KInternational.Models.Models
{
	public class ProductVideo
	{
		[BsonId]
		public ObjectId Id { get; set; }

		[BsonElement("ProductId")]
		public int ProductId { get; set; }

		[BsonElement("FileName")]
		public string FileName { get; set; }

		[BsonElement("ThumbnailName")]
		public string ThumbnailName { get; set; }

		[BsonElement("Title")]
		public string Title { get; set; }
	}
}
