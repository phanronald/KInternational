﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace KInternational.Models.Models
{
	public class CountryModel
	{
		[BsonId]
		public ObjectId Id { get; set; }

		[BsonElement("CountryId")]
		public int CountryId { get; set; }

		[BsonElement("Name")]
		public string Name { get; set; }

		[BsonElement("UrlKey")]
		public string UrlKey { get; set; }
	}
}
