﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace KInternational.Models.Models
{
	public class ColorModel
	{
		[BsonId]
		public ObjectId Id { get; set; }

		[BsonElement("ColorId")]
		public int ColorId { get; set; }

		[BsonElement("ColorName")]
		public string ColorName { get; set; }

		[BsonElement("ImageUrl")]
		public string ImageUrl { get; set; }
	}
}
