﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace KInternational.Models.Models
{
	public class SpecificationGroup
	{
		[BsonId]
		public ObjectId Id { get; set; }

		[BsonElement("SpecGroupId")]
		public int SpecGroupId { get; set; }

		[BsonElement("Name")]
		public string Name { get; set; }

		[BsonElement("SortOrder")]
		public int SortOrder { get; set; }
	}
}
