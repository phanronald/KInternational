﻿using KInternational.Models.Models.Shared;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace KInternational.Models.Models
{
	public class AboutModel
	{
		[BsonId]
		public ObjectId Id { get; set; }

		[BsonElement("HeaderText")]
		public string HeaderText { get; set; }

		[BsonElement("HeaderImage")]
		public string HeaderImage { get; set; }

		[BsonElement("Overview")]
		public string Overview { get; set; }

		[BsonElement("Detailed")]
		public string Detailed { get; set; }

		[BsonElement("Contact")]
		public BasicContact Contact { get; set; }

		[BsonElement("MainAddress")]
		public string MainAddress { get; set; }

		[BsonElement("MailAddress")]
		public string MailAddress { get; set; }

		[BsonElement("Phone")]
		public string Phone { get; set; }

		[BsonElement("AboutLinks")]
		public Link[] AboutLinks { get; set; }
	}
}
