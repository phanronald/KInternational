﻿using KInternational.Models.Models.Shared;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KInternational.Models.Models
{
	public class TechnologyModel
	{
		[BsonId]
		public ObjectId Id { get; set; }

		[BsonElement("Name")]
		public string Name { get; set; }

		[BsonElement("TechType")]
		public int TechType { get; set; }

		[BsonElement("Image")]
		public string Image { get; set; }

		[BsonElement("Description")]
		public string Description { get; set; }

		[BsonElement("Media")]
		public Media[] Media { get; set; }

		[BsonElement("ProductAvailable")]
		public ProductAvailable[] ProductAvailable { get; set; }
	}
}
