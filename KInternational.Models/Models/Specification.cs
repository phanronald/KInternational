﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace KInternational.Models.Models
{
	public class Specification
	{
		[BsonId]
		public ObjectId Id { get; set; }

		[BsonElement("ProductSpecId")]
		public int ProductSpecId { get; set; }

		[BsonElement("SpecGroupId")]
		public int SpecGroupId { get; set; }

		[BsonElement("Name")]
		public string Name { get; set; }

		[BsonElement("SortOrder")]
		public int SortOrder { get; set; }

		[BsonElement("IsDisplayed")]
		public bool IsDisplayed { get; set; }
	}
}
