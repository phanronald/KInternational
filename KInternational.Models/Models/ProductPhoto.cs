﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KInternational.Models.Models
{
	public class ProductPhoto
	{
		[BsonId]
		public ObjectId Id { get; set; }

		[BsonElement("ProductId")]
		public int ProductId { get; set; }

		[BsonElement("FileName")]
		public string FileName { get; set; }

		[BsonElement("Title")]
		public string Title { get; set; }
	}
}
