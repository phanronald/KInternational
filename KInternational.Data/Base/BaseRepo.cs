﻿using KInternational.Data.Interfaces;
using MongoDB.Driver;

namespace KInternational.Data.Base
{
	public class BaseRepo
	{
		protected readonly MongoClient client = new MongoClient();
		protected readonly string MonogoDBUri = string.Empty;
		protected readonly string MonogoDBName = string.Empty;
		protected readonly IMongoDatabase MongoDatabase;
		protected readonly string MONGO_URI = "MONGO_URI";
		protected readonly string MONGO_DB = "MONGOLAB_DB";

		public BaseRepo(IConfig config)
		{
			MonogoDBUri = config.GetReadonlyDBConnectionString(MONGO_URI);
			MonogoDBName = config.GetDatabaseName(MONGO_DB);
			client = new MongoClient(MonogoDBUri);
			MongoDatabase = client.GetDatabase(MonogoDBName);
		}
	}
}
