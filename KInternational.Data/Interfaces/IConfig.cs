﻿namespace KInternational.Data.Interfaces
{
	public interface IConfig
	{
		string GetReadonlyDBConnectionString(string key);
		string GetAdminDBConnectionString(string key);
		string GetDatabaseName(string key);
	}
}
