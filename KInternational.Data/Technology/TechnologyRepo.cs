﻿using KInternational.Data.Base;
using KInternational.Data.Core;
using KInternational.Data.Interfaces;
using KInternational.Models.Models;
using MongoDB.Driver;
using System.Linq;

namespace KInternational.Data.Technology
{
	public class TechnologyRepo : BaseRepo, ITechnologyRepo
	{
		private readonly string CollectionName = "Technology";

		public TechnologyRepo(IConfig config) : base(config)
		{
		}

		public IQueryable<TechnologyModel> GetTechnologies()
		{
			var technologyModelCollection = MongoDatabase.GetCollection<TechnologyModel>(CollectionName);
			return technologyModelCollection.AsQueryable();
		}
	}
}
