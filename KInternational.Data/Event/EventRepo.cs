﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;
using KInternational.Data.Core;
using KInternational.Data.Base;
using KInternational.Data.Interfaces;
using KInternational.Models.Models;

namespace KInternational.Data.Event
{
	public class EventRepo : BaseRepo, IEventRepo
	{
		private readonly string CollectionName = "Events";

		public EventRepo(IConfig config) : base(config)
		{

		}

		public IEnumerable<EventModel> GetAllEvents()
		{
			var eventModelCollection = MongoDatabase.GetCollection<EventModel>(CollectionName);
			return eventModelCollection.AsQueryable();
		}
	}
}
