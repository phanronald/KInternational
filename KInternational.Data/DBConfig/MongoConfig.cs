﻿using KInternational.Data.Interfaces;
using System;
using System.Configuration;

namespace KInternational.Data.DBConfig
{
	public class MongoConfig : IConfig
	{
		public string GetAdminDBConnectionString(string key)
		{
			throw new NotImplementedException();
		}

		public string GetDatabaseName(string key)
		{
			var fromConfig = ConfigurationManager.AppSettings[key];
			if (string.Equals(fromConfig, "{MONGODB_DBNAME}", StringComparison.InvariantCultureIgnoreCase))
			{
				return Environment.GetEnvironmentVariable(key);
			}

			return fromConfig;
		}

		public string GetReadonlyDBConnectionString(string key)
		{
			var fromConfig = ConfigurationManager.AppSettings[key];
			if (string.Equals(fromConfig, "{MONGODB_READONLY}", StringComparison.InvariantCultureIgnoreCase))
			{
				return Environment.GetEnvironmentVariable(key);
			}

			return fromConfig;
		}
	}
}
