﻿using KInternational.Data.Interfaces;
using System;
using System.Configuration;

namespace KInternational.Data.DBConfig
{
	public class SQLServerConfig : IConfig
	{
		public string GetAdminDBConnectionString(string key)
		{
			throw new NotImplementedException();
		}

		public string GetDatabaseName(string key)
		{
			var fromConfig = ConfigurationManager.AppSettings[key];
			if (string.Equals(fromConfig, "{SQLSERVER_DB}", StringComparison.InvariantCultureIgnoreCase))
			{
				return Environment.GetEnvironmentVariable(key);
			}

			return fromConfig;
		}

		public string GetReadonlyDBConnectionString(string key)
		{
			var fromConfig = ConfigurationManager.AppSettings[key];
			if (string.Equals(fromConfig, "{SQLSERVER_CONNECTION_STRING}", StringComparison.InvariantCultureIgnoreCase))
			{
				return Environment.GetEnvironmentVariable(key);
			}

			return fromConfig;
		}
	}
}
