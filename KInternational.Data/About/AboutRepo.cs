﻿using System.Linq;
using MongoDB.Driver;
using KInternational.Data.Base;
using KInternational.Data.Core;
using KInternational.Data.Interfaces;
using KInternational.Models.Models;

namespace KInternational.Data.About
{
	public class AboutRepo : BaseRepo, IAboutRepo
	{
		private readonly string CollectionName = "AboutPage";

		public AboutRepo(IConfig config) : base(config)
		{
			
		}

		public AboutModel GetAboutData()
		{
			var aboutModelCollection = MongoDatabase.GetCollection<AboutModel>(CollectionName);
			if(aboutModelCollection != null && aboutModelCollection.AsQueryable().Any())
			{
				return aboutModelCollection.AsQueryable().FirstOrDefault();
			}

			return null;
		}
	}
}
