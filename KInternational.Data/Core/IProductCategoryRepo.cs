﻿using KInternational.Models.Models;
using System.Linq;

namespace KInternational.Data.Core
{
	public interface IProductCategoryRepo
	{
		IQueryable<ProductCategoryModel> GetCategories();
	}
}
