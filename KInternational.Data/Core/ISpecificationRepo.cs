﻿using KInternational.Models.Models;
using System.Linq;

namespace KInternational.Data.Core
{
	public interface ISpecificationRepo
	{
		IQueryable<SpecificationGroup> GetAllSpecificationGroups();
		IQueryable<Specification> GetAllSpecifications();
		IQueryable<ProductSpecification> GetAllProductSpecification();
	}
}
