﻿using KInternational.Models.Models;
using System.Linq;

namespace KInternational.Data.Core
{
	public interface IProductRepo
	{
		IQueryable<ColorModel> GetAllColors();
		IQueryable<ProductModel> GetProducts();
		IQueryable<ProductPhoto> GetAllProductPhotos();
		IQueryable<ProductVideo> GetAllProductVideos();
	}
}
