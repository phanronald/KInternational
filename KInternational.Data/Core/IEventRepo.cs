﻿using KInternational.Models.Models;
using System.Collections.Generic;

namespace KInternational.Data.Core
{
	public interface IEventRepo
	{
		IEnumerable<EventModel> GetAllEvents();
	}
}
