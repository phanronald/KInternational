﻿using KInternational.Models.Models;

namespace KInternational.Data.Core
{
	public interface IAboutRepo
	{
		AboutModel GetAboutData();
	}
}
