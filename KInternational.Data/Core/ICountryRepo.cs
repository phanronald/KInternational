﻿using KInternational.Models.Models;
using System.Collections.Generic;

namespace KInternational.Data.Core
{
	public interface ICountryRepo
	{
		IEnumerable<CountryModel> GetAllCountries();
	}
}
