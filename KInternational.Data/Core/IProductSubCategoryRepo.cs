﻿using KInternational.Models.Models;
using System.Linq;

namespace KInternational.Data.Core
{
	public interface IProductSubCategoryRepo
	{
		IQueryable<ProductSubCategoryModel> GetSubCategories();
	}
}
