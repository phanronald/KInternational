﻿using KInternational.Models.Models;
using System.Linq;

namespace KInternational.Data.Core
{
	public interface IDistributorRepo
	{
		IQueryable<DistributorModel> GetAllDistributors();
	}
}
