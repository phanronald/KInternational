﻿using KInternational.Data.Base;
using KInternational.Data.Core;
using KInternational.Data.Interfaces;
using KInternational.Models.Models;
using MongoDB.Driver;
using System.Linq;

namespace KInternational.Data.Distributor
{
	public class DistributorRepo : BaseRepo, IDistributorRepo
	{
		private readonly string CollectionName = "Distributor";

		public DistributorRepo(IConfig config) : base(config)
		{
		}

		public IQueryable<DistributorModel> GetAllDistributors()
		{
			var distributorModelCollection = MongoDatabase.GetCollection<DistributorModel>(CollectionName);
			return distributorModelCollection.AsQueryable();
		}
	}
}
