﻿using KInternational.Data.Base;
using KInternational.Data.Core;
using KInternational.Data.Interfaces;
using KInternational.Models.Models;
using MongoDB.Driver;
using System.Linq;

namespace KInternational.Data.Dealer
{
	public class DealerRepo : BaseRepo, IDealerRepo
	{
		private readonly string CollectionName = "Dealer";

		public DealerRepo(IConfig config) : base(config)
		{
		}

		public IQueryable<DealerModel> GetAllDealers()
		{
			var dealerModelCollection = MongoDatabase.GetCollection<DealerModel>(CollectionName);
			return dealerModelCollection.AsQueryable();
		}
	}
}
