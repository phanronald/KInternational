﻿using System.Linq;
using MongoDB.Driver;
using KInternational.Data.Core;
using KInternational.Data.Base;
using KInternational.Models.Models;
using KInternational.Data.Interfaces;

namespace KInternational.Data.Product
{
	public class ProductRepo : BaseRepo, IProductRepo
	{
		private readonly string CollectionName = "Product";
		private readonly string ColorCollectionName = "Colors";
		private readonly string ProductPhotoCollectionName = "ProductPhotos";
		private readonly string ProductVideoCollectionName = "ProductVideos";

		public ProductRepo(IConfig config): base(config)
		{

		}

		public IQueryable<ProductModel> GetProducts()
		{
			var prodCollection = MongoDatabase.GetCollection<ProductModel>(CollectionName);
			if (prodCollection != null && prodCollection.AsQueryable().Any())
			{
				return prodCollection.AsQueryable();
			}

			return null;
		}

		public IQueryable<ColorModel> GetAllColors()
		{
			var colorsCollection = MongoDatabase.GetCollection<ColorModel>(ColorCollectionName);
			if (colorsCollection != null && colorsCollection.AsQueryable().Any())
			{
				return colorsCollection.AsQueryable();
			}

			return null;
		}

		public IQueryable<ProductPhoto> GetAllProductPhotos()
		{
			var productPhotosCollection = MongoDatabase.GetCollection<ProductPhoto>(ProductPhotoCollectionName);
			if (productPhotosCollection != null && productPhotosCollection.AsQueryable().Any())
			{
				return productPhotosCollection.AsQueryable();
			}

			return null;
		}

		public IQueryable<ProductVideo> GetAllProductVideos()
		{
			var productVideosCollection = MongoDatabase.GetCollection<ProductVideo>(ProductVideoCollectionName);
			if (productVideosCollection != null && productVideosCollection.AsQueryable().Any())
			{
				return productVideosCollection.AsQueryable();
			}

			return null;
		}
	}
}
