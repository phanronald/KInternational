﻿using System.Linq;
using MongoDB.Driver;
using KInternational.Data.Core;
using KInternational.Data.Base;
using KInternational.Data.Interfaces;
using KInternational.Models.Models;

namespace KInternational.Data.Product
{
	public class ProductCategoryRepo : BaseRepo, IProductCategoryRepo
	{
		private readonly string CollectionName = "ProductCategory";

		public ProductCategoryRepo(IConfig config) : base(config)
		{

		}

		public IQueryable<ProductCategoryModel> GetCategories()
		{
			var prodCategoryCollection = MongoDatabase.GetCollection<ProductCategoryModel>(CollectionName);
			if (prodCategoryCollection != null && prodCategoryCollection.AsQueryable().Any())
			{
				return prodCategoryCollection.AsQueryable();
			}

			throw null;
		}
	}
}
