﻿using KInternational.Data.Base;
using KInternational.Data.Core;
using KInternational.Data.Interfaces;
using KInternational.Models.Models;
using MongoDB.Driver;
using System.Linq;

namespace KInternational.Data.Product
{
	public class SpecificationRepo : BaseRepo, ISpecificationRepo
	{
		private readonly string SpecGroupCollectionName = "SpecificationGroup";
		private readonly string SpecCollectionName = "Specification";
		private readonly string ProductSpecCollectionName = "ProductSpecifiation";

		public SpecificationRepo(IConfig config) : base(config)
		{
		}

		public IQueryable<ProductSpecification> GetAllProductSpecification()
		{
			var prodSpecCollection = MongoDatabase.GetCollection<ProductSpecification>(ProductSpecCollectionName);
			if (prodSpecCollection != null && prodSpecCollection.AsQueryable().Any())
			{
				return prodSpecCollection.AsQueryable();
			}

			return null;
		}

		public IQueryable<SpecificationGroup> GetAllSpecificationGroups()
		{
			var specGroupCollection = MongoDatabase.GetCollection<SpecificationGroup>(SpecGroupCollectionName);
			if (specGroupCollection != null && specGroupCollection.AsQueryable().Any())
			{
				return specGroupCollection.AsQueryable();
			}

			return null;
		}

		public IQueryable<Specification> GetAllSpecifications()
		{
			var specCollection = MongoDatabase.GetCollection<Specification>(SpecCollectionName);
			if (specCollection != null && specCollection.AsQueryable().Any())
			{
				return specCollection.AsQueryable();
			}

			return null;
		}
	}
}
