﻿using KInternational.Data.Base;
using KInternational.Data.Core;
using KInternational.Data.Interfaces;
using KInternational.Models.Models;
using MongoDB.Driver;
using System.Linq;

namespace KInternational.Data.Product
{
	public class ProductColorPhotosRepo : BaseRepo, IProductColorPhotosRepo
	{
		private readonly string CollectionName = "ProductColorPhotos";

		public ProductColorPhotosRepo(IConfig config) : base(config)
		{
		}

		public IQueryable<ProductColorPhotos> GetAllProductColorPhotos()
		{
			var prodPhotoColorCollection = MongoDatabase.GetCollection<ProductColorPhotos>(CollectionName);
			return prodPhotoColorCollection.AsQueryable();
		}
	}
}
