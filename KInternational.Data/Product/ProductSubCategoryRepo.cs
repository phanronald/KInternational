﻿using System.Linq;
using MongoDB.Driver;
using KInternational.Data.Core;
using KInternational.Data.Base;
using KInternational.Data.Interfaces;
using KInternational.Models.Models;

namespace KInternational.Data.Product
{
	public class ProductSubCategoryRepo : BaseRepo, IProductSubCategoryRepo
	{
		private readonly string CollectionName = "ProductSubCategory";

		public ProductSubCategoryRepo(IConfig config) : base(config)
		{

		}

		public IQueryable<ProductSubCategoryModel> GetSubCategories()
		{
			var prodSubCategoryCollection = MongoDatabase.GetCollection<ProductSubCategoryModel>(CollectionName);
			if (prodSubCategoryCollection != null && prodSubCategoryCollection.AsQueryable().Any())
			{
				return prodSubCategoryCollection.AsQueryable();
			}

			throw null;
		}
	}
}
