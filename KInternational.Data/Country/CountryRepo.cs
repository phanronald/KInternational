﻿using MongoDB.Driver;
using System.Collections.Generic;
using KInternational.Data.Base;
using KInternational.Data.Core;
using KInternational.Data.Interfaces;
using KInternational.Models.Models;

namespace KInternational.Data.Country
{
	public class CountryRepo : BaseRepo, ICountryRepo
	{
		private readonly string CollectionName = "Country";

		public CountryRepo(IConfig config) : base(config)
		{

		}

		public IEnumerable<CountryModel> GetAllCountries()
		{
			var countryModelCollection = MongoDatabase.GetCollection<CountryModel>(CollectionName);
			return countryModelCollection.AsQueryable();
		}
	}
}
